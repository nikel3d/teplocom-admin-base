<?

namespace app\commands;

use phpseclib\Net\SSH2;
use yii\console\Controller;

class DeployController extends Controller
{
    private $_servers = [
        'main' => [
            'host' => '',
            'username' => '',
            'password' => '',
            'pathToApp' => 'data/www/yii',
        ],
    ];

    public function actionIndex()
    {
        foreach ($this->_servers as $k => $server) {
            $this->actionServer($k);
        }
    }

    public function actionServer($serverKey) {
        if (!empty($this->_servers[$serverKey])) {
            $server = $this->_servers[$serverKey];
       
            if ($ssh = new SSH2($server['host'])) {
                if ($ssh->login($server['username'], $server['password'])) {
                    $res = $ssh->exec(implode(' && ', [
                        'cd ' . $server['pathToApp'],
                        'git pull',
                        'php yii rbac/init',
                        'php yii migrate --interactive=0',
                        'php yii migrate --migrationPath=@yii/rbac/migrations --interactive=0',
                        'cd vendor/rusbitles/admin-base',
                        'git pull',
                        'cd ../../..',
                        'php yii migrate --migrationPath=@rusbitles/adminbase/migrations --interactive=0',
                        'composer update',
                    ]));

                    echo $res;
                }
            }
        }
    }
}