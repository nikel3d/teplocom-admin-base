<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;

        /**
         * @var $permissionIndex \yii\rbac\Permission
         * @var $permissionCreate \yii\rbac\Permission
         * @var $permissionUpdate \yii\rbac\Permission
         */
        $permVariants = [
            'Index' => [
                'description' => 'Просмотр',
            ],
            'Create' => [
                'description' => 'Создание',
                'parent' => 'Index',
            ],
            'Update' => [
                'description' => 'Обновление',
                'parent' => 'Index',
            ]
        ];
        /**
         * @var $superadmin \yii\rbac\Role
         * @var $guest \yii\rbac\Role
         * @var $registered \yii\rbac\Role
         */
        $basicRoles = ['superadmin' => 'Супер админ', 'guest' => 'Гость', 'registered' => 'Зарегистрированный пользователь'];

        foreach($basicRoles as $role => $description) {
            $$role = $auth->getRole($role);
            if (!$$role) {
                $$role = $auth->createRole($role);
                $$role->description = $description;
                $$role->data = ['type' => 'system'];

                $auth->add($$role);
            }
        }

        /**
         * @var $adminpage \yii\rbac\Permission
         */
        $basicPermissions = ['adminpage' => 'Доступ в админку', 'spyauthorize' => 'Авторизация под чужим аккаунтом'];
        foreach($basicPermissions as $permission => $description) {
            $$permission = $auth->getPermission($permission);
            if (!$$permission) {
                $$permission = $auth->createPermission($permission);
                $$permission->description = $description;
                $$permission->data = ['type' => 'system'];

                $auth->add($$permission);
            }
        }

        if (!$auth->hasChild($superadmin, $adminpage)) {
            $auth->addChild($superadmin, $adminpage);
        }

        $controller_dir = Yii::getAlias('@app/controllers/admin');
        if (file_exists($controller_dir)) {
            $controllers = scandir($controller_dir);

            foreach ($controllers as $controller) {
                if ($controller != '.' && $controller != '..' && !is_dir($controller_dir . '/' . $controller)) {
                    $controllerName = 'app\\controllers\\admin\\' . str_replace('.php', '', $controller);

                    if (property_exists($controllerName, 'modelTitles')) {
                        $titles = $controllerName::$modelTitles;
                        $cName = str_replace('Controller.php', '', $controller) . 'Admin';

                        foreach ($permVariants as $permVariant => $value) {
                            $permission = $auth->getPermission($cName . $permVariant);
                            if (!$permission) {
                                $permission = $auth->createPermission($cName . $permVariant);
                                $permission->description = $titles[1] . ' ' . $value['description'];
                                $permission->data = ['type' => 'system'];
                                
                                $auth->add($permission);
                            } else {
                                $permission->description = $titles[1] . ' ' . $value['description'];
                                $permission->data = ['type' => 'system'];
                                $auth->update($cName . $permVariant, $permission);
                            }
                            ${'permission'. $permVariant} = $permission;

                            if (isset($value['parent']) && !$auth->hasChild($permission, ${'permission'. $value['parent']})) {
                                $auth->addChild($permission, ${'permission'. $value['parent']});
                            }

                            if (!$auth->hasChild($superadmin, $permission)) {
                                $auth->addChild($superadmin, $permission);
                            }
                        }
                    }
                }
            }
        }
    }
}