<?php $this->beginPage() ?>
<!DOCTYPE html>
<html class='jsDisabled'>
    <head>
        <meta charset='utf-8'>
        <title><?=$this->context->seo['title']?></title>
        <meta http-equiv='X-UA-Compatible' content='IE=edge' />
        <meta http-equiv='Last-Modified' content='' />

        <script>
            document.documentElement.className=document.documentElement.className.replace('jsDisabled',' jsEnabled jsLoading '+(!!('ontouchstart' in window) || !!('msmaxtouchpoints' in window.navigator) ? 'touchEnabled' : 'touchDisabled'));
        </script>

        <?php $this->context->head($this) ?>
    </head>
    
    <body>
        <?php $this->context->beginBody($this) ?>
        <?=$content?>
        <?php $this->context->endBody($this) ?>
    </body>
</html>
<?php $this->endPage() ?>