<?php

namespace rusbitles\adminbase\classes;

use rusbitles\adminbase\helpers\TextHelper;

class Date {
    private $_date;
    
    public function __construct($date)
    {
        $this->_date = $date;
    }

    public function __toString()
    {
        return $this->_date;
    }
    
    public function rusDate() {
        return TextHelper::getRusDate($this->_date);
    }

    public function langDate($months) {
        return date('d', $this->_date) . ' ' . $months[intval(date('m', $this->_date)) - 1] . ' ' . date('Y', $this->_date);
    }

    public function shortLangDate($months) {
        return $months[intval(date('m', $this->_date)) - 1] . ' ' . date('Y', $this->_date);
    }

    public function rusDateSmall() {
        return TextHelper::getRusDateSmall($this->_date);
    }
    
    public function date($template) {
        if ($this->_date > 0) {
            return date($template, $this->_date);
        } else {
            return '';
        }
    }

    public function timestamp() {
        return $this->_date;
    }
}