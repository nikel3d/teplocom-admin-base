<?php

namespace rusbitles\adminbase\classes;

use rusbitles\adminbase\helpers\BImages;

class Image {
    private $_image;
    
    public function __construct($image)
    {
        $this->_image = $image;
    }

    public function __toString()
    {
        return $this->_image;
    }

    public function doCrop($width, $height, $quality = 95) {
        return BImages::doCrop($this->_image, $width, $height, $quality);
    }

    public function doProp($width, $height, $quality = 95) {
        return BImages::doProp($this->_image, $width, $height, $quality);
    }
}