<?php

namespace rusbitles\adminbase\classes;

use yii\widgets\ActiveField;

class RBField extends ActiveField {
    public function dropDownUniqueList($items, $options = []) {
        $cn = $this->model->className();
        $variants = $cn::find()->where([$this->attribute => array_keys($items)])->all();

        foreach ($variants as $variant) {
            if ($this->model->id != $variant->id) {
                unset($items[$variant->{$this->attribute}]);
            }
        }

        return $this->dropDownList($items, $options);
    }
}