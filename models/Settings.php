<?php

namespace rusbitles\adminbase\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $title
 * @property string $value
 * @property integer $weight
 */
class Settings extends CActiveRecord
{
    public static $types = [
        1 => 'Текст',
        2 => 'HTML',
        3 => 'Файл',
        4 => 'Элемент модели',
    ];
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
            [['weight', 'type'], 'integer'],
            [['title', 'code', 'value_file', 'model_classname'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'value' => 'Значение',
            'value_file' => 'Значение (Файл)',
            'weight' => 'Порядок',
            'code' => 'Код',
            'model_classname' => 'Модель',
            'type' => 'Тип',
        ];
    }
    
    public static function dictionary() {
        if (!empty(Yii::$app->components['user'])) {
            return [
                ['%USERNAME%'],
                [Yii::$app->user->identity->fullName],
            ];
        }
        return [[], []];
    }

    public static $allConfig = false;
    private static function getConfig($module, $code) {
        if (static::$allConfig === false) {
            static::$allConfig = [];
            $configs = Settings::find()->all();
            foreach($configs as $config) {
                static::$allConfig[$config->module][$config->code] = $config;
            }
        }

        if (isset(static::$allConfig[$module][$code])) return static::$allConfig[$module][$code];

        return false;
    }

    public static $configs = [];
    public static function getParam($module, $code, $default = '') {
        if (!isset(static::$configs[$module][$code])) {
            static::$configs[$module][$code] = $default;
            if ((static::$configs[$module][$code] = Yii::$app->cache->get('rb_admin_settings_'.$module.'_'.$code)) === false) {
                $config = static::getConfig($module, $code);
                if ($config) {
                    if ($config->type == 4 && $config->model_classname != '') {
                        $cn = 'app\\models\\' . $config->model_classname;
                        static::$configs[$module][$code] = $cn::findOne(['id' => $config->value]);
                    } elseif ($config->type == 3) {
                        static::$configs[$module][$code] = $config->value_file;
                    } else {
                        static::$configs[$module][$code] = $config->value;
                    }
                    Yii::$app->cache->set('rb_admin_settings_'.$module.'_'.$code, static::$configs[$module][$code], 86400);
                } else {
                    $settings = new Settings();
                    $settings->title = $module . ' ' . $code;
                    $settings->module = $module;
                    $settings->code = $code;
                    $settings->value = $default;
                    $settings->type = 1;
                    $settings->save();
                }
            }
        }

        if (is_string(static::$configs[$module][$code])) {
            return str_replace(static::dictionary()[0], static::dictionary()[1], static::$configs[$module][$code]);
        } else {
            return static::$configs[$module][$code];
        }
    }
    
    public static function setParam($module, $code, $value, $type = 1) {
        Yii::$app->cache->delete('rb_admin_settings_'.$module.'_'.$code);
        $value = strval($value);
        $settings = Settings::findOne(['module' => $module, 'code' => $code]);
        if (!$settings) {
            $settings = new Settings();
            $settings->module = $module;
            $settings->code = $code;
            $settings->type == $type;
        }
        if ($settings->type == 3) {
            $settings->value_file = $value;
        } else {
            $settings->value = $value;
        }
        if($settings->save()) {
            unset(static::$configs[$module][$code]);
        }
    } 

    public function beforeSave($insert)
    {
        Yii::$app->cache->delete('rb_admin_settings_'.$this->module.'_'.$this->code);

        return parent::beforeSave($insert);
    }

    public static function getParamBR($module, $code) {
        return str_replace("\r\n", '<br/>', self::getParam($module, $code));
    }

    public static function getParamBRTemplate($module, $code, $template) {
        $content = self::getParamBR($module, $code);
        if ($content == '') return '';

        return str_replace("[content]", $content, $template);
    }
    
    public static function getParamTemplate($module, $code, $template) {
        $content = self::getParam($module, $code);
        if ($content == '') return '';
        
        return str_replace("[content]", $content, $template);
    }
}
