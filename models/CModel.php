<?php
namespace rusbitles\adminbase\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;

class CModel extends Model {
    public $success = false;
    
    public function validateName($attribute, $params, $validator = false) {
        if (preg_match('/[0-9]/', $this->$attribute)) {
            $this->addError($attribute, 'Поле «'.$this->getAttributeLabel($attribute).'» не должно иметь цифр');
        }
    }

    public function validateLatin($attribute, $params, $validator = false) {
        if (preg_match('/[а-яА-ЯёЁ]/', $this->$attribute)) {
            $this->addError($attribute, 'Поле «'.$this->getAttributeLabel($attribute).'» не должно содержать кириллических символов');
        }
    }

    public function getAttributeLabelWithRequired($attribute, $required_template = ' *')
    {
        $res = $this->getAttributeLabel($attribute);
        if ($this->isAttributeRequired($attribute)) $res .= $required_template;
        return $res;
    }

    public function getAttributeFormName($attribute) {
        return $this->formName() . '['.$attribute.']';
    }

    public function getValue($attribute) {
        $res = CActiveRecord::getAttributeTemplate($attribute);

        return ArrayHelper::getValue($this, implode('.', $res));
    }
}
?>