<?php

namespace rusbitles\adminbase\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class CActiveRecord extends ActiveRecord {
    /**
     * @var Log $createLog
     */
    public static $sortField = '';
    public static $ajaxAccess = false;
    public $doLog = true;
    public $success = false;
    public $refresh = false;
    public $creator = false;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        if (!isset($scenarios['search'])) {
            $scenarios['search'] = $scenarios['default'];
        }

        return $scenarios;
    }

    public function getTemplateValue($template) {
        $replaceArray = [];
        preg_match_all('/\[(.*?)\]/', $template, $m);
        if (!empty($m) && !empty($m[0])) {
            foreach ($m[0] as $k => $v) {
                $replaceArray[0][] = $v;
                $replaceArray[1][] = explode('->', $m[1][$k]);
            }
        }

        $val = $template;
        foreach ($replaceArray[1] as $k => $rep) {
            $repVal = $this;
            foreach($rep as $repItem) {
                $repVal = $repVal->$repItem;
            }
            $val = str_replace($replaceArray[0][$k], $repVal, $val);
        }

        return $val;
    }

    public static function getDropDownList($type = false, $public = false, $defaultValue = false, $orderBy = false) {
        $result = array();
        $criteria = self::find();

        if ($type !== false) {
            $criteria->andWhere(['type' => $type]);
        }
        if ($public  !== false) {
            $criteria->published();
        }
        if ($orderBy !== false) {
            $criteria->addOrderBy($orderBy);
        }
        $items = $criteria->all();

        if($defaultValue !== false) {
            $result[0] = $defaultValue;
        }
        foreach($items as $item) {
            $result[$item->id] = $item->title;
        }
        return $result;
    }

    public static function find() {
        $f = new ScopeQuery(get_called_class());
        return $f;
    }

    public function getIsPublic() {
        return $this->public > 0;
    }
    
    public static function getNewLoaded($defAttrs = [])
    {
        $res = new static;
        $res->load($defAttrs, '');
        return $res;
    }
    
    public function afterInit() {
    }

    public function beforeSave($insert)
    {
        if ($this->hasAttribute('weight')) {
            if ($this->weight == '') {
                $criteria =  static::find()->orderBy('weight DESC')->limit(1);
                if (!$insert) {
                    $criteria->andWhere(['<>', 'id', $this->id]);
                }
                if ($this->hasAttribute('parent_id')) {
                    $criteria->andWhere(['parent_id' => $this->parent_id]);
                }
                $near = $criteria->one();

                if ($near) {
                    $this->weight = $near->weight + 10;
                } else {
                    $this->weight = 10;
                }
            }
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($this->className() != 'rusbitles\adminbase\models\Log' && $this->doLog) {
            $log = new Log();
            $log->model = $this->className();
            $log->model_id = $this->id;
            $log->operation = $insert?'save':'update';

            if (Yii::$app->request->isConsoleRequest) {
                $log->ip = 'console';
                if ($this->creator) $log->user_id = $this->creator->id;
            } elseif(Yii::$app->user->isGuest) {
                $log->ip = Yii::$app->request->userIP;
                $log->post = json_encode(Yii::$app->request->post());
            } else {
                $log->ip = Yii::$app->request->userIP;
                $log->post = json_encode(Yii::$app->request->post());
                if ($this->creator) {
                    $log->user_id = $this->creator->id;
                } else {
                    $log->user_id = Yii::$app->user->id;
                }
            }

            if ($log->ip != 'console') $log->save();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function getAttributeLabelWithRequired($attribute, $required_template = ' *')
    {
        $res = $this->getAttributeLabel($attribute);
        if ($this->isAttributeRequired($attribute)) $res .= $required_template;
        return $res;
    }
    
    public function getAttributeFormName($attribute) {
        $res = static::getAttributeTemplate($attribute);
        return $this->formName() . str_replace('[null]', '[]', '[' . implode('][', $res) . ']');
    }
    
    public function drawErrors() {
        if ($this->hasErrors()) {
            return '<div class="messageBlock messageBlock-errors"><ul><li>' . implode('</li><li>', $this->firstErrors) . '</li></ul></div>';
        }
    }

    public static function dropDownAjax($q, $options, $criteria = false) {
        if (static::$ajaxAccess) {
            if ($criteria !== false) {
                return $criteria->andWhere(['LIKE', $options['attr'], $q])->getDropDownListJson($options['tmpl'], "Не выбрано");
            } else {
                return static::find()->andWhere(['LIKE', $options['attr'], $q])->getDropDownListJson($options['tmpl'], "Не выбрано");
            }
        } else {
            return [];
        }
    }

    public function getCreateLog() {
        return Log::find()->where(['model' => $this->className(), 'model_id' => $this->id, 'operation' => 'save'])->orderBy('date ASC')->one();
    }

    public function getAuthor() {
        $log = $this->createLog;
        if ($log && $log->user) return $log->user->fullName . ' (id: ' . $log->user_id . ')';
        return '<Не найдено>';
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'author' => 'Автор',
        ]);
    }

    protected function saveLinkMany2Many($table, $attribute, $modelColumn, $targetColumn) {
        $db = Yii::$app->db->createCommand();
        $batch = [];
        foreach($this->{$attribute} as $value) {
            $batch[] = [
                $modelColumn => $this->id,
                $targetColumn => $value,
            ];
        }
        $db->delete($table, [$modelColumn => $this->id])->execute();
        $db->batchInsert($table, [$modelColumn, $targetColumn], $batch)->execute();
    }
    
    public function getErrorsAndClear($attribute = null)
    {
        $res = parent::getErrors($attribute);
        $this->clearErrors($attribute);
        
        return $res;
    }

    public function getValue($attribute) {
        $res = CActiveRecord::getAttributeTemplate($attribute);

        return ArrayHelper::getValue($this, implode('.', $res));
    }

    public static function getAttributeTemplate($attribute) {
        preg_match_all('/\[?([^\[\]]*)\]?/', $attribute, $m);
        $res = [];
        if (count($m) > 0) {
            foreach ($m[0] as $k => $v) {
                if ($v != '') {
                    if ($m[1][$k] != '') {
                        $res[] = $m[1][$k];
                    } else {
                        $res[] = 'null';
                    }
                }
            }
        }

        return $res;
    }

    public function loadAndSavePost($flash = false, $after_save = null) {
        if ($flash != '' && Yii::$app->getSession()->hasFlash(static::className() . 'Success')) {
            $this->success = true;

            $id = Yii::$app->getSession()->getFlash(static::className() . 'Success');
            $res = static::find()->where(['id' => $id])->one();
            $res->success = true;

            return $res;
        } elseif (Yii::$app->request->isPost && $this->load(Yii::$app->request->post()) && $this->save()) {
            $this->success = true;
            if (is_callable($after_save)) {
                $after_save($this);
            }
            if ($flash) {
                Yii::$app->getSession()->setFlash(static::className() . 'Success', $this->id);
                Yii::$app->controller->refresh();
            }

            return $this;
        }

        return false;
    }

    public function drawInput($attribute, $type = 'text', $class = '', $errorClass = '', $placeholder = '') {
        return '<input type="'.$type.'" name="'.$this->getAttributeFormName($attribute).'" value="'.$this->{$attribute}.'" class="'. ($this->hasErrors($attribute)?$errorClass:$class) .'" placeholder="'. $placeholder .'" />';
    }

    public function drawCheckbox($attribute, $class = '') {
        return '<input type="checkbox" name="'.$this->getAttributeFormName($attribute).'" value="1" class="'. $class .'"'.($this->{$attribute}==1?' checked':'').'/>';
    }

    public function drawRadio($attribute, $value, $class = '') {
        return '<input type="radio" name="'.$this->getAttributeFormName($attribute).'" value="'.$value.'" class="'. $class .'"'.($this->{$attribute}==$value?' checked':'').'/>';
    }

    public function drawHidden($attribute, $value, $class = '', $options = '') {
        return '<input type="hidden" name="'.$this->getAttributeFormName($attribute).'" value="'.$value.'" class="'. $class .'" '.$options.'/>';
    }

    public function drawRadioGroup($attribute, $values = []) {
        $res = '';
        foreach ($values as $k => $v) {
            $res .= '<input type="radio" name="'.$this->getAttributeFormName($attribute).'" value="'.$v.'"'.($this->{$attribute}==$v?' checked':'').'/>';
        }
        return $res;
    }

    public function drawTextarea($attribute, $class = '', $errorClass = '') {
        return '<textarea name="'.$this->getAttributeFormName($attribute).'" class="'. ($this->hasErrors($attribute)?$errorClass:$class) .'">'.$this->{$attribute}.'</textarea>';
    }

    public function drawCsrf() {
        return '<input type="hidden" name="'.Yii::$app->getRequest()->csrfParam.'" value="'.Yii::$app->getRequest()->getCsrfToken().'" />';
    }

    public function getXml($attributes = false, $manualAttributes = []) {
        if ($attributes === false) $attributes = $this->attributes();

        $res = '';
        foreach($attributes as $attribute) {
            $res .= '<'.$attribute.'>'.$this->{$attribute}.'</'.$attribute.'>'."\r\n";
        }
        foreach($manualAttributes as $manualAttribute => $value) {
            $res .= '<'.$manualAttribute.'>'.$value.'</'.$manualAttribute.'>'."\r\n";
        }
        return $res;
    }
}

class ScopeQuery extends ActiveQuery {
    public function published() {
        $this->alias('t');
        
        $this->andWhere(['>=', 't.public', 1]);
        
        return $this;
    }
    public function ordered() {
        $this->alias('t');
        $this->orderBy('t.weight ASC');
        return $this;
    }
    public function random() {
        $this->addOrderBy('rand()');
        return $this;
    }

    public function getDropDownList($template = '[title]', $defaultValue = false) {
        $result = array();
        $items = $this->all();

        if($defaultValue !== false) {
            $result[0] = $defaultValue;
        }

        foreach($items as $item) {
            $result[$item->id] = $item->getTemplateValue($template);
        }
        return $result;
    }

    public function getDropDownListJson($template = '[title]', $defaultValue = false) {
        $result = $this->getDropDownList($template, $defaultValue);

        $res = ['results' => []];

        foreach($result as $k => $item) {
            $res['results'][] = [
                'id' => $k,
                'text' => $item,
            ];
        }
        
        return $res;
    }
}

?>