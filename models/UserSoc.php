<?php

namespace rusbitles\adminbase\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class UserSoc extends CActiveRecord
{
    public static function tableName()
    {
        return 'system_users_soc';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'network', 'uid'], 'required'],
            [['network', 'uid'], 'string', 'max' => 255],
            [['soc_array'], 'string'],
            [['user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'user' => 'Пользователь',
            'network' => 'Сеть',
            'uid' => 'id пользователя',
            'soc_array' => 'Массив',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
        ];
    }

    public function behaviors() {
        return [
            'timeStamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
