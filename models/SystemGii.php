<?php

namespace rusbitles\adminbase\models;

use Yii;

/**
 * This is the model class for table "system_gii".
 *
 * @property integer $id
 * @property string $table
 * @property string $post
 * @property integer $time
 */
class SystemGii extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_gii';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post'], 'string'],
            [['time'], 'integer'],
            [['table'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'table' => 'Table',
            'post' => 'Post',
            'time' => 'Time',
        ];
    }

    public function behaviors() {
        return [];
    }

    public function afterFind()
    {
        $this->createGiiDir();
        if (file_exists(Yii::getAlias('@app/gii/'.$this->table.'/db.json'))) {
            $this->db = file_get_contents(Yii::getAlias('@app/gii/'.$this->table.'/db.json'));
        }

        if (file_exists(Yii::getAlias('@app/gii/'.$this->table.'/post.json'))) {
            $this->post = file_get_contents(Yii::getAlias('@app/gii/'.$this->table.'/post.json'));
        }

        parent::afterFind();
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->createGiiDir();
        file_put_contents(Yii::getAlias('@app/gii/'.$this->table.'/db.json'), $this->db);
        file_put_contents(Yii::getAlias('@app/gii/'.$this->table.'/post.json'), $this->post);

        parent::afterSave($insert, $changedAttributes);
    }

    private function createGiiDir() {
        if (!file_exists(Yii::getAlias('@app/gii'))) {
            mkdir(Yii::getAlias('@app/gii'));
        }

        if (!file_exists(Yii::getAlias('@app/gii/'.$this->table))) {
            mkdir(Yii::getAlias('@app/gii/'.$this->table));
        }
    }
}