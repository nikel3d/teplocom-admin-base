<?php

namespace rusbitles\adminbase\models;

use Yii;
use yii\base\Model;

class TreeForm extends Model
{
    public $operation;
    public $target_id;
    public $operationList = [
        1 => 'На тот же уровень, до',
        2 => 'На тот же уровень, после',
        3 => 'Вложить в начало',
        4 => 'Вложить в конец',
    ];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['operation', 'target_id'], 'required'],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'operation' => 'Операция',
            'target_id' => 'Цель',
        ];
    }
}
