<?php
namespace rusbitles\adminbase\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Log extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [];
    }

    public function behaviors() {
        return [
            'timeStamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['date'],
                ],
            ],
        ];
    }

    public function getUser() {
        $userClass = '';
        if (!empty(Yii::$app->components['user'])) {
            $userClass = Yii::$app->user->identityClass;
        } else {
            $userClass = Yii::$app->params['userClass'];
        }

        return $this->hasOne($userClass, ['id' => 'user_id']);
    }
}