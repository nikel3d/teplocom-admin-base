<?php
namespace rusbitles\adminbase\models\forms;

use rusbitles\adminbase\models\CModel;
use rusbitles\adminbase\models\Settings;
use rusbitles\adminbase\models\User;
use Yii;

class RecoveryForm extends CModel {
    public $username;
    public $email;

    public function rules()
    {
        return [
            [['username'], 'string'],
            [['email'], 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email' => 'Email',
        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        $res = parent::validate($attributeNames, $clearErrors);

        if ($this->email == '' && $this->username == '') {
            $this->addError('email', 'Заполните хотя бы одно из полей');
            return false;
        }

        if (!$this->hasErrors()) {
            if(!$this->user) {
                $this->addError('email', 'Пользователь не найден!');
                return false;
            }
        }

        return $res;
    }

    public function recoverySend($backController = '') {
        if ($this->validate()) {
            $user = $this->user;

            $user->recovery = substr(md5(time()), 10, 8);
            $user->save(false, ['recovery']);

            Yii::$app->mailer->setViewPath('@rusbitles/adminbase/mail');
            $res = Yii::$app->mailer->compose('recovery', ['backController' => $backController, 'user' => $user])
                ->setFrom(Settings::getParam('site', 'adminEmail'))
                ->setTo($user->email)
                ->setSubject('Восстановление пароля на сайте «' . Yii::$app->params['name'] . '»')
                ->send();
            
            return true;
        }
        return false;
    }

    public function getUser() {
        $userLogin = User::find()->andWhere(['=', 'username', $this->username])->andWhere(['=', 'public', 1])->one();
        if ($userLogin) return $userLogin;

        $userEmail = User::find()->andWhere(['=', 'email', $this->email])->andWhere(['=', 'public', 1])->one();
        if ($userEmail) return $userEmail;

        return false;
    }
}