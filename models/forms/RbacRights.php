<?php
namespace rusbitles\adminbase\models\forms;

use rusbitles\adminbase\models\CModel;
use Yii;

class RbacRights extends CModel {
    public $permissions = [], $roles = [];

    public function rules()
    {
        return [
            [['permissions', 'roles'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'permissions' => 'Права',
            'roles' => 'Роли',
        ];
    }

    public static function getModel($uid) {
        $res = new static();
        $auth = Yii::$app->authManager;

        $assigns = $auth->getAssignments($uid);
        foreach ($assigns as $assign) {
            $permission = $auth->getPermission($assign->roleName);
            if ($permission) $res->permissions[] = $permission->name;

            $role = $auth->getRole($assign->roleName);
            if ($role) $res->roles[] = $role->name;
        }

        return $res;
    }

    public function save($uid) {
        $auth = Yii::$app->authManager;

        if ($this->validate()) {
            $auth->revokeAll($uid);

            foreach($this->permissions as $permissionName) {
                $permission = $auth->getPermission($permissionName);
                if ($permission) $auth->assign($permission, $uid);
            }

            foreach($this->roles as $roleName) {
                $role = $auth->getRole($roleName);
                if ($role) $auth->assign($role, $uid);
            }
            return true;
        }
        return false;
    }
}