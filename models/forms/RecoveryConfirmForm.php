<?php
namespace rusbitles\adminbase\models\forms;

use rusbitles\adminbase\models\CModel;
use rusbitles\adminbase\models\Settings;
use rusbitles\adminbase\models\User;
use Yii;

class RecoveryConfirmForm extends CModel {
    public $user_id;
    public $code;
    public $password;
    public $repeat;

    public function rules()
    {
        return [
            [['password'], 'string', 'min' => 6, 'max' => 20],
            [['password', 'repeat'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'password' => 'Новый пароль',
            'repeat' => 'Повторите пароль',
        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        $res = parent::validate($attributeNames, $clearErrors);

        if ($this->password != $this->repeat) {
            $this->addError('password', 'Пароли должны совпадать');
            return false;
        }

        return $res;
    }

    public function changePassword() {
        $user = User::findOne($this->user_id);
        if ($user) {
            if ($user->recovery == $this->code && $user->recovery != '') {
                $user->newPassword = $this->password;
                $user->recovery = '';
                $user->save();

                return true;
            } else {
                $user->recovery = '';
                $user->save();
            }
        }

        return false;
    }
}