<?php
namespace rusbitles\adminbase\models\forms;

use rusbitles\adminbase\models\CModel;
use Yii;

class RbacGroup extends CModel {
    public $name, $description, $create = true, $permissions = [], $roles = [];

    public function rules()
    {
        return [
            [['name', 'description'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 64],
            [['permissions', 'roles'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Код',
            'description' => 'Описание',
            'permissions' => 'Права',
            'roles' => 'Роли',
        ];
    }
    
    public static function getModel($name) {
        $res = new static();
        $auth = Yii::$app->authManager;

        $role = $auth->getRole($name);
        if ($role) {
            $res->name = $role->name;
            $res->description = $role->description;
            $res->create = false;

            $res->roles = [];
            $res->permissions = [];
            $children = $auth->getChildren($role->name);
            foreach ($children as $child) {
                if ($child->type == 1) {
                    $res->roles[] = $child->name;
                } else {
                    $res->permissions[] = $child->name;
                }
            }

            return $res;
        }
        
        return false;
    }

    public function save() {
        $auth = Yii::$app->authManager;

        if ($this->validate()) {
            $role = $auth->getRole($this->name);

            if ($this->create) {
                if (!$role) {
                    $role = $auth->createRole($this->name);
                } else {
                    $this->addError('name', 'Такой код уже занят');
                    return false;
                }
            } else {
                if (!$role) {
                    $this->addError('name', 'Группа с таким кодом отсутствует');
                    return false;
                }
            }

            $role->description = $this->description;
            $auth->removeChildren($role);

            if ($this->create) {
                $auth->add($role);
                $this->create = false;
            } else {
                $auth->update($this->name, $role);
            }

            foreach($this->permissions as $permissionName) {
                $permission = $auth->getPermission($permissionName);
                if ($permission) $auth->addChild($role, $permission);
            }

            foreach($this->roles as $roleName) {
                $otherRole = $auth->getRole($roleName);
                if ($otherRole) $auth->addChild($role, $otherRole);
            }
            
            return true;
        }
        return false;
    }

    public static function deleteModel($name) {
        $res = new static();
        $auth = Yii::$app->authManager;

        $role = $auth->getRole($name);
        if ($role) {
            $auth->remove($role);

            return $res;
        }

        return false;
    }

    public static function getPermissions() {
        $auth = Yii::$app->authManager;

        $allPermissions = [];
        $permissions = $auth->getPermissions();
        foreach($permissions as $permission) {
            if (preg_match('/^(.*)(Index|Create|Update)$/', $permission->name, $m)) {
                $allPermissions['table'][$m[1]][$m[2]] = $permission;
            } else {
                $allPermissions['common'][] = $permission;
            }
        }
        
        return $allPermissions;
    }

    public static function getRoles() {
        $auth = Yii::$app->authManager;
        
        $roles = $auth->getRoles();

        return $roles;
    }
}