<?php
namespace rusbitles\adminbase\models\forms;

use rusbitles\adminbase\models\CModel;
use rusbitles\adminbase\models\User;
use Yii;

class ChangePassword extends CModel {
    public $user_id;
    public $oldpassword;
    public $password;
    public $repeat;

    public function rules()
    {
        return [
            [['oldpassword', 'password'], 'string', 'min' => 6, 'max' => 16, 'tooLong' => 'Значение «{attribute}» должно содержать максимум 16 символов.', 'tooShort' => 'Значение «{attribute}» должно содержать минимум 6 символов.'],
            [['oldpassword', 'password', 'repeat'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'oldpassword' => 'Старый пароль',
            'password' => 'Новый пароль',
            'repeat' => 'Повторите пароль',
        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        $res = parent::validate($attributeNames, $clearErrors);

        if ($this->password != $this->repeat) {
            $this->addError('password', 'Пароли должны совпадать');
            return false;
        }

        $user = User::findOne($this->user_id);
        if (!$user)  $this->addError('user_id', 'Не указан пользователь');
        if ($user && $user->generatePassword($this->oldpassword) != $user->password) {
            $this->addError('oldpassword', 'Старый пароль введен не верно');
            return false;
        }

        return $res;
    }

    public function changePassword() {
        $user = User::findOne($this->user_id);
        if ($user) {
            $user->newPassword = $this->password;
            $user->save();
            
            return true;
        }

        return false;
    }
}