<?php
namespace rusbitles\adminbase\models;

/**
 * This is the model class for table "structure".
 *
 * @property integer $id
 * @property string $controller
 * @property string $url
 * @property integer $weight
 * @property integer $public
 */
class Structure extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_structure';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['controller'], 'required'],
            [['controller'], 'unique'],
			[['controller', 'url', 'h1_title', 'seo_title', 'seo_keywords', 'og_title', 'og_image'], 'string', 'max' => 255],
			[['weight'], 'integer'],
			[['seo_description', 'og_description'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
			'controller' => 'Тип страницы',
			'url' => 'Адрес',
			'weight' => 'Порядок',
			'h1_title' => 'H1 title',
			'seo_title' => 'Seo title',
			'seo_description' => 'Seo description',
			'seo_keywords' => 'Seo keywords',
			'og_title' => 'og title',
			'og_description' => 'og description',
			'og_image' => 'og image',
        ];
    }

    public function behaviors() {
        return [
        ];
    }
}