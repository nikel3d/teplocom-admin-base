<?php

namespace rusbitles\adminbase\models;

use creocoder\nestedsets\NestedSetsQueryBehavior;
use Yii;

class CTreeActiveRecord extends CActiveRecord {
    public static function find() {
        $f = new ScopeTreeQuery(get_called_class());
        return $f;
    }
	
	public static function getMenu($func, $prefix = '', $menu = false, $result = []) {
		$cacheKey = [static::class, $func, $prefix, $menu, $result];

		if (($menu = Yii::$app->cache->get($cacheKey)) === false) {
			$menu = static::getMenuRecurcive($func, $prefix, $menu, $result);
			
			Yii::$app->cache->set($cacheKey, $menu, 86400);
        }
        return $menu;
    }

    public static function getMenuRecurcive($func, $prefix = '', $menu = false, $result = []) {
        if ($menu === false) $menu = static::find()->where(['depth' => 0])->one();

        if ($menu) {
            foreach ($menu->children(1)->published()->all() as $menuItem) {
                $buffRow = array_merge([
                    'label' => $menuItem->title,
                    'url' => '',
                    'active' => false,
                    'target' => false,
                    'class' => '',
                    'virtual' => false,
                    'children' => [],
                    'parents' => [],
                ], $func($menuItem));

                if (!empty($result[$prefix . $menu->id])) {
                    $buffRow['parents'] = array_merge([$prefix . $menu->id], $result[$prefix . $menu->id]['parents']);
                    $result[$prefix . $menu->id]['children'][] = $prefix . $menuItem->id;
                }
                $result[$prefix . $menuItem->id] = $buffRow;

                $result = static::getMenuRecurcive($func, $prefix, $menuItem, $result);
            }
        }

        return $result;
    }
}

class ScopeTreeQuery extends ScopeQuery {
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

    public function getDropDownList($template = '[title]', $defaultValue = false) {
        $result = array();
        $items = $this->orderBy('lft ASC')->all();

        if($defaultValue !== false) {
            $result[0] = $defaultValue;
        }

        foreach($items as $item) {
            $result[$item->id] = str_repeat('- ', $item->depth) . $item->getTemplateValue($template);
        }
        return $result;
    }
}

?>