<?php

namespace rusbitles\adminbase\models;

use Yii;
use yii\web\IdentityInterface;

class User extends CActiveRecord implements IdentityInterface
{
    public $rememberMe = 0;
    public $newPassword = '';
    public $fullname_search;

    public function rules()
    {
        return [
            [['username', 'email', 'name', 'last_name', 'second_name'], 'string', 'max' => 255, 'on' => ['login', 'register', 'default']],
            [['newPassword'], 'string', 'min' => 6, 'max' => 16, 'on' => ['login', 'register', 'default'], 'tooLong' => 'Значение «{attribute}» должно содержать максимум 16 символов.', 'tooShort' => 'Значение «{attribute}» должно содержать минимум 6 символов.'],
            [['password'], 'string', 'on' => ['login', 'register']],
            [['public', 'rememberMe'], 'integer', 'on' => ['login', 'register', 'default']],
            [['email'], 'email', 'on' => ['login', 'register', 'default']],
            [['email', 'username'], 'unique', 'on' => ['register', 'default']],

            [['username'], 'required', 'on' => ['login', 'register']],
            [['password'], 'required', 'on' => 'login'],
            [['email'], 'required', 'on' => 'register'],
            [['password'], 'validatePassword', 'on' => 'login'],

            [['fullname_search'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Логин',
            'password' => 'Пароль',
            'email' => 'Адрес электронной почты',
            'public' => 'Публикация',
            'rememberMe' => 'Запомнить',
            'name' => 'Имя',
            'fullName' => 'Фио',
            'fullname_search' => 'Фио',
            'image' => 'Аватар',
            'last_name' => 'Фамилия',
            'second_name' => 'Отчество',
            'newPassword' => 'Пароль',
        ];
    }

    public function register($backController) {
        $this->scenario = 'register';
        if ($this->save()) {
            $this->auth_key = substr(md5(time()), 10, 18);
            $this->public = 0;
            $this->save(false, ['auth_key', 'public']);
            
            $this->send_register_mail($backController);
            
            return true;
        }
        
        return false;
    }
    
    public function send_register_mail($backController) {
        Yii::$app->mailer->setViewPath('@rusbitles/adminbase/mail');
        Yii::$app->mailer->compose('register', ['backController' => $backController, 'user' => $this])
            ->setFrom(Settings::getParam('site', 'adminEmail'))
            ->setTo($this->email)
            ->setSubject('Завершение регистрации на сайте «' . Yii::$app->params['name'] . '»')
            ->send();
    }
    
    public static function confirmRegistration($uid, $auth_code) {
        $user = User::find()->where(['id' => $uid])->andWhere(['auth_key' => $auth_code])->one();
        if ($user && $auth_code != '') {
            $user->auth_key = '';
            $user->public = 1;
            $user->save(false, ['auth_key', 'public']);
            
            return $user;
        }
        
        return false;
    }

    public function login()
    {
        $this->scenario = 'login';
        if ($this->validate()) {

            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }
        
        return false;
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            
            if (!$user || $user->password != $this->generatePassword($this->password) || $user->public == 0) {
                $this->addError($attribute, 'Неверно введен логин или пароль');
            }
        }
    }

    public $_user = false;
    public function getUser()
    {
        if ($this->_user === false) {
            $user = static::findOne(['username' => $this->username]);
            if ($user) $this->_user = $user;
        }

        return $this->_user;
    }
    
    public function getFullName() {
        $fn = [];
        if ($this->last_name != '') $fn[] = $this->last_name;
        if ($this->name != '') $fn[] = $this->name;
        if ($this->second_name != '') $fn[] = $this->second_name;
        if (count($fn) > 0) {
            return implode(' ', $fn);
        } else {
            return $this->username;
        }
    }

    public function beforeSave($insert)
    {
        if($this->newPassword !== false && $this->newPassword != '') {
            $this->password = $this->generatePassword($this->newPassword);
        }

        return parent::beforeSave($insert);
    }

    public static function tableName()
    {
        return 'system_users';
    }
    
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function generatePassword($password) {
        return md5($password);
    }
    
    public function formName()
    {
        return 'User'.$this->scenario;
    }

    public function getSocs() {
        return $this->hasMany(UserSoc::className(), ['user_id' => 'id']);
    }
}