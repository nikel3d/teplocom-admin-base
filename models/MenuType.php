<?php
namespace rusbitles\adminbase\models;

use Yii;
/**
 * This is the model class for table "menu_type".
 *
 * @property integer $id
 * @property string $title
 * @property string $code
 * @property integer $weight
 * @property integer $public
 */
class MenuType extends CActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_menu_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
			[['title', 'code'], 'string', 'max' => 255],
			[['weight'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
			'title' => 'Наименование',
			'code' => 'Код меню',
			'weight' => 'Порядок',
        ];
    }

    public function behaviors() {
        return [
        ];
    }






}