<?php
namespace rusbitles\adminbase\models;

use Yii;
use creocoder\nestedsets\NestedSetsBehavior;
/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $title
 * @property integer $weight
 * @property integer $public
 * @property integer $target
 */
class Menu extends CTreeActiveRecord
{
    public $tree_parent_id = false;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
			[['title', 'url', 'class'], 'string', 'max' => 255],
			[['public', 'in_menu', 'in_breadcrumbs', 'target'], 'integer'],
			[['tree_parent_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
			'title' => 'Наименование',
			'public' => 'Публикация',
			'types' => 'Типы меню',
			'lft' => 'Lft',
			'rgt' => 'Rgt',
			'tree' => 'Tree',
			'depth' => 'Depth',
			'tree_parent_id' => 'Родительский раздел',
			'url' => 'Адрес',
			'in_menu' => 'Выводить в меню',
			'class' => 'Метки',
			'in_breadcrumbs' => 'Выводить в хлебных крошках',
			'target' => 'Открывать во вкладке',
        ];
    }

    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
            ],
        ];
    }


    public function getTypes() {
        return $this->hasMany(MenuType::className(), ['id' => 'type_id'])->viaTable('system_menu2type', ['menu_id' => 'id']);
    }
    
    public function getImplodeTypes() {
        $res = [];
        foreach ($this->types as $type) {
            $res[] = $type->title;
        }

        return implode(', ', $res);
    }

    public static function getDropDownList($type = false, $public = false, $defaultValue = false, $orderBy = false) {
        $result = array();
        $items = Menu::find()->orderBy('lft')->all();
        foreach($items as $item) {
            $result[$item->id] = str_repeat('–', $item->depth) . ' ' . $item->title;
        }
        return $result;
    }

    public function getBreadcrumbs() {
        $res = [];

        $obj = $this;
        do {
            if ($obj->depth > 0 && $obj->public == 1 && $obj->in_breadcrumbs == 1) {
                array_unshift($res, [
                    'label' => $obj->title,
                    'url' => $obj->url,
                ]);
            } else {
                break;
            }
        } while ($obj = $obj->parents(1)->one());

        /*array_unshift($res, [
            'label' => 'Главная',
            'url' => '/',
        ]);*/
        
        return $res;
    }
}