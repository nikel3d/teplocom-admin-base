<?php
    namespace rusbitles\adminbase\behaviors;

    use rusbitles\adminbase\classes\Date;
    use yii\base\Behavior;
    use yii\db\ActiveRecord;

    class DateBehavior extends Behavior
    {
        public $attributes = [];
        public $values = [];
        public $format = 'd.m.Y H:i';

        public function events()
        {
            return [
                ActiveRecord::EVENT_AFTER_FIND => 'beforeInit',
                ActiveRecord::EVENT_INIT => 'beforeInit',
                ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
                ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
                ActiveRecord::EVENT_AFTER_INSERT => 'beforeInit',
                ActiveRecord::EVENT_AFTER_UPDATE => 'beforeInit',
            ];
        }

        public function beforeInit($event) {
            foreach($this->attributes as $attribute) {
                $this->values[$attribute] = $this->owner->{$attribute};
                if ($this->owner->{$attribute} != 0 && $this->owner->{$attribute} != '') {
                    if (is_numeric($this->owner->{$attribute})) {
                        $this->owner->{$attribute} = date($this->format, $this->owner->{$attribute});
                    } elseif(is_string($this->owner->{$attribute})) {
                        $this->owner->{$attribute} = date($this->format, strtotime($this->owner->{$attribute}));
                    } else {
                        $this->owner->{$attribute} = '';
                    }
                } else {
                    $this->owner->{$attribute} = '';
                }
            }
        }

        public function beforeSave($event) {
            foreach($this->attributes as $attribute) {
                $this->owner->{$attribute} = strtotime($this->owner->{$attribute});
            }
        }

        public function canGetProperty($name, $checkVars = true)
        {
            if (in_array(str_replace('_obj', '', $name), $this->attributes)) {
                return true;
            }

            return parent::canGetProperty($name, $checkVars);
        }

        public function __get($name)
        {
            $attr = str_replace('_obj', '', $name);

            return new Date($this->values[$attr]);
        }
    }