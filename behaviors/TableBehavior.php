<?php
namespace rusbitles\adminbase\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class TableBehavior extends Behavior
{
    public $fields = [];
    /*
     * [
     *  'fieldKey' => [
     *          'items' => [
     *              'class 1' => ['text 1', 'text 2']
     *          ],
     *          'hidden' => [0],
     *          'cols' => 2,
     *      ]
     * ]
     * */
    private $tables = [];
    private $tablesSeparated = [];
    private $tablesRowImploded = [];

    public function getTableSettings($field) {
        if (isset($this->fields[$field])) {
            return $this->fields[$field];
        }

        return [];
    }

    public function getTableField($field) {
        if (isset($this->tables[$field])) {
            return $this->tables[$field];
        } elseif(isset($this->fields[$field])) {
            $result = [];
            $lines = explode('[;;]', $this->owner->{$field});
            foreach($lines as $line) {
                $result[] = explode('[::]', $line);
            }
            $this->tables[$field] = $result;
            return $result;
        }

        return [];
    }

    public function getTableSeparated($field) {
        if (isset($this->tablesSeparated[$field])) {
            return $this->tablesSeparated[$field];
        } elseif(isset($this->fields[$field])) {
            $table = $this->getTableField($field);
            $res = [];

            foreach($table as $row) {
                foreach ($row as $k => $cell) {
                    $res[$k][] = $cell;
                }
            }
            $this->tablesSeparated[$field] = $res;
            return $res;
        }

        return [];
    }

    public function getTableRowImploded($field, $imp) {
        if (isset($this->tablesRowImploded[$field][$imp])) {
            return $this->tablesRowImploded[$field][$imp];
        } elseif(isset($this->fields[$field])) {
            $table = $this->getTableField($field);
            $res = [];

            foreach($table as $k => $row) {
                $res[$k] = implode($imp, $row);
            }
            $this->tablesRowImploded[$field][$imp] = $res;
            return $res;
        }

        return [];
    }

    public function canGetProperty($name, $checkVars = true)
    {
        if (
            preg_match('/^(.*)TableArray$/', $name, $m) ||
            preg_match('/^(.*)TableSeparatedArray$/', $name, $m) ||
            preg_match('/^(.*)TableRowImplodedArray$/', $name, $m)
        ) {
            return true;
        }

        return parent::canGetProperty($name, $checkVars);
    }

    public function __get($name)
    {
        if (preg_match('/^(.*)TableArray$/', $name, $m)) {
            return $this->getTableField($m[1]);
        }
        if (preg_match('/^(.*)TableSeparatedArray$/', $name, $m)) {
            return $this->getTableSeparated($m[1]);
        }
        if (preg_match('/^(.*)TableRowImplodedArray$/', $name, $m)) {
            return $this->getTableRowImploded($m[1], ' ');
        }
    }
}