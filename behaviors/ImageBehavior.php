<?php
namespace rusbitles\adminbase\behaviors;

use rusbitles\adminbase\classes\Image;
use yii\base\Behavior;

class ImageBehavior extends Behavior
{
    public $attributes = [];

    public function canGetProperty($name, $checkVars = true)
    {
        if (in_array(str_replace('_obj', '', $name), $this->attributes)) {
            return true;
        }

        return parent::canGetProperty($name, $checkVars);
    }

    public function __get($name)
    {
        $attr = str_replace('_obj', '', $name);

        return new Image($this->owner->{$attr});
    }
}