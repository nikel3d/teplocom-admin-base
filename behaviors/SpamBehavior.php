<?php
    namespace rusbitles\adminbase\behaviors;

    use yii\base\Behavior;
    use yii\db\ActiveRecord;

    class SpamBehavior extends Behavior
    {
        public $attributes = [];
        public $emailCheck = true;
        public $slashCheck = true;
        public $tagCheck = true;
        public $httpCheck = true;
        public $manualFunction = false;

        public function events()
        {
            return [
                ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ];
        }

        public function beforeValidate($event)
        {
            foreach ($this->attributes as $attribute) {
                $res = true;
                $text = $this->owner->{$attribute};

                if ($this->slashCheck && strpos($text, '/') !== false) $res = false;
                if ($this->tagCheck && $text != strip_tags($text)) $res = false;
                if ($this->httpCheck && preg_match('/https?/', $text)) $res = false;
                if ($this->emailCheck && strpos($text, '@') !== false) $res = false;
                if (is_callable($this->manualFunction) && $this->manualFunction()) $res = false;

                if (!$res) $this->owner->addError($attribute, 'Ошибка ввода');
            }
        }
    }