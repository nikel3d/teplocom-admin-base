<?php
    namespace rusbitles\adminbase\behaviors;
    
    use yii\base\Behavior;

    class SelectBehavior extends Behavior
    {
        public $attributes = [];

        public function canGetProperty($name, $checkVars = true)
        {
            $attr = str_replace('_val', '', $name);
            if (!empty($this->attributes[$attr])) {
                return true;
            }

            return parent::canGetProperty($name, $checkVars);
        }

        public function __get($name)
        {
            $attr = str_replace('_val', '', $name);

            return $this->attributes[$attr][$this->owner->{$attr}];
        }
    }