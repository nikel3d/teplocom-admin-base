<?php

namespace rusbitles\adminbase\components;

use Yii;

class Pagination extends \yii\data\Pagination {
    public $maxPage;
    
    public function calculate() {
        $this->maxPage = intval($this->totalCount / $this->pageSize) + (($this->totalCount % $this->pageSize==0)?0:1);
    }
    
    public function draw($view, $itemsCount = 2, $template = '//layouts/_pager') {
        $pages = $this;
        $maxPage = $this->maxPage;
        $currentPage = $this->page + 1;
        $startPage = $currentPage - $itemsCount; if($startPage < 1) $startPage = 1;
        $endPage = $startPage + $itemsCount * 2; if($endPage > $maxPage) $endPage = $maxPage;
        $startPage = $endPage - $itemsCount * 2; if($startPage < 1) $startPage = 1;
        $this->params = is_array($this->params)?$this->params:[];

        $firstPageUrl = \Yii::$app->urlManager->createUrl(array_merge([$this->route], $this->params));
        $endPageUrl = \Yii::$app->urlManager->createUrl(array_merge([$this->route], $this->params, [$this->pageParam => $maxPage]));
        if ($currentPage > 2)
            $previousUrl = \Yii::$app->urlManager->createUrl(array_merge([$this->route], $this->params, [$this->pageParam => $currentPage - 1]));
        else
            $previousUrl = \Yii::$app->urlManager->createUrl(array_merge([$this->route], $this->params));

        if ($currentPage < $maxPage)
            $nextUrl = \Yii::$app->urlManager->createUrl(array_merge([$this->route], $this->params, [$this->pageParam => $currentPage + 1]));
        else
            $nextUrl = \Yii::$app->urlManager->createUrl(array_merge([$this->route], $this->params, [$this->pageParam => $currentPage]));
        
        $nextPageItemCount = $pages->totalCount - $currentPage * $pages->pageSize;
        if ($nextPageItemCount > $pages->pageSize) $nextPageItemCount = $pages->pageSize;

        return $view->render($template, compact(
            'pages', 'maxPage', 'currentPage', 'startPage', 'endPage', 'firstPageUrl', 'endPageUrl', 'previousUrl', 'nextUrl', 'nextPageItemCount'
        ));
    }

    public function getUrl($page) {
        if ($page == 1) {
            return Yii::$app->urlManager->createUrl(array_merge([$this->route], $this->params));
        }
        return Yii::$app->urlManager->createUrl(array_merge([$this->route], $this->params, [$this->pageParam => $page]));
    }
}