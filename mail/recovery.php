<?php
use yii\helpers\Url;
?>
<p>Для восстановления пароля перейдите по ссылке: <a href="<?=Url::to([$backController, 'i' => $user->id, 'c' => $user->recovery], true)?>"><?=Url::to([$backController, 'i' => $user->id, 'c' => $user->recovery], true)?></a></p>