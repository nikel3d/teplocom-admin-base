<?php
use yii\helpers\Url;
?>
<p>Для завершения регистрации перейдите по ссылке: <a href="<?=Url::to([$backController, 'u' => $user->id, 'a' => $user->auth_key], true)?>"><?=Url::to([$backController, 'u' => $user->id, 'a' => $user->auth_key], true)?></a></p>