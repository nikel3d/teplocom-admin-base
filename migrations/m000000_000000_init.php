<?php
use yii\db\Migration;

class m000000_000000_init extends Migration
{
    public function up()
    {
        $this->createTable('system_gii', [
            'id' => $this->primaryKey(),
            'table' => $this->string(255),
            'post' => $this->text(),
            'db' => $this->text(),
            'time' => $this->integer(),
        ]);

        $this->createTable('system_menu', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'url' => $this->string(255),
            'public' => 'tinyint',
            'lft' => $this->integer(),
            'rgt' => $this->integer(),
            'depth' => $this->integer(),
        ]);

        $this->createTable('system_menu2type', [
            'menu_id' => $this->integer(),
            'type_id' => $this->integer(),
        ]);

        $this->createIndex('idx_menu_type',
            'system_menu2type',
            ['menu_id', 'type_id'],
        true);

        $this->createTable('system_menu_type', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'code' => $this->string(255),
            'weight' => 'smallint',
        ]);

        $this->createTable('system_settings', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'module' => $this->string(255),
            'code' => $this->string(255),
            'value' => $this->text(),
            'value_file' => $this->string(255),
            'type' => 'tinyint',
            'weight' => 'smallint',
        ]);

        $this->createTable('system_sort', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'model' => $this->string(255),
            'column' => $this->string(255),
            'sort' => 'tinyint',
            'pagesize' => 'smallint',
            'partial' => 'tinyint',
        ]);

        $this->createTable('system_structure', [
            'id' => $this->primaryKey(),
            'controller' => $this->string(255),
            'url' => $this->string(255),
            'weight' => 'smallint',
            'h1_title' => $this->string(255),
            'seo_title' => $this->string(255),
            'seo_description' => $this->string(500),
            'seo_keywords' => $this->string(255),
            'og_title' => $this->string(255),
            'og_description' => $this->string(500),
            'og_image' => $this->string(255),
        ]);

        $this->createTable('system_users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(50),
            'password' => $this->string(50),
            'auth_key' => $this->string(32),
            'access_token' => $this->string(32),
            'recovery' => $this->string(32),
            'email' => $this->string(50),
            'step' => $this->text(),
            'values' => $this->text(),
            'cookieArray' => $this->text(),
            'group' => $this->integer(),
            'public' => 'tinyint',
            'name' => $this->string(255),
            'last_name' => $this->string(255),
            'second_name' => $this->string(255),
            'image' => $this->string(255),
            'provider' => $this->string(255),
            'soc_links' => $this->text(),
            'last_visit' => $this->text(),
        ]);

        $this->insert('system_users', [
            'username' => 'admin',
            'password' => '0b90e0868ac9a4f8704b3ed6c2e2ccb1',
            'auth_key' => '',
            'access_token' => '',
            'recovery' => '',
            'email' => 'info@example.com',
            'step' => '',
            'values' => '',
            'cookieArray' => '',
            'group' => 1,
            'public' => 1,
            'name' => 'Админ',
            'image' => '',
            'provider' => '',
            'soc_links' => '',
            'last_visit' => '',
        ]);
    }

    public function down()
    {
        $this->dropTable('system_gii');
        $this->dropTable('system_menu');
        $this->dropTable('system_menu2type');
        $this->dropTable('system_menu_type');
        $this->dropTable('system_settings');
        $this->dropTable('system_sort');
        $this->dropTable('system_structure');
        $this->dropTable('system_users');
    }
}