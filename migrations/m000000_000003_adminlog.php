<?php
use yii\db\Migration;

class m000000_000003_adminlog extends Migration
{
    public function up()
    {
        $this->createTable('system_log', [
            'id' => $this->primaryKey(),
            'model' => $this->string(150),
            'model_id' => $this->integer(),
            'operation' => $this->string(50),
            'ip' => $this->string(15),
            'post' => $this->text(),
            'user_id' => $this->integer(),
            'date' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('system_log');
    }
}