<?php
use yii\db\Migration;

class m000000_000007_menu_change extends Migration
{
    public function up()
    {
        $this->addColumn('system_menu', 'class', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('system_menu', 'class');
    }
}