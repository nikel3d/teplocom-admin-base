<?php
use yii\db\Migration;

class m000000_000001_changes extends Migration
{
    public function up()
    {
        $this->addColumn('system_settings', 'model_classname', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('system_settings', 'model_classname');
    }
}