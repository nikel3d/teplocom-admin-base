<?php
use yii\db\Migration;

class m000000_000005_user_change extends Migration
{
    public function up()
    {
        $this->dropColumn('system_users', 'group');
    }

    public function down()
    {
        $this->addColumn('system_users', 'group', $this->integer());
    }
}