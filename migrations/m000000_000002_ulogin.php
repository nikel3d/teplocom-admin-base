<?php
use yii\db\Migration;

class m000000_000002_ulogin extends Migration
{
    public function up()
    {
        $this->addColumn('system_users', 'soc_array', $this->text());
        $this->dropColumn('system_users', 'provider');
        $this->dropColumn('system_users', 'soc_links');
        $this->dropColumn('system_users', 'last_visit');
    }

    public function down()
    {
        $this->dropColumn('system_users', 'soc_array');
    }
}