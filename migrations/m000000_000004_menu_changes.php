<?php
use yii\db\Migration;

class m000000_000004_menu_changes extends Migration
{
    public function up()
    {
        $this->addColumn('system_menu', 'in_menu', 'tinyint');
        $this->addColumn('system_menu', 'in_breadcrumbs', 'tinyint');
    }

    public function down()
    {
        $this->dropColumn('system_menu', 'in_menu');
        $this->dropColumn('system_menu', 'in_breadcrumbs');
    }
}