<?php
use rusbitles\adminbase\models\User;
use rusbitles\adminbase\models\UserSoc;
use yii\db\Migration;

class m000000_000008_user_socials extends Migration
{
    public function up()
    {
        $this->createTable('system_users_soc', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'network' => $this->string(255),
            'uid' => $this->string(255),
            'soc_array' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $users = User::find()->all();
        foreach($users as $user) {
            if (!empty($user->soc_array)) {
                $soc_array = json_decode($user->soc_array, true);

                foreach($soc_array as $soca) {
                    $soc = new UserSoc();
                    $soc->user_id = $user->id;
                    $soc->network = $soca['network'];
                    $soc->uid = $soca['uid'];
                    $soc->soc_array = json_encode($soca);
                    $soc->save();
                }
            }
        }
    }

    public function down()
    {
        $this->dropTable('system_users_soc');
    }
}