<?php
use yii\db\Migration;

class m000000_000006_menu_change extends Migration
{
    public function up()
    {
        $this->addColumn('system_menu', 'target', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('system_menu', 'target');
    }
}