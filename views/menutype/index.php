<?php
use rusbitles\adminbase\helpers\BImages;
use yii\helpers\Html;
use app\models\MenuType;

$fields = [
    "id",
	'title',
	'code',
	'weight',
];
?>
<?=$this->render("@rusbitles/adminbase/views/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>