<div class="form">
    <?php $form = \yii\bootstrap\ActiveForm::begin([
        "layout" => "horizontal",
        "enableAjaxValidation" => true,
        "enableClientValidation" => false,
        "fieldConfig" => [
            "options" => [
                'class' => 'row',
            ],
            "template" => "<div class='col-sm-2 text-right'>{label}</div><div class='col-sm-8'>{input}\n{hint}\n{error}</div>",
        ],
        "fieldClass" => 'rusbitles\adminbase\classes\RBField',
    ]); ?>
    <p class="note"><span class="required">*</span> Обязательно для заполнения.</p>

    <?php foreach($views as $k => $v) { ?>
        <?=$this->render($views[$k], ['form' => $form, 'model' => $models[$k]])?>
    <?php } ?>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=$form->errorSummary($models)?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=\yii\bootstrap\Html::submitButton("Сохранить", ["class" => "btn btn-primary", "name"=>"save"])?>
            <?=\yii\bootstrap\Html::submitButton("Применить", ["class" => "btn btn-primary", "name"=>"apply"])?>
            <?=\yii\bootstrap\Html::submitButton("Отмена", ["class" => "btn btn-default", "name"=>"cancel"])?>
        </div>
    </div>

    <?php $form->end(); ?>
</div>