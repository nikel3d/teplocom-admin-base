<?php
use rusbitles\adminbase\models\TreeForm;
?>
<h1><?=$this->context->modelTitles[0]?>: редактирование <?=isset($models[0]->title)?$model[0]->title:$model[0]->id; ?></h1>

<?php if (count($tabs) > 1) { ?>
    <ul class="nav nav-tabs">
        <?php foreach($tabs as $tab) { ?>
            <li class="<?=$tab['active']?'active':''?>">
                <a href="<?=$tab['link']?>"><?=$tab['label']?></a>
            </li>
        <?php } ?>
    </ul>
    <br/>
<?php } ?>

<?php if ($tabs[0]['active']) { ?>
    <?=$this->render('form', ['models' => $models, 'views' => $views])?>
    <?php if (is_subclass_of($this->context, 'rusbitles\adminbase\controllers\base\TreeAdminController')) { ?>
        <?=$this->render('tree_form', array('model' => $models[0], 'treeForm' => new TreeForm()))?>
    <?php } ?>
<?php } else { ?>
    <?php foreach($tabs as $tab) { ?>
        <?php if($tab['active']) { ?>
            <?=Yii::$app->runAction($tab['controller'], $tab['controller_params'])?>
        <?php } ?>
    <?php } ?>
<?php } ?>