<?php
use rusbitles\adminbase\helpers\SystemTools;
use yii\bootstrap\Html;
use yii\data\Pagination;
use yii\grid\GridView;
use yii\jui\DatePicker;
use yii\widgets\LinkPager;

?>

<?php
    $searchFields = $this->context->searchFields();

    $modelName = end(explode('\\', get_class($item)));
    foreach($fields as $fKey => $fVal) {
        if (is_array($fVal) && !empty($fVal['class'])) continue;
        if (!is_array($fVal)) $fields[$fKey] = ['attribute' => $fVal];

        $sort = (!empty($_GET['sort'])&&$_GET['sort']==$fields[$fKey]['attribute'])?('-'.$fields[$fKey]['attribute']):($fields[$fKey]['attribute']);
        if (empty($fVal['header']) && $this->context->actionParams['partial'] && count($this->context->parents) > 0) {
            $fields[$fKey]['header'] = '<a class="headerTable" href="'.Yii::$app->urlManager->createUrl([$this->context->module->requestedRoute, 'id' => $this->context->actionParams['parent'], 'tab'=>$modelName, 'sort'=>$sort]).'">'.$item->getAttributeLabel($fields[$fKey]['attribute']).'</a><span class="headerTable">'.$item->getAttributeLabel($fields[$fKey]['attribute']).'</span>';
        } else {
            $fields[$fKey]['header'] = '<a class="headerTable" href="'.SystemTools::getUrl(['sort' => $sort]).'">'.$item->getAttributeLabel($fields[$fKey]['attribute']).'</a><span class="headerTable">'.$item->getAttributeLabel($fields[$fKey]['attribute']).'</span>';
        }
        if (empty($searchFields[$fields[$fKey]['attribute']])) {
            $fields[$fKey]['filter'] = false;
        } else {
            switch ($searchFields[$fields[$fKey]['attribute']]['type']) {
                case 'public':
                    $fields[$fKey]['filter'] = [1 => "Активные", 0 => "Неактивные"];
                    break;
                case 'checkbox':
                    $fields[$fKey]['filter'] = [1 => "Вкл", 0 => "Выкл"];
                    break;
                case 'date':
                    $fields[$fKey]['filter'] = DatePicker::widget([
                        'model' => $item,
                        'attribute' => $fields[$fKey]['attribute'],
                        'language' => 'ru',
                        'dateFormat' => 'dd.MM.yyyy',
                    ]);
                    break;
                default:
                    $fields[$fKey]['filter'] = true;
            }
        }
    }

    $module = $this->context->baseModel?$this->context->module->module:$this->context->module;

    $pagesParams = [
        'route' => $module->requestedRoute,
        'page' => empty($_GET['page'])?0:(intval($_GET['page'])-1),
        'totalCount' => $dataProvider->getTotalCount(),
        'pageSize' => $dataProvider->pagination->pageSize,
    ];

    if ($module->requestedAction->id == 'update') { // inTab
        $pagesParams['params'] = ['id' => $this->context->actionParams['parent'], 'tab'=>$modelName];
    } else { // not in tab
        //$pagesParams['params'] = ['id' => $this->context->actionParams['parent']];
    }

    $pages = new Pagination($pagesParams);
?>

<h1><?=$this->context->modelTitles[1]?><span>: управление</span></h1>

<?php if ($this->context->getButtons()) { ?>
    <div class="btn-group actionButtons">
        <?php foreach($this->context->getButtons() as $k => $v) { ?>
            <a class="btn btn-default" href="<?=$v['url']?>"><i class="fa fa-<?=$v['icon']?>"></i> <?=$v['title']?></a>
        <?php } ?>
    </div>
<?php } ?>

<?php
$wParams = [
    'dataProvider' => $dataProvider,
    'layout' => "{summary}{items}",
    'columns' => $fields,
];
if (!isset($hideAction) || $hideAction == false) {
    $lastColumn = [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{update} {delete}',
        'buttons' => [
            'update' => function ($url, $model) {
                $modelName = strtolower(end(explode('\\', get_class($model))));
                $urlParams = [];
                if ($this->context->actionParams['partial']) $urlParams['partial'] = 1;
                if (count($this->context->parents) > 0) $urlParams['pback'] = 1;
                if (!empty($_GET['page'])) $urlParams['pback'] = intval($_GET['page']);
                if (!empty($_GET['current'])) $urlParams['current'] = $_GET['current'];

                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Yii::$app->urlManager->createUrl([($this->context->baseModel?$this->context->admin->id.'/':'') . $this->context->id . '/update', 'id' => $model->id] + $urlParams));
            },
            'delete' => function ($url, $model) {
                $modelName = strtolower(end(explode('\\', get_class($model))));
                $urlParams = [];
                if ($this->context->actionParams['partial']) $urlParams['partial'] = 1;
                if (count($this->context->parents) > 0) $urlParams['pback'] = 1;
                if (!empty($_GET['page'])) $urlParams['pback'] = intval($_GET['page']);

                return Html::a('<span class="glyphicon glyphicon-trash"></span>', Yii::$app->urlManager->createUrl([($this->context->baseModel?$this->context->admin->id.'/':'') . $this->context->id . '/delete', 'id' => $model->id] + $urlParams), ['data-confirm' => Yii::t('yii', 'Вы точно хотите удалить запись?')]);
            },
        ],
    ];

    if ($item->hasAttribute('public')) {
        $lastColumn['template'] = '{changepublic} {update} {delete}';
        $lastColumn['buttons']['changepublic'] = function($url, $model) {
            $modelName = strtolower(end(explode('\\', get_class($model))));
            $urlParams = [];
            if ($this->context->actionParams['partial']) $urlParams['partial'] = 1;
            if (count($this->context->parents) > 0) $urlParams['pback'] = 1;
            if (!empty($_GET['page'])) $urlParams['pback'] = intval($_GET['page']);

            $eyeClass = 'open';
            if ($model->isPublic) $eyeClass = 'close';

            return Html::a('<span class="glyphicon glyphicon-eye-'.$eyeClass.'"></span>', Yii::$app->urlManager->createUrl([($this->context->baseModel?$this->context->admin->id.'/':'') . $this->context->id . '/changepublic', 'id' => $model->id] + $urlParams), ['data-confirm' => Yii::t('yii', 'Вы точно хотите '.($eyeClass=='open'?'Показать':'Скрыть').' запись?')]);
        };
    }

    $wParams['columns'] = array_merge($wParams['columns'], [$lastColumn]);
}
//if(isset($searcher) && $searcher == true) {
    $wParams['filterModel'] = $item;
//}

echo GridView::widget($wParams);
?>

<div class="row pageBox">
    <div class="col-sm-9">
        <?=LinkPager::widget(['pagination' => $pages]);?>
    </div>
    <div class="col-sm-3">
        <div class="pageSizer">
            Выводить по
            <a class="<?=($this->context->pageSize==20)?'active':''?>" href="<?=SystemTools::getUrl(['ps' => 20], ['page'])?>">20</a>
            <a class="<?=($this->context->pageSize==50)?'active':''?>" href="<?=SystemTools::getUrl(['ps' => 50], ['page'])?>">50</a>
            <a class="<?=($this->context->pageSize==100)?'active':''?>" href="<?=SystemTools::getUrl(['ps' => 100], ['page'])?>">100</a>
            <a class="<?=($this->context->pageSize==100000000)?'active':''?>" href="<?=SystemTools::getUrl(['ps' => 'all'], ['page'])?>">Все</a>
        </div>
    </div>
</div>
