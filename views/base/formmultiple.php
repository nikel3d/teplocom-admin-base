<div class="form">
    <?php $form = \yii\bootstrap\ActiveForm::begin([
        "id" => "multipleForm",
        "layout" => "horizontal",
        "enableAjaxValidation" => false,
        "enableClientValidation" => false,
        "fieldConfig" => [
            "options" => [
                'class' => 'row',
            ],
            "template" => "<td>{label}: {input}\n{hint}\n{error}</td>",
        ],
        "fieldClass" => 'rusbitles\adminbase\classes\RBField',
    ]); ?>
    <p class="note"><span class="required">*</span> Обязательно для заполнения.</p>

    <table class="multipletable">
        <tr>
            <?=$this->render($view, ['form' => $form, 'model' => $model])?>
        </tr>
        <tr class="copy">
            <?=$this->render($view, ['form' => $form, 'model' => $model])?>
        </tr>
    </table>

    <?/*<div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=$form->errorSummary($model)?>
        </div>
    </div>*/?>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=\yii\bootstrap\Html::button("Добавить", ["class" => "btn", "name"=>"add"])?>
            <?=\yii\bootstrap\Html::submitButton("Сохранить", ["class" => "btn btn-primary", "name"=>"save"])?>
            <?//=\yii\bootstrap\Html::submitButton("Применить", ["class" => "btn btn-primary", "name"=>"apply"])?>
            <?=\yii\bootstrap\Html::submitButton("Отмена", ["class" => "btn btn-default", "name"=>"cancel"])?>
        </div>
    </div>

    <?php $form->end(); ?>
</div>