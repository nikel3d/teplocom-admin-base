<div class="form">
    <?php $form = \yii\bootstrap\ActiveForm::begin([
        'action' => Yii::$app->urlManager->createUrl(['admin/'.$this->context->mainmenuHandle.'/treeform', 'id'=>$model->id]),
        'layout' => 'horizontal',
        'fieldConfig' => [
            //'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?=$form->field($treeForm, 'operation')->dropDownList($treeForm->operationList)?>
    <?php $mn = get_class($this->context); $mn = $mn::$fullModelName; ?>
    <?=$form->field($treeForm, 'target_id')->dropDownList($mn::getDropDownList())?>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=$form->errorSummary($treeForm)?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8 col-lg-offset-2">
            <?=\yii\bootstrap\Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name'=>'save'])?>
            <?=\yii\bootstrap\Html::submitButton('Применить', ['class' => 'btn btn-primary', 'name'=>'apply'])?>
        </div>
    </div>

    <?php $form->end(); ?>

</div><!-- form -->