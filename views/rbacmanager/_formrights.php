<?php $permissions = \rusbitles\adminbase\models\forms\RbacGroup::getPermissions()?>
<?php $roles = \rusbitles\adminbase\models\forms\RbacGroup::getRoles()?>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>
                <input class="all_check" id="all1" data-aim=".col1" type="checkbox" />
                <label class="control-label" for="all1">Выделить все</label>
            </th>
            <th>
                <input class="all_check" id="all2" data-aim=".col2" type="checkbox" />
                <label class="control-label" for="all2">Выделить все</label>
            </th>
            <th>
                <input class="all_check" id="all3" data-aim=".col3" type="checkbox" />
                <label class="control-label" for="all3">Выделить все</label>
            </th>
        </tr>
    </thead>
    <tbody>
        <? foreach($permissions['table'] as $permissionName => $permissionRow) { ?>
            <tr>
                <? $i = 0; foreach($permissionRow as $operationName => $permission) { $i++; ?>
                    <td>
                        <input class="col<?=$i?>" type="checkbox" name="<?=$model->formName()?>[permissions][]" id="permission-<?=$permission->name?>" value="<?=$permission->name?>"<?=(in_array($permission->name, $model->permissions)?' checked':'')?>/>
                        <label class="control-label" for="permission-<?=$permission->name?>"><?=$permission->description?> (<?=$permission->name?>)</label>
                    </td>
                <? } ?>
            </tr>
        <? } ?>
        <tr><td>Другие права</td></tr>
        <? foreach($permissions['common'] as $k => $permission) { ?>
            <tr>
                <td>
                    <input type="checkbox" name="<?=$model->formName()?>[permissions][]" id="permission-<?=$permission->name?>" value="<?=$permission->name?>"<?=(in_array($permission->name, $model->permissions)?' checked':'')?>/>
                    <label class="control-label" for="permission-<?=$permission->name?>"><?=$permission->description?></label>
                </td>
            </tr>
        <? } ?>
            <tr><td>Другие группы</td></tr>
        <? foreach($roles as $k => $role) { ?>
            <tr>
                <td>
                    <input type="checkbox" name="<?=$model->formName()?>[roles][]" id="role-<?=$role->name?>" value="<?=$role->name?>"<?=(in_array($role->name, $model->roles)?' checked':'')?>/>
                    <label class="control-label" for="role-<?=$role->name?>"><?=$role->description?></label>
                </td>
            </tr>
        <? } ?>
    </tbody>
</table>