<h1>Группы пользователей</h1>

<div class="btn-group actionButtons">
    <a class="btn btn-default" href="<?=Yii::$app->urlManager->createUrl([$this->context->admin->id . '/rbacmanager/create'])?>"><i class="fa fa-plus"></i> Добавить</a>
</div>

<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Код</th>
            <th>Описание</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <? foreach($permissions as $permission) { ?>
            <tr>
                <td><?=$permission->name?></td>
                <td><?=$permission->description?></td>
                <td>
                    <a href="<?=Yii::$app->urlManager->createUrl([$this->context->admin->id . '/rbacmanager/create', 'name' => $permission->name])?>"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="<?=Yii::$app->urlManager->createUrl([$this->context->admin->id . '/rbacmanager/delete', 'name' => $permission->name])?>" data-confirm="Вы точно хотите удалить запись?"><span class="glyphicon glyphicon-trash"></span></a>
                </td>
            </tr>
        <? } ?>
    </tbody>
</table>