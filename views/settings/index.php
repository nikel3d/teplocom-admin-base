<?php
use rusbitles\adminbase\helpers\BImages;
use yii\helpers\Html;

$fields = [
        'id',
        'code',
        'title',
        [
            'attribute' => 'value',
            "content" => function ($data) {
                if ($data->type == 4 && $data->model_classname != '') {
                    $cn = 'app\\models\\' . $data->model_classname;
                    return $cn::findOne(['id' => $data->value])->title;
                } elseif ($data->type == 3) {
                    if (is_file($_SERVER['DOCUMENT_ROOT'] . $data->value_file)) {
                        if (in_array(pathinfo($_SERVER['DOCUMENT_ROOT'] . $data->value_file)['extension'],
                            ['gif', 'jpeg', 'jpg', 'png'])) {
                            return Html::img(BImages::doProp($data->value_file, 100, 100));
                        } else {
                            return Html::a('Ссылка', $data->value_file);
                        }
                    }
                } else {
                    return $data->value;
                }
            },
        ],
        'weight',
    ];
?>

<?=$this->render('@rusbitles/adminbase/views/base/index', ['fields' => $fields, 'dataProvider' => $dataProvider, 'item' => $item])?>
