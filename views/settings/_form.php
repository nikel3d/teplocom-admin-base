<?php
use rusbitles\adminbase\helpers\AdminTools;
use rusbitles\adminbase\models\Settings;
?>

<?=$form->field($model, 'title')?>
<?=$form->field($model, 'code')?>

<?php if($model->type == 4) { $cn = 'app\\models\\'.$model->model_classname; if(class_exists($cn)) { ?>
    <?=AdminTools::generateSelect2($model, $form, "value", $cn::getDropDownList(), false)?>
<?php } } elseif($model->type == 3) { ?>
    <?=AdminTools::generateFileInputs($model, $form, 'value_file')?>
<?php } elseif($model->type == 2) { ?>
    <?=AdminTools::generateRedactor($model, $form, 'value')?>
<?php } elseif($model->type == 1) { ?>
    <?=$form->field($model, 'value')->textarea()?>
<?php } ?>

<?=$form->field($model, 'type')->dropDownList(Settings::$types)?>
<?php if(in_array($model->type, [4])) { ?>
    <?=$form->field($model, 'model_classname')?>
<?php } ?>
<?=$form->field($model, 'weight')?>