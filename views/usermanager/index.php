<?php
use rusbitles\adminbase\helpers\BImages;
use rusbitles\adminbase\models\User;
use yii\helpers\Html;

$fields = [
        'id',
        'username',
        [
            'attribute' => 'fullname_search',
            'value' => function ($model) {
                return $model->fullName;
            },
        ],
        'email',
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Права доступа',
            'template' => '{rights}',
            'buttons' => [
                'rights' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-user"></span>', Yii::$app->urlManager->createUrl([$this->context->admin->id . '/rbacmanager/rights', 'uid' => $model->id]));
                },
            ],
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Войти под пользователем',
            'template' => '{spyauthorize}',
            'buttons' => [
                'spyauthorize' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-log-in"></span>', Yii::$app->urlManager->createUrl([$this->context->admin->id . '/usermanager/spyauth', 'uid' => $model->id]), ['data-confirm' => Yii::t('yii', 'Вы точно хотите авторизоваться под этим пользователем?')]);
                },
            ],
        ],
        'author',
        [
            'attribute' => 'soc_array',
            'label' => 'Социалки',
            'value' => function ($model) {
                return ($model->soc_array == '')?'Нет':'Да';
            },
        ],
        'public',
    ];
?>

<?=$this->render('@rusbitles/adminbase/views/base/index', ['fields' => $fields, 'dataProvider' => $dataProvider, 'item' => $item])?>
