<?php
use rusbitles\adminbase\helpers\AdminTools;

$cn = Yii::$app->user->identityClass;
?>

<?php if($model->isNewRecord) { ?>
    <?=$form->field($model, 'username')?>
<?php } ?>
<?=$form->field($model, 'newPassword')->passwordInput()?>
<?=$form->field($model, 'email')?>
<?=$form->field($model, 'name')?>
<?=$form->field($model, 'last_name')?>
<?=$form->field($model, 'second_name')?>
<?=AdminTools::generateImageInputs($model, $form, 'image')?>
<?=$form->field($model, 'public')->checkbox([], false)?>