<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="page">
    <div class="page-container">
        <div class="page-content">
            <?php if (!$success) { ?>
                <p>Ошибка!</p>
            <?php } else { ?>
                <p>Вы успешно подтвердили регистрацию! Вы автоматически авторизованы на сайте!</p>
            <?php } ?>
        </div>
    </div>
</div>