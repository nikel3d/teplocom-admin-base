<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="page">
    <div class="page-container">
        <div class="page-content">
            <?php if (!$success) { ?>
                <?php $form = ActiveForm::begin([
                    'id' => 'changepass-form',
                    'options' => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                        'labelOptions' => ['class' => 'col-lg-1 control-label'],
                    ],
                ]); ?>
                <?php if($form->errorSummary($model)) { ?>
                    <div>
                        <?=$form->errorSummary($model)?>
                    </div>
                <?php } ?>
                <label class="label-text">
                    <span>Пароль<?=$model->isAttributeRequired('password')?' *':''?></span>
                    <?=Html::input('password', 'ChangepassForm[password]', $model->password)?>
                </label>
                <label class="label-text">
                    <span>Повторите пароль<?=$model->isAttributeRequired('confirmPassword')?' *':''?></span>
                    <?=Html::input('password', 'ChangepassForm[confirmPassword]', $model->confirmPassword)?>
                </label>
                <?=Html::submitButton('Смена пароля', ['class' => 'lk-save', 'name' => 'changepass-button']) ?>
                <?php ActiveForm::end(); ?>
            <?php } else { ?>
                <p>Пароль успешно изменен!</p>
            <?php } ?>
        </div>
    </div>
</div>