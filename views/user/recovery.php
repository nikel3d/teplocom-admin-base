<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="page">
    <div class="page-container">
        <div class="page-content">
            <?php if (!$success) { ?>
            <?php $form = ActiveForm::begin([
                'id' => 'recovery-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
            ]); ?>
                <?php if($form->errorSummary($model)) { ?>
                    <div>
                        <?=$form->errorSummary($model)?>
                    </div>
                <?php } ?>
                <label class="label-text">
                    <span>Имя пользователя<?=$model->isAttributeRequired('username')?' *':''?></span>
                    <?=Html::input('text', 'RecoveryForm[username]', $model->username)?>
                </label>
                <label class="label-text">
                    <span>Ваш Email<?=$model->isAttributeRequired('email')?' *':''?></span>
                    <?=Html::input('text', 'RecoveryForm[email]', $model->email)?>
                </label>
                <?=Html::submitButton('Восстановить', ['class' => 'lk-save', 'name' => 'recovery-button']) ?>
            <?php ActiveForm::end(); ?>
            <?php } else { ?>
               <p>Спасибо! На Вашу почту было отправлено письмо с дальнейшими указаниями.</p>
            <?php } ?>
        </div>
    </div>
</div>