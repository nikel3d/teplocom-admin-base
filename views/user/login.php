<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use rmrevin\yii\ulogin\ULogin;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin([
    "enableAjaxValidation" => false,
    "enableClientValidation" => false,
    "fieldConfig" => [
        "options" => [
            'class' => 'row',
        ],
        "template" => "<div class='col-sm-2 text-right'>{label}</div><div class='col-sm-8'>{input}\n{hint}\n{error}</div>",
    ],
    "fieldClass" => 'rusbitles\adminbase\classes\RBField',
]); ?>
    <?php if($form->errorSummary($model)) { ?>
        <div>
            <?=$form->errorSummary($model)?>
        </div>
    <?php } ?>
    <?=$form->field($model, "username")?>
    <?=$form->field($model, "password")->passwordInput()?>
    <?=$form->field($model, "rememberMe")->checkbox()?>

    <div class='col-sm-offset-2'>
        <p>Авторизация через социальные сети: </p>
        <p>
            <?=ULogin::widget([
                // widget look'n'feel
                'display' => ULogin::D_PANEL,

                // required fields
                'fields' => [ULogin::F_FIRST_NAME, ULogin::F_LAST_NAME, ULogin::F_EMAIL, ULogin::F_PHOTO_BIG],

                // optional fields
                'optional' => [ULogin::F_BDATE],

                // login providers
                'providers' => [ULogin::P_VKONTAKTE, ULogin::P_FACEBOOK, ULogin::P_TWITTER, ULogin::P_GOOGLE],

                // login providers that are shown when user clicks on additonal providers button
                'hidden' => [],

                // where to should ULogin redirect users after successful login
                'redirectUri' => Yii::$app->urlManager->createUrl([$this->context->admin->id . '/user/ulogin']),

                // optional params (can be ommited)
                // force widget language (autodetect by default)
                'language' => ULogin::L_RU,

                // providers sorting ('relevant' by default)
                'sortProviders' => ULogin::S_RELEVANT,

                // verify users' email (disabled by default)
                'verifyEmail' => '0',

                // mobile buttons style (enabled by default)
                'mobileButtons' => '1',
            ])?>
        </p>
    </div>

    <div class="col-sm-offset-2">
        <?=Html::submitButton('Войти', ['class' => 'btn']) ?>
        <p><a href="<?=Yii::$app->urlManager->createUrl(['base/user/recovery'])?>">Забыли пароль?</a></p>
    </div>
<?php ActiveForm::end(); ?>