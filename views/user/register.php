<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="page">
    <div class="page-container">
        <div class="page-content">
            <?php if (!$success) { ?>
            <?php $form = ActiveForm::begin([
                'id' => 'register-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
            ]); ?>
                <?php if($form->errorSummary($model)) { ?>
                    <div>
                        <?=$form->errorSummary($model)?>
                    </div>
                <?php } ?>
                <label class="label-text">
                    <span>Имя пользователя<?=$model->isAttributeRequired('username')?' *':''?></span>
                    <?=Html::input('text', 'RegisterForm[username]', $model->username)?>
                </label>
                <label class="label-text">
                    <span>Ваш Email<?=$model->isAttributeRequired('email')?' *':''?></span>
                    <?=Html::input('text', 'RegisterForm[email]', $model->email)?>
                </label>
                <label class="label-text">
                    <span>Пароль<?=$model->isAttributeRequired('password')?' *':''?></span>
                    <?=Html::input('password', 'RegisterForm[password]', $model->password)?>
                </label>
                <label class="label-text">
                    <span>Повторите пароль<?=$model->isAttributeRequired('confirmPassword')?' *':''?></span>
                    <?=Html::input('password', 'RegisterForm[confirmPassword]', $model->confirmPassword)?>
                </label>
                <?php/*<label class="label-text">
                    <span>Ваше имя<?=$model->isAttributeRequired('name')?' *':''?></span>
                    <?=Html::input('text', 'RegisterForm[name]', $model->name)?>
                </label>*/?>
                <?=Html::submitButton('Регистрация', ['class' => 'lk-save', 'name' => 'register-button']) ?>
            <?php ActiveForm::end(); ?>
            <?php } else { ?>
               <p>Спасибо! Вы успешно зарегистрированы на сайте! На Вашу почту было отправлено письмо с ссылкой для активизации.</p>
            <?php } ?>
        </div>
    </div>
</div>