<?php
use rusbitles\adminbase\assets\AdminAsset;

$bundle = AdminAsset::register($this);
?>
<?php $this->beginContent('@rusbitles/adminbase/views/layouts/gentelella_base.php'); ?>
    <div role="main">
        <br/>
        <div class="col-md-4 col-xs-12 col-md-offset-4">
            <div class="x_panel">
                <div class="x_title">
                    <h2><?=$this->context->h1title?></h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?=$content?>
                </div>
            </div>

        </div>
    </div>
<?php $this->endContent(); ?>