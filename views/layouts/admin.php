<?php

use rusbitles\adminbase\Admin;
use rusbitles\adminbase\assets\AdminAsset;
use yii\helpers\Html;
use yii\jui\Accordion;

/**
 * @var \rusbitles\adminbase\Admin $module
 */

AdminAsset::register($this);

$module = Admin::getInstance();
$menu = $module->getMenuWidget();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
    <head>
        <?=Html::csrfMetaTags()?>
        <?php $this->head() ?>
    </head>

    <body>
        <?php $this->beginBody() ?>

        <div class="pageContainer">
            <div class="row">
                <div class="col-xs-2">
                    <a class="logo" href="/">
                        <img src="<?=$module->logo?>">
                        <?=$module->title?>
                    </a>
                    <?=Accordion::widget([
                        'items' => $menu,
                        'clientOptions' => ['collapsible' => true, 'active' => true],
                    ]);?>
                </div>
                <div class="col-xs-10">
                    <?=$content?>
                </div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
