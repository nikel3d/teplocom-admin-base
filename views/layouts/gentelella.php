<?php
use rusbitles\adminbase\Admin;
use rusbitles\adminbase\assets\AdminAsset;
use rusbitles\adminbase\helpers\BImages;
use yii\helpers\Url;
use yiister\gentelella\widgets\Menu;

$bundle = AdminAsset::register($this);
$menu = $this->context->admin->treeMenu;
$module = Admin::getInstance();
?>
<?php $this->beginContent('@rusbitles/adminbase/views/layouts/gentelella_base.php'); ?>
    <div class="col-md-3 left_col">
        <div class="left_col scroll-view">

            <div class="navbar nav_title" style="border: 0;">
                <a href="/" class="site_title"><img src="<?=BImages::doProp(($this->context->admin->logo=='')?$bundle->baseUrl.'/images/logo.png':$this->context->admin->logo, 48, 48)?>"> <span><?=$this->context->admin->title?></span></a>
            </div>
            <div class="clearfix"></div>

            <!-- menu prile quick info -->
            <div class="profile">
                <div class="profile_pic">
                    <img src="<?=BImages::doCrop(Yii::$app->user->identity->image, 128, 128)?>" class="img-circle profile_img">
                </div>
                <div class="profile_info">
                    <span>Добро пожаловать,</span>
                    <h2><?=Yii::$app->user->identity->fullName?></h2>
                </div>
            </div>
            <!-- /menu prile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                <div class="menu_section">
                    <h3>Меню</h3>
                    <?=Menu::widget(["items" => $menu]) ?>
                </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                <?php/*<a data-toggle="tooltip" data-placement="top" title="Settings">
                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                    <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="Lock">
                    <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                </a>*/?>
                <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?=Yii::$app->urlManager->createUrl([$module->id.'/user/logout'])?>">
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a>
            </div>
            <!-- /menu footer buttons -->
        </div>
    </div>

    <!-- top navigation -->
    <div class="top_nav">

        <div class="nav_menu">
            <nav class="" role="navigation">
                <div class="nav toggle">
                    <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                </div>

                <ul class="nav navbar-nav navbar-right">
                    <li class="">
                        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <img src="<?=BImages::doCrop(Yii::$app->user->identity->image, 128, 128)?>" alt=""><?=Yii::$app->user->identity->fullName?>
                            <span class=" fa fa-angle-down"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-usermenu pull-right">
                            <?php/*<li><a href="javascript:;">  Profile</a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <span class="badge bg-red pull-right">50%</span>
                                    <span>Settings</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">Help</a>
                            </li>*/?>
                            <li>
                                <a href="<?=Yii::$app->urlManager->createUrl([$module->id.'/user/logout'])?>"><i class="fa fa-sign-out pull-right"></i> Выйти</a>
                            </li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="<?=Url::base(true) . "/product_feed.yml"?>" target="_blank">product_feed.yml</a>
                    </li>

<?php/*<li role="presentation" class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-envelope-o"></i>
                            <span class="badge bg-green">6</span>
                        </a>
                        <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                            <li>
                                <a>
              <span class="image">
                                <img src="http://placehold.it/128x128" alt="Profile Image" />
                            </span>
              <span>
                                <span>John Smith</span>
              <span class="time">3 mins ago</span>
              </span>
              <span class="message">
                                Film festivals used to be do-or-die moments for movie makers. They were where...
                            </span>
                                </a>
                            </li>
                            <li>
                                <a>
              <span class="image">
                                <img src="http://placehold.it/128x128" alt="Profile Image" />
                            </span>
              <span>
                                <span>John Smith</span>
              <span class="time">3 mins ago</span>
              </span>
              <span class="message">
                                Film festivals used to be do-or-die moments for movie makers. They were where...
                            </span>
                                </a>
                            </li>
                            <li>
                                <a>
              <span class="image">
                                <img src="http://placehold.it/128x128" alt="Profile Image" />
                            </span>
              <span>
                                <span>John Smith</span>
              <span class="time">3 mins ago</span>
              </span>
              <span class="message">
                                Film festivals used to be do-or-die moments for movie makers. They were where...
                            </span>
                                </a>
                            </li>
                            <li>
                                <a>
              <span class="image">
                                <img src="http://placehold.it/128x128" alt="Profile Image" />
                            </span>
              <span>
                                <span>John Smith</span>
              <span class="time">3 mins ago</span>
              </span>
              <span class="message">
                                Film festivals used to be do-or-die moments for movie makers. They were where...
                            </span>
                                </a>
                            </li>
                            <li>
                                <div class="text-center">
                                    <a href="/">
                                        <strong>See All Alerts</strong>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>*/?>

                </ul>
            </nav>
        </div>

    </div>

    <div class="right_col" role="main">
        <?php if (isset($this->params['h1'])): ?>
            <div class="page-title">
                <div class="title_left">
                    <h1><?= $this->params['h1'] ?></h1>
                </div>
                <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                        <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="clearfix"></div>

        <?= $content ?>
    </div>

    <footer>
        <div class="pull-right">
            Rusbitles admin
        </div>
        <div class="clearfix"></div>
    </footer>
<?php $this->endContent(); ?>