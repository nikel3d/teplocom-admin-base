<?php
use rusbitles\adminbase\Admin;
use yii\helpers\Html;
use app\models\Structure;

$fields = [
    "id",
	[
		'attribute' => 'controller',
		'content' => function($data) {
			if (!empty(Admin::$allControllers[$data->controller])) return Admin::$allControllers[$data->controller];
			return '(empty)';
		}
	],
	'url',
	'weight',
];
?>
<?=$this->render("@rusbitles/adminbase/views/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>