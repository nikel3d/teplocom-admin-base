<?php
use rusbitles\adminbase\Admin;
use rusbitles\adminbase\helpers\AdminTools;
?>

<?=$form->field($model, "controller")->dropDownUniqueList(Admin::$allControllers)?>
<?=$form->field($model, "url")?>
<?=$form->field($model, "h1_title")?>
<?=$form->field($model, "seo_title")?>
<?=$form->field($model, "seo_description")->textarea()?>
<?=$form->field($model, "seo_keywords")?>

<?=$form->field($model, "og_title")?>
<?=$form->field($model, "og_description")->textarea()?>
<?=AdminTools::generateImageInputs($model, $form, "og_image")?>

<?=$form->field($model, "weight")?>