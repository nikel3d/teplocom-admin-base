<?php
use rusbitles\adminbase\Admin;

$module = Admin::getInstance();
?>

<p><?=$exception->getMessage()?></p>
<?php if ($exception->statusCode == 403) { ?>
    <p>
        <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?=Yii::$app->urlManager->createUrl([$module->id.'/user/logout'])?>">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span> Сменить пользователя
        </a>
    </p>
<?php } ?>