<h1>Приветствуем Вас в админке!</h1>

<?php if($parserLogs):?>
  <div style="margin-top:15px;">
    <h2>Результаты парсинга из 1С</h2>
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>№</th>
          <th>Время</th>
          <th>Сообщение</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($parserLogs as $parserLog):?>
          <tr>
            <td><?= $parserLog['ID'] ?></td>
            <td><?= date("d.m.Y H:i", $parserLog['date_start']) ?></td>
            <td><?= $parserLog['info'] ?></td>
          </tr>
        <?php endforeach;?>
      </tbody>
    </table>
  </div>
<?php endif;?>
