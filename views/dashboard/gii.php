<?php use yii\helpers\Html;
use yii\widgets\ActiveForm; ?>

<?php if ($createdTables) { ?>
    <div class="form-group">
        <?php foreach ($createdTables as $ct) { ?>
        <form action="/gii/andCrud" method="POST">
            <?php
            $form = ActiveForm::begin(['action' => '/gii/andCrud','method' => 'POST']);
            ?>
            <?=generateInputs(json_decode($ct->post, true), 'Generator'); ?>
            <button type="submit" name="preview" value=""><?=$ct->table?></button>
            <?php $form->end() ?>
            <?php } ?>
    </div>
<?php } ?>

<?php
function generateInputs($post, $prevVar = '') {
    $res = '';
    foreach($post as $pKey => $pVal) {
        if (is_array($pVal)) {
            if ($prevVar == '') {
                $res .= generateInputs($pVal, $pKey);
            } else {
                $res .= generateInputs($pVal, $prevVar . '[' . $pKey . ']');
            }
        } else {
            if ($prevVar == '') {
                $res .= '<input type="hidden" name="' . $pKey . '" value="' . $pVal . '" />';
            } else {
                $res .= '<input type="hidden" name="' . $prevVar . '[' . $pKey . ']" value="' . $pVal . '" />';
            }
        }
    }
    return $res;
}