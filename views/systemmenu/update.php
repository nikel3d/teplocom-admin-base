<?php
use rusbitles\adminbase\models\TreeForm;

if ($tabs[0]['active']) {
        $content .= Yii::$app->controller->renderPartial('_tree_form', array('model' => $model, 'treeForm' => new TreeForm()));
    }
?>

<?=$this->render("@rusbitles/adminbase/views/base/update", ["model" => $model, "tabs" => $tabs, "content" => $content])?>