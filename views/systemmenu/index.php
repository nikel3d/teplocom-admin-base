<?php
$fields = [
    "id",
	[
        "attribute"  => "public",
        "content" => function ($data) {
            return $data->public>0?"+":"-";
        },
        "filter" => [1 => "Активные", 0 => "Неактивные"],
    ],
	'title',
	'url',
    [
        "attribute"  => "types",
        "content" => function ($data) {
            return $data->implodeTypes;
        },
        "filter" => [1 => "Активные", 0 => "Неактивные"],
    ],
];
?>
<?=$this->render("@rusbitles/adminbase/views/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>