<?php
    $lastLvl = 1;
?>

<h1><?=$this->context->modelTitles[0]?>: Дерево</h1>

<?php if ($this->context->getButtons()) { ?>
    <div class="btn-group actionButtons">
        <?php foreach($this->context->getButtons() as $k => $v) { ?>
            <a class="btn btn-default" href="<?=$v['url']?>"><i class="fa fa-<?=$v['icon']?>"></i> <?=$v['title']?></a>
        <?php } ?>
    </div>
<?php } ?>

<ul class="content-tree">
    <?php foreach ($tree as $treeElement) { ?>
        <?php if($lastLvl < $treeElement->depth) { echo str_repeat('<ul>', 1); } ?>
        <?php if($lastLvl > $treeElement->depth) { echo str_repeat('</ul>', $lastLvl - $treeElement->depth); } ?>
        <li class="<?=($treeElement->public==1)?'':'disabled'?>">
            <a href="<?=Yii::$app->urlManager->createUrl(['admin/'.$this->context->mainmenuHandle.'/update', 'id' => $treeElement->id])?>">
                <?=$treeElement->title?> [<?=$treeElement->url?>] [<?=$treeElement->implodeTypes?>]
            </a>
        </li>
    <?php $lastLvl = $treeElement->depth; } ?>

    <?php if($lastLvl > 1) { echo str_repeat('</ul>', $lastLvl - 1); } ?>
</ul>