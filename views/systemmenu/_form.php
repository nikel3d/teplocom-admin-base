<?php
use rusbitles\adminbase\helpers\AdminTools;
use rusbitles\adminbase\models\MenuType;
?>

<?=$form->field($model, "title")?>
<?=AdminTools::generateSelect2($model, $form, "types", MenuType::getDropDownList(), true)?>
<?=$form->field($model, "url")?>
<?=$form->field($model, "in_menu")->checkbox([], false)?>
<?=$form->field($model, "in_breadcrumbs")->checkbox([], false)?>
<?=$form->field($model, "target")->checkbox([], false)?>
<?=$form->field($model, "public")->checkbox([], false)?>
<?=$form->field($model, "class")?>
<?php if($this->context->action->id == 'create') { ?>
    <?php $mn = get_class($this->context); $mn = $mn::$fullModelName; ?>
    <?=$form->field($model, 'tree_parent_id')->dropDownList($mn::getDropDownList())?>
<?php } ?>