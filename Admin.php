<?php

namespace rusbitles\adminbase;

use rusbitles\adminbase\models\Menu;
use rusbitles\adminbase\models\Structure;
use Yii;
use yii\base\BootstrapInterface;
use yii\base\Module;

class Admin extends Module implements BootstrapInterface {
    public $title = 'RB ADMIN';
    public $logo = '';
    public $admin_path = 'admin';
    public $adminEmail = 'admin@alente.ru';
    public $debug = false;

    public $mainMenu = [
        'site' => [
            'parent' => false,
            'title' => 'Общие',
            'url' => '#',
            'weight' => 10000,
        ],
        'site_structure' => [
            'parent' => 'site',
            'title' => 'Структура',
            'url' => '/structure',
            'icon' => 'sitemap',
            'weight' => 10,
        ],
        'site_structure_menu' => [
            'parent' => 'site',
            'title' => 'Меню',
            'url' => '/systemmenu',
            'icon' => 'sitemap',
            'weight' => 30,
        ],
        'site_usersblock' => [
            'parent' => 'site',
            'title' => 'Пользователи',
            'url' => '#',
            'icon' => 'users',
            'weight' => 40,
        ],
        'site_users' => [
            'parent' => 'site_usersblock',
            'title' => 'Список пользователей',
            'url' => '/usermanager',
            'weight' => 10,
        ],
        'site_groups' => [
            'parent' => 'site_usersblock',
            'title' => 'Группы пользователей',
            'url' => '/rbacmanager',
            'weight' => 20,
        ],
        'site_files' => [
            'parent' => 'site',
            'title' => 'Файловый менеджер',
            'url' => '/files',
            'icon' => 'file',
            'weight' => 50,
        ],
        'site_settings' => [
            'parent' => 'site',
            'title' => 'Настройки',
            'url' => '/settings?current[module]=site',
            'icon' => 'gear',
            'weight' => 60,
        ],
    ];
    private static $menu = false;
    private static $activeMenu = false;
    private static $publicMenu = false;
    public static $allControllers = [];
    
    public function init()
    {
        $baseModels = [
            'settings',
            'user',
            'structure',
            'systemmenu',
            'menutype',
            'files',
            'usermanager',
            'system',
            'rbacmanager'
        ];

        $drules = [];
        if ($this->debug) {
            $this->mainMenu['develop'] = [
                'parent' => false,
                'title' => 'Инструменты разработки',
                'url' => '#',
                'weight' => 11000,
            ];

            $this->mainMenu['site_menu_type'] = [
                'parent' => 'develop',
                'title' => 'Типы меню',
                'url' => '/menutype',
                'icon' => '',
                'weight' => 5,
            ];

            $this->mainMenu['develop_gii'] = [
                'parent' => 'develop',
                'title' => 'Генератор кода',
                'url' => '#',
                'weight' => 10,
            ];

            $this->mainMenu['gii_main'] = [
                'parent' => 'develop_gii',
                'title' => 'Генератор кода',
                'url' => '/gii',
                'weight' => 10,
            ];

            $this->mainMenu['gii_start'] = [
                'parent' => 'develop_gii',
                'title' => 'Созданные модели',
                'url' => '/gii_start',
                'weight' => 20,
            ];

            $drules['/autodeploy'] = $this->id . '/ajax/autodeploy';
            $drules['/ulogin'] = $this->id . '/user/ulogin';
        }

        $bm = implode('|', $baseModels);

        // Формируем список роутов
        if ($this->debug || ($rules = Yii::$app->cache->get('rb_admin_rules')) === false) {
            $rules = [
                '<module:elfinder>/<controller:[-\w]+>'                                                         => '<module>/<controller>',
                '<module:elfinder>/<controller:[-\w]+>/<action:[-\w]+>'                                         => '<module>/<controller>/<action>',
                
                '/api/dropdownlist'                                                                             => $this->id . '/ajax/dropdownlist',

                $this->admin_path . '/gii_start'                                                                => $this->id . '/dashboard/gii',
                $this->admin_path . '/gii'                                                                      => $this->id . '/gii/index',
                $this->admin_path . '/login'                                                                    => $this->id . '/user/login',
                $this->admin_path . '/logout'                                                                   => $this->id . '/user/logout',
                $this->admin_path . '/<controller:(' . $bm . ')>/id<id:\d+>'                                    => $this->id . '/<controller>/index',
                $this->admin_path . '/<controller:(' . $bm . ')>/parent<parent:\d+>'                            => $this->id . '/<controller>/index',
                $this->admin_path . '/<controller:(' . $bm . ')>'                                               => $this->id . '/<controller>/index',
                $this->admin_path . '/<controller:(' . $bm . ')>/<action:\w+>/id<id:\d+>/tab<tab:\w+>'          => $this->id . '/<controller>/<action>',
                $this->admin_path . '/<controller:(' . $bm . ')>/<action:\w+>/id<id:\d+>/pback<pback:\d>'       => $this->id . '/<controller>/<action>',
                $this->admin_path . '/<controller:(' . $bm . ')>/<action:\w+>/id<id:\d+>'                       => $this->id . '/<controller>/<action>',
                $this->admin_path . '/<controller:(' . $bm . ')>/<action:\w+>/parent<parent:\d+>/tab<tab:\w+>'  => $this->id . '/<controller>/<action>',
                $this->admin_path . '/<controller:(' . $bm . ')>/<action:\w+>/parent<parent:\d+>'               => $this->id . '/<controller>/<action>',
                $this->admin_path . '/<controller:(' . $bm . ')>/<action:\w+>'                                  => $this->id . '/<controller>/<action>',

                $this->admin_path . '/<controller:\w+>/id<id:\d+>'                                              => 'admin/<controller>/index',
                $this->admin_path . '/<controller:\w+>/parent<parent:\d+>'                                      => 'admin/<controller>/index',
                $this->admin_path . '/<controller:\w+>'                                                         => 'admin/<controller>/index',
                $this->admin_path . '/<controller:\w+>/<action:\w+>/id<id:\d+>/tab<tab:\w+>'                    => 'admin/<controller>/<action>',
                $this->admin_path . '/<controller:\w+>/<action:\w+>/id<id:\d+>/pback<pback:\d>'                 => 'admin/<controller>/<action>',
                $this->admin_path . '/<controller:\w+>/<action:\w+>/id<id:\d+>'                                 => 'admin/<controller>/<action>',
                $this->admin_path . '/<controller:\w+>/<action:\w+>/parent<parent:\d+>/tab<tab:\w+>'            => 'admin/<controller>/<action>',
                $this->admin_path . '/<controller:\w+>/<action:\w+>/parent<parent:\d+>'                         => 'admin/<controller>/<action>',
                $this->admin_path . '/<controller:\w+>/<action:\w+>'                                            => 'admin/<controller>/<action>',
                $this->admin_path . ''                                                                          => $this->id . '/dashboard/index',
                '/sitemap<page:\d+>.xml'                                                                        => $this->id . '/sitemap/index',
                '/sitemap.xml'                                                                                  => $this->id . '/sitemap/index',
            ];

            $structures = Structure::find()->ordered()->all();
            foreach ($structures as $structure) {
                $controllerName = explode('_', $structure->controller);
                
                $controller = 'app\\controllers\\' . $controllerName[0] . 'Controller';

                if (class_exists($controller) && method_exists($controller, 'getRules')) {
                    $controllerRules = $controller::getRules(!empty($controllerName[1])?$controllerName[1]:false);
                    $newRules = [];
                    foreach($controllerRules as $k => $v) {
                        if (is_array($v)) {
                            $newRules[$k] = $v;
                            $pattern = str_replace('[url]', $structure->url, $newRules[$k]['pattern']);
                            $newRules[$k]['pattern'] = ($pattern==$newRules[$k]['pattern'])?($structure->url . $newRules[$k]['pattern']):$pattern;
                        } else {
                            $pattern = str_replace('[url]', $structure->url, $k);
                            $newRules[($pattern == $k)?($structure->url . $k):($pattern)] = $v;
                        }
                    }

                    $rules = array_merge($rules, $newRules);
                }
            }

            Yii::$app->cache->set('rb_admin_rules', $rules, 86400);
        }
        
        // Формируем список названий контроллеров (Публичных)
        if ($this->debug || (self::$allControllers = Yii::$app->cache->get('rb_admin_controllers')) === false) {
            $controller_dir = Yii::getAlias('@app/controllers');
            $dirsToResearch = [$controller_dir];

            while (count($dirsToResearch) > 0) {
                $current_dir = array_shift($dirsToResearch);
                if (file_exists($current_dir)) {
                    $controllers = scandir($current_dir);

                    foreach ($controllers as $controller) {
                        if ($controller != '.' && $controller != '..' && $controller != 'admin') {
                            if(!is_dir($current_dir . '/' . $controller)) {
                                $controllerFullName = 'app\\controllers' .
                                    str_replace(
                                        [$controller_dir, '/'],
                                        ['', '\\'],
                                        $current_dir
                                    ) . '\\' .
                                    str_replace('.php', '', $controller);
                                $controllerName = str_replace(
                                    [$controller_dir . '/', $controller_dir . '\\', $controller_dir, '/'],
                                    ['', '', '', '\\'],
                                    $current_dir
                                );
                                $controllerName = (!empty($controllerName)?$controllerName.'\\':'') . str_replace('Controller.php', '', $controller);

                                if (property_exists($controllerFullName, 'controllerTitle')) {
                                    $cTitle = $controllerFullName::getTitle();
                                    if (!empty($cTitle)) {
                                        if (is_array($cTitle)) {
                                            foreach ($cTitle as $k => $title) {
                                                self::$allControllers[$controllerName . '_' . $k] = $title;
                                            }
                                        } else {
                                            self::$allControllers[$controllerName] = $cTitle;
                                        }
                                    }
                                }
                            } else {
                                $dirsToResearch[] = $current_dir . '/' . $controller;
                            }
                        }
                    }
                }
            }

            Yii::$app->cache->set('rb_admin_controllers', self::$allControllers, 86400);
        }

        Yii::$app->getUrlManager()->addRules($rules, false);
        Yii::$app->getUrlManager()->addRules($drules, false);

        Yii::$app->setModule('gii', [
            'class' => 'yii\gii\Module',
            'generators' => [
                'andCrud' => [
                    'class' => 'rusbitles\adminbase\generators\andCrud\Generator',
                ],
            ],
        ]);

        Yii::$app->setComponents([
            'image' => [
                'class' => 'yii\image\ImageDriver',
                'driver' => 'GD',  //GD or Imagick
            ],
        ]);

        parent::init();
    }

    public function bootstrap($app)
    {
        if ($app instanceof \yii\console\Application) {
            $this->controllerNamespace = 'rusbitles\adminbase\commands';
        }
    }
    
    public function getMenu() {
        if (self::$menu !== false) return self::$menu;
        
        if ($this->debug || (self::$menu = Yii::$app->cache->get('rb_admin_menu')) === false) {
            $menu = [];
            self::$menu = [
                'root' => [
                    'children' => [],
                    'lvl' => 0,
                ]
            ];
            $controller_dir = Yii::getAlias('@app/controllers/admin');
            if (file_exists($controller_dir)) {
                $controllers = scandir($controller_dir);

                foreach ($controllers as $controller) {
                    if ($controller != '.' && $controller != '..' && !is_dir($controller_dir . '/' . $controller)) {
                        $controllerName = 'app\\controllers\\admin\\' . str_replace('.php', '', $controller);

                        if (method_exists($controllerName, 'getMenu')) {
                            $menu = array_merge($menu, $controllerName::getMenu());
                        }
                    }
                }
            }

            $menu = array_merge($menu, $this->mainMenu);

            foreach ($menu as $k => $v) {
                self::$menu[$k] = $v;
                self::$menu[$k]['children'] = [];
                self::$menu[$k]['active'] = false;

                if (!isset(self::$menu[$k]['weight'])) {
                    self::$menu[$k]['weight'] = 0;
                }

                if (isset(self::$menu[$k]['url'])) {
                    if (self::$menu[$k]['url'] != '#') {
                        self::$menu[$k]['url'] = '/' . $this->admin_path . self::$menu[$k]['url'];
                    }
                } else {
                    self::$menu[$k]['url'] = '#';
                }

                if (empty(self::$menu[$k]['parent'])) {
                    self::$menu[$k]['lvl'] = 1;
                } else {
                    self::$menu[$k]['lvl'] = self::$menu[self::$menu[$k]['parent']]['lvl'] + 1;
                }
            }

            uasort(self::$menu, function ($a, $b) {
                if ($a['lvl'] > $b['lvl']) {
                    return 1;
                } elseif ($a['lvl'] < $b['lvl']) {
                    return -1;
                } else {
                    if ($a['weight'] > $b['weight']) {
                        return 1;
                    } elseif ($a['weight'] < $b['weight']) {
                        return -1;
                    }
                }

                return 0;
            });

            foreach (self::$menu as $k => $v) {
                if (!empty(self::$menu[$k]['parent'])) {
                    self::$menu[self::$menu[$k]['parent']]['children'][] = $k;
                } elseif ($k != 'root') {
                    self::$menu['root']['children'][] = $k;
                }
            }

            Yii::$app->cache->set('rb_admin_menu', self::$menu, 86400);
        }

        $auth = Yii::$app->authManager;
        foreach(self::$menu as $handle => $menuItem) {
            if (preg_match('/(.*)_(?:main|settings)/', $handle, $m)) {
                $permission = $auth->getPermission(ucfirst($m[1]) . 'AdminIndex');
                if ($permission && !Yii::$app->user->can($permission->name)) unset(self::$menu[$handle]);
            }
        }

        $this->setMenuActive(self::$menu);

        return self::$menu;
    }

    private function setMenuActive(&$menuTree, $handle = false) {
        if ($handle) {
            $menuTree[$handle]['active'] = true;
            //if ($menuTree[$handle]['parent']) self::setMenuActive($menuTree, $menuTree[$handle]['parent']);
        } else {
            $url = urldecode(Yii::$app->request->url);
            foreach ($menuTree as $k => $v) {
                if (/*!empty($v['url']) && $v['url'] == $url || */(self::$activeMenu === $k)) {
                    self::setMenuActive($menuTree, $k);
                }
            }
        }
    }
    
    public function setMenuHandleActive($handle) {
        self::$activeMenu = $handle;
    }

    public function getMenuWidget() {
        $menu = $this->getMenu();
        $wmenu = [];

        foreach ($menu as $k => $v) {
            if ($v['lvl'] == 1) {
                $li = [
                    'header' => $v['title'],
                    'content' => '',
                    'active' => $v['active'],
                ];

                foreach ($v['children'] as $chKey) {
                    $li['content'] .= '<li><a href="'.$menu[$chKey]['url'].'"><i class="fa fa-'.$menu[$chKey]['icon'].' fa-fw"></i> '.$menu[$chKey]['title'].'</a></li>';
                }

                if (!empty($li['content'])) {
                    $li['content'] = '<ul class="nav" id="'.$k.'">' . $li['content'] . '</ul>';
                }

                $wmenu[] = $li;
            }
        }
        
        return $wmenu;
    }

    public function getTreeMenu($handle = 'root') {
        $menu = $this->getMenu();

        $return_menu = [];

        if (!empty($menu[$handle]['children'])) {
            foreach ($menu[$handle]['children'] as $chH) {
                $row = [
                    "label" => $menu[$chH]['title'],
                    //"url" => $menu[$chH]['url'],
                    //"active" => $menu[$chH]['active'],
                ];

                if (!in_array($menu[$chH]['url'], ['#', ''])) {
                    $row['url'] = $menu[$chH]['url'];
                } else {
                    $row['url'] = 'javascript:void(0)';
                }
                if ($menu[$chH]['active']) $row['active'] = true;

                if (!empty($menu[$chH]['icon'])) $row['icon'] = $menu[$chH]['icon'];
                if (isset($menu[$chH]['success'])) {
                    $row['badge'] = $menu[$chH]['success'];
                    $row['badgeOptions'] = ["class" => "label-success"];
                }

                $items = $this->getTreeMenu($chH);
                if (!empty($items)) {
                    if (count($items) == 1 && $row['url'] == 'javascript:void(0)' && !empty($items[0]['url'])) {
                        $row['url'] = $items[0]['url'];
                        if (!empty($items[0]['items'])) $row['items'] = $items[0]['items'];
                    } else {
                        $row['items'] = $items;
                    }
                }

                if ($row['url'] != 'javascript:void(0)' || count($row['items']) != 0) {
                    $return_menu[] = $row;
                }
            }
        }

        return $return_menu;
    }

    public function getPublicMenu() {
        if (self::$publicMenu === false && ($this->debug || (self::$publicMenu = Yii::$app->cache->get('rb_admin_public_menu')) === false)) {
            $menu = Menu::find()->where(['depth' => 0])->published()->one();

            self::$publicMenu = $this->getPublicMenuRecurse($menu);

            Yii::$app->cache->set('rb_admin_public_menu', self::$publicMenu, 86400);
        }
        //echo '<pre>01';print_r(self::$publicMenu);echo '</pre>';
        return self::$publicMenu;
    }
    
    public function setPublicMenu($menuType, $addMenu) {
        $this->publicMenu;
        if (empty(self::$publicMenu[$menuType])) self::$publicMenu[$menuType] = [];
        self::$publicMenu[$menuType] = array_merge(self::$publicMenu[$menuType], $addMenu);
    }

    public function addPublicMenu($menuType, $key, $addMenu) {
        $this->publicMenu;
        if (empty(self::$publicMenu[$menuType][$key])) self::$publicMenu[$menuType][$key] = [];

        $addedParents = empty(self::$publicMenu[$menuType][$key])?[]:array_merge(self::$publicMenu[$menuType][$key]['parents'], [$key]);
        foreach ($addMenu as $k => $item) {
            if (isset(self::$publicMenu[$menuType][$key]['children']) && count($item['parents']) == 0)
                self::$publicMenu[$menuType][$key]['children'][] = $k;
            $addMenu[$k]['parents'] = array_merge($addedParents, $addMenu[$k]['parents']);
        }

        self::$publicMenu[$menuType] = self::$publicMenu[$menuType] + $addMenu;
    }

    public function setPublicMenuActive($active) {
        $menu = $this->publicMenu;

        //echo '<pre>';print_r(self::$publicMenu);echo '</pre>';

        foreach ($menu as $code => $items) {
            foreach ($items as $k => $item) {
                if ($item['url'] == $active->url) {
                    $ids = array_merge([$k], !empty($item['parents'])?$item['parents']:[]);

                    foreach ($ids as $id) {
                        self::$publicMenu[$code][$id]['active'] = true;
                    }

                    break;
                }
            }
        }
    }

    /**
     * @param Menu $menu
     */
    private function getPublicMenuRecurse($menu, $result = []) {
        //echo '001<pre>';print_r($menu);echo '</pre>'; //[di]
        foreach ($menu->children(1)->all() as $chMenu) {
            $buffRow = [
                'label' => $chMenu->title,
                'url' => $chMenu->url,
                'active' => false,
                'target' => $chMenu->target,
                'class' => $chMenu->class,
                'virtual' => $chMenu->url==''?true:false,
                'children' => [],
                'parents' => [],
            ];

            foreach($chMenu->types as $type) {
                $row = $buffRow;

                if (!empty($result[$type->code][$menu->id])) {
                    $row['parents'] = array_merge([$menu->id], $result[$type->code][$menu->id]['parents']);
                    $result[$type->code][$menu->id]['children'][] = $chMenu->id;
                }

                if ($chMenu->in_menu == 1) {
                    $result[$type->code][$chMenu->id] = $row;
                }
            }

            $result = $this->getPublicMenuRecurse($chMenu, $result);
        }

        if ($menu->depth == 0) {
            foreach ($result as $code => $res) {
                foreach(array_reverse($res, true) as $k => $v) {
                    if ($v['virtual'] && count($v['children']) > 0) {
                        $result[$code][$k]['url'] = $result[$code][current($v['children'])]['url'];
                    }
                }
            }
        }

        return $result;
    }


    //[di]





}