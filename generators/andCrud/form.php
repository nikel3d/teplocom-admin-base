<?php
use app\models\SystemGii;
use yii\helpers\Html;

$filter = ['fields', 'links', 'manyLinks', 'children', 'parents'];
foreach ($filter as $filterItem) {
    if (!is_array($generator->{$filterItem})) $generator->{$filterItem} = [];
    foreach($generator->{$filterItem} as $k => $field) {
        if ($field['name'] == '') {
            unset($generator->{$filterItem}[$k]);
        }
    }
    $generator->{$filterItem} = array_values($generator->{$filterItem});
}


$generator->fields[] = [
    'name' => '',
    'label' => '',
    'required' => '',
    'show' => '',
    'edit' => '',
    'type' => '',
    'class' => ' last',
];
$generator->selects[] = [
    'name' => '',
    'label' => '',
    'variants' => '',
    'required' => '',
    'show' => '',
    'edit' => '',
    'class' => ' last',
];
$generator->links[] = [
    'name' => '',
    'link' => '',
    'classname' => '',
    'label' => '',
    'tmpl' => '[title]',
    'class' => ' last',
];
$generator->manyLinks[] = [
    'name' => '',
    'link' => '',
    'class' => ' last',
];
$generator->children[] = [
    'name' => '',
    'label1' => '',
    'label12' => '',
    'class' => ' last',
];
$generator->parents[] = [
    'name' => '',
    'label' => '',
    'class' => ' last',
];
?>

<style>
    .editTable td {
        padding: 2px 5px;
    }
    .editTable tr.last {
        display: none;
    }
</style>

<div class="form-group">
    <label class="control-label" for="generator-fields" data-original-title="" title="">Поля таблицы</label>
    <table class="fieldTable editTable">
        <?php if (isset($generator->fields)) { ?>
            <tr>
                <td>Поле</td>
                <td>Наименование</td>
                <td>*</td>
                <td>В</td>
                <td>Р</td>
                <td>Тип</td>
                <td></td>
            </tr>
            <?php foreach($generator->fields as $k => $field) { ?>
                <tr class="fieldsRow<?=!empty($field['class'])?$field['class']:''?>" data-key="<?=$k?>">
                    <td><input type="text" class="form-control" name="Generator[fields][<?=$k?>][name]" value="<?=$field['name']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[fields][<?=$k?>][label]" value="<?=$field['label']?>"/></td>
                    <td><input type="checkbox" name="Generator[fields][<?=$k?>][required]"<?=!empty($field['required'])?' checked':''?>/></td>
                    <td><input type="checkbox" name="Generator[fields][<?=$k?>][show]"<?=!empty($field['show'])?' checked':''?>/></td>
                    <td><input type="checkbox" name="Generator[fields][<?=$k?>][edit]"<?=!empty($field['edit'])?' checked':''?>/></td>
                    <td>
                        <select class="form-control" name="Generator[fields][<?=$k?>][type]"/>
                            <?php foreach($generator->types as $tK => $type) { ?>
                                <option value="<?=$tK?>"<?=($tK==$field['type'])?' selected':''?>><?=$type[1]?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <td><div class="btn btn-default trDel">У</div></td>
                </tr>
            <?php } ?>
        <?php } ?>
    </table>
</div>
<div class="form-group">
    <div class="btn btn-primary fieldAdd btnAdd">Добавить поле</div>
</div>



<div class="form-group">
    <label class="control-label" for="generator-fields" data-original-title="" title="">Поле select</label>
    <table class="fieldTable editTable">
        <?php if (isset($generator->selects)) { ?>
            <tr>
                <td>Поле</td>
                <td>Наименование</td>
                <td>Варианты</td>
                <td>*</td>
                <td>В</td>
                <td>Р</td>
                <td></td>
            </tr>
            <?php foreach($generator->selects as $k => $select) { ?>
                <tr class="fieldsRow<?=!empty($select['class'])?$select['class']:''?>" data-key="<?=$k?>">
                    <td><input type="text" class="form-control" name="Generator[selects][<?=$k?>][name]" value="<?=$select['name']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[selects][<?=$k?>][label]" value="<?=$select['label']?>"/></td>
                    <td><textarea class="form-control" name="Generator[selects][<?=$k?>][variants]"><?=$select['variants']?></textarea></td>
                    <td><input type="checkbox" name="Generator[selects][<?=$k?>][required]"<?=!empty($select['required'])?' checked':''?>/></td>
                    <td><input type="checkbox" name="Generator[selects][<?=$k?>][show]"<?=!empty($select['show'])?' checked':''?>/></td>
                    <td><input type="checkbox" name="Generator[selects][<?=$k?>][edit]"<?=!empty($select['edit'])?' checked':''?>/></td>
                    <td><div class="btn btn-default trDel">У</div></td>
                </tr>
            <?php } ?>
        <?php } ?>
    </table>
</div>
<div class="form-group">
    <div class="btn btn-primary fieldAdd btnAdd">Добавить</div>
</div>



<div class="form-group">
    <label class="control-label" for="generator-fields" data-original-title="" title="">Связь многое к одному (var_id)</label>
    <table class="fieldTable editTable">
        <?php if (isset($generator->links)) { ?>
            <tr>
                <td>Поле (var)</td>
                <td>Связующее поле (var_id)</td>
                <td>Класс связанной модели</td>
                <td>Наименование</td>
                <td>Шаблон</td>
                <td></td>
            </tr>
            <?php foreach($generator->links as $k => $link) { ?>
                <tr class="fieldsRow<?=!empty($link['class'])?$link['class']:''?>" data-key="<?=$k?>">
                    <td><input type="text" class="form-control" name="Generator[links][<?=$k?>][name]" value="<?=$link['name']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[links][<?=$k?>][link]" value="<?=$link['link']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[links][<?=$k?>][classname]" value="<?=$link['classname']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[links][<?=$k?>][label]" value="<?=$link['label']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[links][<?=$k?>][tmpl]" value="<?=$link['tmpl']?>"/></td>
                    <td><div class="btn btn-default trDel">У</div></td>
                </tr>
            <?php } ?>
        <?php } ?>
    </table>
</div>
<div class="form-group">
    <div class="btn btn-primary fieldAdd btnAdd">Добавить связь</div>
</div>




<div class="form-group">
    <label class="control-label" for="generator-fields" data-original-title="" title="">Связь многое ко многим</label>
    <table class="fieldTable editTable">
        <?php if (isset($generator->manyLinks)) { ?>
            <tr>
                <td>Поле</td>
                <td>Класс целевой модели</td>
                <td>Связующая таблица (var2var)</td>
                <td>Идентификатор текущей модели</td>
                <td>Идентификатор целевой модели</td>
                <td>Название поля</td>
                <td></td>
            </tr>
            <?php foreach($generator->manyLinks as $k => $link) { ?>
                <tr class="fieldsRow<?=!empty($link['class'])?$link['class']:''?>" data-key="<?=$k?>">
                    <td><input type="text" class="form-control" name="Generator[manyLinks][<?=$k?>][name]" value="<?=$link['name']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[manyLinks][<?=$k?>][classname]" value="<?=$link['classname']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[manyLinks][<?=$k?>][linktable]" value="<?=$link['linktable']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[manyLinks][<?=$k?>][id_1]" value="<?=$link['id_1']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[manyLinks][<?=$k?>][id_2]" value="<?=$link['id_2']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[manyLinks][<?=$k?>][label]" value="<?=$link['label']?>"/></td>
                    <td><div class="btn btn-default trDel">У</div></td>
                </tr>
            <?php } ?>
        <?php } ?>
    </table>
</div>
<div class="form-group">
    <div class="btn btn-primary fieldAdd btnAdd">Добавить связь</div>
</div>



<div class="form-group">
    <label class="control-label" for="generator-children" data-original-title="" title="">Дочерние таблицы</label>
    <table class="childrenTable editTable">
        <?php if (isset($generator->children)) { ?>
            <tr>
                <td>Название таблицы</td>
                <td>Наименование списка</td>
                <td>Кнопка добавления</td>
                <td>Имя переменной</td>
                <td>Доп.</td>
                <td></td>
            </tr>
            <?php foreach($generator->children as $k => $child) { ?>
                <tr class="childrenRow<?=!empty($child['class'])?$child['class']:''?>" data-key="<?=$k?>">
                    <td><input type="text" class="form-control" name="Generator[children][<?=$k?>][name]" value="<?=$child['name']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[children][<?=$k?>][label1]" value="<?=$child['label1']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[children][<?=$k?>][label2]" value="<?=$child['label2']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[children][<?=$k?>][var]" value="<?=$child['var']?>"/></td>
                    <td><input type="checkbox" name="Generator[children][<?=$k?>][add]"<?=!empty($child['add'])?' checked':''?>/></td>
                    <td><div class="btn btn-default trDel">У</div></td>
                </tr>
            <?php } ?>
        <?php } ?>
    </table>
</div>
<div class="form-group">
    <div class="btn btn-primary childrenAdd btnAdd">Добавить дочернюю таблицу</div>
</div>



<div class="form-group">
    <label class="control-label" for="generator-parents" data-original-title="" title="">Родительская таблица</label>
    <table class="parentsTable editTable">
        <?php if (isset($generator->parents)) { ?>
            <tr>
                <td>Название таблицы</td>
                <td>Наименование</td>
                <td>Имя переменной</td>
                <td></td>
            </tr>
            <?php foreach($generator->parents as $k => $parent) { ?>
                <tr class="parentsRow<?=!empty($parent['class'])?$parent['class']:''?>" data-key="<?=$k?>">
                    <td><input type="text" class="form-control" name="Generator[parents][<?=$k?>][name]" value="<?=$parent['name']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[parents][<?=$k?>][label]" value="<?=$parent['label']?>"/></td>
                    <td><input type="text" class="form-control" name="Generator[parents][<?=$k?>][var]" value="<?=$parent['var']?>"/></td>
                    <td><div class="btn btn-default trDel">У</div></td>
                </tr>
            <?php } ?>
        <?php } ?>
    </table>
</div>
<div class="form-group">
    <div class="btn btn-primary parentsAdd btnAdd">Добавить родительскую таблицу</div>
</div>

<?php
    echo $form->field($generator, 'fieldTimes')->checkbox();
    echo $form->field($generator, 'fieldWeight')->checkbox();
    echo $form->field($generator, 'fieldSeo')->checkbox();
    echo $form->field($generator, 'fieldPublic')->checkbox();
    echo $form->field($generator, 'doMail')->checkbox();
    echo $form->field($generator, 'publicController')->checkbox();
    echo $form->field($generator, 'doMigration')->checkbox();
    echo $form->field($generator, 'isTree')->checkbox();
    echo $form->field($generator, 'fieldHandle')->checkbox();
    echo $form->field($generator, 'tableName');
    echo $form->field($generator, 'modelLabels');
?>

<?php
$script = <<< JS
    $(document).ready(function() {
        $('.btnAdd').click(function() {
            var table = $(this).parent().prev().find('table');
            table.append(table.find('tr.last').clone(true));
            table.find('tr.last').eq(0).removeClass('last');
            var k = parseInt(table.find('tr.last').attr('data-key'));
            table.find('tr.last').attr('data-key', k+1).find('input, select').each(function(idx, el) {
                var name = $(el).attr('name');
                name = name.replace('['+k+']', '['+(k+1)+']');
                $(el).attr('name', name);
            });
        });
        
        $('.trDel').click(function() {
            $(this).parents('tr').remove();
        });
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);