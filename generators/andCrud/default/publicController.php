<?='<?php'?>

namespace app\controllers;

use rusbitles\adminbase\controllers\base\PublicController;
use app\models\<?=$generator->getModelName()?>;
use rusbitles\adminbase\helpers\SystemTools;
use rusbitles\adminbase\models\Settings;
use Yii;
use yii\web\NotFoundHttpException;

class <?=$generator->getControllerName()?>Controller extends PublicController
{
    public static function getRules($type = false)
    {
        return [
            '/<handle:[-\w]+>'    => '<?=$generator->getHandleName()?>/detail',
            ''                    => '<?=$generator->getHandleName()?>/index',
        ];
    }
    
    public static $controllerTitle = '<?=$generator->modelLabelsArray[0]?>';

    public function actionIndex($page = 1)
    {
        $criteria = <?=$generator->getModelName()?>::find()->published();
        $criteria->orderBy('t.weight ASC');
        
        $countQuery = clone $criteria;
        $pages = SystemTools::getPages($countQuery->count(), Settings::getParam('<?=$generator->getHandleName()?>', 'countOnPage'), '<?=$generator->getHandleName()?>/index', [], $page - 1);
        
        $items = $criteria->offset($pages->offset)->limit($pages->limit)->all();
        
        return $this->render('index', ['items' => $items, 'pages' => $pages]);
    }

    public function actionDetail($handle) {
        $item = <?=$generator->getModelName()?>::find()->where(['handle'=>$handle])->published()->one();
        if (!$item) throw new NotFoundHttpException();
<?php if($generator->fieldSeo) { ?>
        $this->addSeoItem($item, $item->title, $item->title);
<?php } else { ?>
        $this->h1title = $item->title;
        $this->addBreadCrumb('', $item->title);
<?php } ?>
        return $this->render('detail', ['item' => $item]);
    }
}
