<?='<?php'?>

use yii\db\Migration;

class m<?=date('ymd')?>_<?=substr('000000'.$generator->migrationNum, -6)?>_gii_create_<?=$generator->tableName?>_table extends Migration
{
    public function up()
    {
<?php foreach($generator->migrationFields as $tn => $fields) { ?>
<?php if (isset($history[$tn])) { ?>
<?php foreach($fields as $code => $value) { ?>
<?php if(!isset($history[$tn][$code])) { ?>
        $this->addColumn('<?=$tn?>', '<?=$code?>', <?=$value?>);
<?php } elseif($history[$tn][$code] != $value) { ?>
        $this->alterColumn('<?=$tn?>', '<?=$code?>', <?=$value?>);
<?php } ?>
<?php } ?>
<?php foreach($history[$tn] as $code => $value) { ?>
<?php if(!isset($fields[$code])) { ?>
        $this->dropColumn('<?=$tn?>', '<?=$code?>');
<?php } ?>
<?php } ?>
<?php } else { ?>
        $this->createTable('<?=$tn?>', [
<?php foreach($fields as $code => $value) { ?>
            '<?=$code?>' => <?=$value?>,
<?php } ?>
        ]);
<?php } ?>
<?php } ?>
    }

    public function down()
    {
<?php foreach($generator->migrationFields as $tn => $fields) { ?>
<?php if (isset($history[$tn])) { ?>
<?php foreach($fields as $code => $value) { ?>
<?php if(!isset($history[$tn][$code])) { ?>
        $this->dropColumn('<?=$tn?>', '<?=$code?>');
<?php } elseif($history[$tn][$code] != $value) { ?>
        $this->alterColumn('<?=$tn?>', '<?=$code?>', <?=$history[$tn][$code]?>);
<?php } ?>
<?php } ?>
<?php foreach($history[$tn] as $code => $value) { ?>
<?php if(!isset($fields[$code])) { ?>
        $this->addColumn('<?=$tn?>', '<?=$code?>', <?=$value?>);
<?php } ?>
<?php } ?>
<?php } else { ?>
        $this->dropTable('<?=$tn?>');
<?php } ?>
<?php } ?>
    }
}