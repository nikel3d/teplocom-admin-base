<?php
    /*echo '<?php' . "\r\n";
    echo "\t" . 'use app\models\SellForm;' . "\r\n";
    echo '?>' . "\r\n\r\n";*/
    echo '<p>Данные формы:</p>' . "\r\n";

    foreach($generator->fields as $field) {
        switch($field['type']) {
            case 'image':
            case 'file':
                break;
            case 'checkbox':
                echo '<p>'.$field['label'].': <?=($model->'.$field['name'].'?"Да":"Нет")?></p>' . "\r\n";
                break;
            default:
                echo '<p>'.$field['label'].': <?=$model->'.$field['name'].'?></p>' . "\r\n";
        }
    }

    foreach ($generator->links as $link) {
        echo '<p>'.$link['label'].': <?=$model->'.$link['name'].'->getTemplateValue("'.$link['tmpl'].'")?></p>' . "\r\n";
    }