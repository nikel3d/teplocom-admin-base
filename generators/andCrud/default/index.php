<?='<?php'?>

use rusbitles\adminbase\helpers\BImages;
use yii\helpers\Html;
use app\models\<?=$generator->getModelName()?>;

$fields = [
    "id",
<?php
if ($generator->fieldPublic) {
    echo "\t" . '[
        "attribute"  => "public",
        "content" => function ($data) {
            return $data->public>0?"+":"-";
        },
    ],'."\r\n";
}
foreach($generator->fields as $field) {
    if (!empty($field['show'])) {
        switch($field['type']) {
            case 'image':
                echo "\t" . '[
        "attribute"  => "'.$field['name'].'",
        "content" => function ($data) {
            return Html::img(BImages::doProp($data->'.$field['name'].', 100, 100));
        },
        "filter" => false,
    ],' . "\r\n";
                break;
            case 'checkbox':
                echo "\t" . '[
        "attribute"  => "'.$field['name'].'",
        "content" => function ($data) {
            return $data->'.$field['name'].'>0?"+":"-";
        },
    ],'."\r\n";
                break;
            default:
                echo "\t'".$field['name']."',\r\n";
        }
    }
}
foreach ($generator->selects as $select) {
    if ($select['show']) {
        echo "\t" . '[
            "attribute" => "'.$select['name'].'",
            "content" => function($data) {
                return ' . $generator->getModelName() . '::$'.$select['name'].'List[$data->'.$select['name'].'];
            },
        ],'."\r\n";
    }
}
foreach ($generator->links as $link) {
        echo "\t" . '[
            "attribute" => "'.$link['link'].'",
            "content" => function($data) {
                if ($data->'.$link['name'].') {
                    return $data->'.$link['name'].'->getTemplateValue("'.$link['tmpl'].'");
                }
                return "(empty)";
            },
        ],'."\r\n";
}
if ($generator->fieldHandle) {
    echo "\t'handle',\r\n";
}
if ($generator->hasAdditional) {
    echo "\t'additionalLabel',\r\n";
}
if ($generator->fieldWeight) {
    echo "\t'weight',\r\n";
}
if ($generator->fieldTimes) {
    echo "\t" . '[
            "attribute" => "created_at",
            "content" => function($data) {
                return date("d.m.Y H:i:s", $data->created_at);
            },
        ],'."\r\n";
}
?>
];
<?='?>'?>

<?='<?=$this->render("@rusbitles/adminbase/views/base/index", ["fields" => $fields, "dataProvider" => $dataProvider, "item" => $item])?>'?>