<?='<?php'?>

use yii\db\Migration;

class m<?=date('ymd')?>_<?=substr('000000'.$generator->migrationNum, -6)?>_gii_create_<?=$tn?>_table extends Migration
{
    public function up()
    {
        $this->createTable('<?=$tn?>', [
            '<?=$id1?>' => $this->integer(),
            '<?=$id2?>' => $this->integer(),
        ]);
    
        $this->createIndex('idx_<?=$id1?>_<?=$id2?>',
            '<?=$tn?>',
            ['<?=$id1?>', '<?=$id2?>'],
        true);
    }
    
    public function down()
    {
        $this->dropTable('<?=$tn?>');
    }
}