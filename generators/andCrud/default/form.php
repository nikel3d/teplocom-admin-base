<?php
    $usesClass = [
        $generator->getModelName() => 'Y',
    ];

    foreach($generator->links as $link) {
        $usesClass[$link['classname']] = 'Y';
    }
    foreach($generator->manyLinks as $mLink) {
        $usesClass[$mLink['classname']] = 'Y';
    }

    $usesClass = array_keys($usesClass);

    echo '<?php' . "\r\n";
    echo "\t" . 'use rusbitles\adminbase\helpers\AdminTools;'."\r\n";
    foreach ($usesClass as $classes) {
        if (in_array($classes, $generator->baseModels)) {
            echo "\t" . "use app\\models\\base\\" . $classes . ";\r\n";
        } else {
            echo "\t" . "use app\\models\\" . $classes . ";\r\n";
        }
    }
echo '?>' . "\r\n"; ?>

<?php
foreach ($generator->fields as $field) {
    if (!empty($field['edit'])) {
        switch ($field['type']) {
            case 'image':
                echo '<?=AdminTools::generateImageInputs($model, $form, "'.$field['name'].'")?>' . "\r\n";
                break;
            case 'file':
                echo '<?=AdminTools::generateFileInputs($model, $form, "'.$field['name'].'")?>' . "\r\n";
                break;
            case 'date':
                echo '<?=AdminTools::generateDatepicker($model, $form, "'.$field['name'].'")?>' . "\r\n";
                break;
            case 'date_only':
                echo '<?=AdminTools::generateOnlyDatepicker($model, $form, "'.$field['name'].'")?>' . "\r\n";
                break;
            case 'text':
                echo '<?=AdminTools::generateRedactor($model, $form, "'.$field['name'].'")?>' . "\r\n";
                break;
            case 'string_500':
                echo '<?=$form->field($model, "'.$field['name'].'")->textarea()?>' . "\r\n";
                break;
            case 'checkbox':
                echo '<?=$form->field($model, "'.$field['name'].'")->checkbox([], false)?>' . "\r\n";
                break;
            case 'table':
                echo '<?=AdminTools::generateTableDatas($model, $form, "'.$field['name'].'")?>' . "\r\n";
                break;
            default:
                echo '<?=$form->field($model, "'.$field['name'].'")?>' . "\r\n";
        }
    }
}
foreach ($generator->selects as $select) {
    echo '<?=$form->field($model, "'.$select['name'].'")->dropDownList('.$generator->getModelName().'::$'.$select['name'].'List)?>' . "\r\n";
}
foreach ($generator->links as $link) {
    echo '<?=AdminTools::generateSelect2($model, $form, "'.$link['link'].'", '.$link['classname'].'::find()->getDropDownList("'.$link['tmpl'].'", "Не выбрано"))?>' . "\r\n";
}
foreach ($generator->manyLinks as $mLink) {
    echo '<?=AdminTools::generateSelect2($model, $form, "'.$mLink['name'].'", '.$mLink['classname'].'::getDropDownList(), true)?>' . "\r\n";
}
if ($generator->fieldHandle && !$generator->isTree) {
    echo '<?=$form->field($model, "handle")?>' . "\r\n";
}
if ($generator->fieldHandle && $generator->isTree) {
    echo '<?=$form->field($model, "real_handle")?>' . "\r\n";
    echo '<?=$form->field($model, "tree_handle")->checkbox([], false)?>' . "\r\n";
}
if ($generator->fieldSeo) {
    echo '<?=$form->field($model, "seo_title")?>' . "\r\n";
    echo '<?=$form->field($model, "seo_description")->textarea()?>' . "\r\n";
    echo '<?=$form->field($model, "seo_keywords")?>' . "\r\n";
}
if ($generator->fieldWeight) {
    echo '<?=$form->field($model, "weight")?>' . "\r\n";
}
if ($generator->fieldPublic) {
    echo '<?=$form->field($model, "public")->checkbox([], false)?>' . "\r\n";
}
if ($generator->isTree) {
    echo '<?php if($this->context->action->id == \'create\') { ?>' . "\r\n";
    echo "\t" . '<?php $mn = get_class($this->context); $mn = $mn::$fullModelName; ?>' . "\r\n";
    echo "\t" . '<?=$form->field($model, \'tree_parent_id\')->dropDownList($mn::getDropDownList())?>' . "\r\n";
    echo '<?php } ?>' . "\r\n";
}
?>