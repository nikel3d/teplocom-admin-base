<?='<?php'?><?php
    $commonRules = [
        'image' => 'safe',
        'file' => 'safe',
        'date' => 'safe',
        'date_only' => 'safe',
        'checkbox' => 'integer',
        'table' => 'text',
        'email' => 'string_255',
    ];

    $needAfterSave = false;
    $useModels = [];
    $required = [];
    $rules = [];
    $rulesSepareted = [];
    foreach($generator->fields as $field) {
        if (!empty($commonRules[$field['type']])) {
            $rules[$commonRules[$field['type']]][] = $field['name'];
        } else {
            $rules[$field['type']][] = $field['name'];
        }
        $rulesSepareted[$field['type']][] = $field['name'];

        if (!empty($field['required'])) {
            $required[] = $field['name'];
        }
        if ($field['type'] == 'email') {
            $rules[$field['type']][] = $field['name'];
        }
    }
    if ($generator->fieldWeight) {
        $rules['integer'][] = 'weight';
    }
    if ($generator->fieldPublic) {
        $rules['integer'][] = 'public';
    }

    foreach($generator->selects as $select) {
        $rules['string_255'][] = $select['name'];
    }

    foreach($generator->links as $link) {
        $rules['integer'][] = $link['link'];
        $useModels[] = $link['classname'];
    }
    foreach($generator->manyLinks as $mLink) {
        $useModels[] = $mLink['classname'];
        $rules['safe'][] = $mLink['name'].'Links';
    }
    if (count($generator->manyLinks)) {
        $needAfterSave = true;
    }
    foreach($generator->children as $child) {
        $useModels[] = $generator->getModelFromString($child['name']);
    }
    foreach($generator->parents as $parent) {
        $useModels[] = $generator->getModelFromString($parent['name']);
    }

    if($generator->fieldSeo) {
        $rules['string_255'][] = 'seo_title';
        $rules['string_500'][] = 'seo_description';
        $rules['string_255'][] = 'seo_keywords';
    }

    if($generator->fieldHandle && $generator->isTree) {
        $rules['string_255'][] = 'real_handle';
        $rules['string_500'][] = 'handle';
    }

    if($generator->fieldHandle && !$generator->isTree) {
        $rules['string_255'][] = 'handle';
    }

    if($generator->isTree) {
        $rules['safe'][] = 'tree_parent_id';
        $rules['integer'][] = 'tree_handle';
    }

    if($generator->fieldTimes) {
        $rules['safe'][] = 'created_at';
        $rules['safe'][] = 'updated_at';
    }

    $useModels = array_unique($useModels);
?>

namespace app\models\parents;

use Yii;
use yii\helpers\ArrayHelper;
<?php foreach($useModels as $um) { ?>
<?php if (in_array($um, $generator->baseModels)) { ?>
use rusbitles\adminbase\models\<?=$um?>;
<?php } else { ?>
use app\models\<?=$um?>;
<?php } ?>
<?php } ?>
<?php if(!empty($rulesSepareted['date']) || !empty($rulesSepareted['date_only'])) { ?>
use rusbitles\adminbase\behaviors\DateBehavior;
<?php } ?>
<?php if($generator->fieldHandle) { ?>
use rusbitles\adminbase\behaviors\HandleBehavior;
<?php } ?>
<?php if(!empty($rulesSepareted['table'])) { ?>
use rusbitles\adminbase\behaviors\TableBehavior;
<?php } ?>
<?php if(!empty($rulesSepareted['image'])) { ?>
use rusbitles\adminbase\behaviors\ImageBehavior;
<?php } ?>
<?php if($generator->isTree) { ?>
use rusbitles\adminbase\models\CTreeActiveRecord;
use creocoder\nestedsets\NestedSetsBehavior;
<?php } else { ?>
use rusbitles\adminbase\models\CActiveRecord;
<?php } ?>
<?php if($generator->fieldTimes) { ?>
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
<?php } ?>
<?php if(count($generator->selects) > 0) { ?>
use rusbitles\adminbase\behaviors\SelectBehavior;
<?php } ?>
/**
 * This is the model class for table "<?=$generator->tableName?>".
 *
 * @property integer $id
<?php foreach($generator->fields as $field) { ?>
 * @property <?=$generator->types[$field['type']][0]?> $<?=$field['name']?><?="\r\n"?>
<?php } ?>
 * @property integer $weight
 * @property integer $public
 */
class <?=$generator->getModelName()?>Parent extends <?=($generator->isTree)?'CTreeActiveRecord':'CActiveRecord'?>
{
<?php foreach($generator->manyLinks as $mLink) { ?>
    public $_<?=$mLink['name']?>Links = false;
<?php } ?>
<?php if($generator->isTree) { ?>
    public $tree_parent_id = false;
<?php } ?>
<?php foreach($generator->selects as $select) { ?>
    public static $<?=$select['name']?>List = [
<?php foreach(explode("\r\n", $select['variants']) as $k => $row) { $fields = explode('[:]', $row);?>
<?php if(count($fields) > 1) { ?>
        '<?=$fields[0]?>' => '<?=$fields[1]?>',
<?php } else { ?>
        <?=$k?> => '<?=$row?>',
<?php } } ?>
    ];
<?php } ?>
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '<?=$generator->tableName?>';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
<?php if (count($required) > 0) { ?>
            [['<?=implode("', '", $required)?>'], 'required'],
<?php } ?>
<?php foreach($rules as $ruleType => $ruleFields) {
    switch($ruleType) {
        case 'string_255':
            echo "\t\t\t[['" . implode("', '", $ruleFields) . "'], 'string', 'max' => 255],\r\n";
            break;
        case 'string_500':
            echo "\t\t\t[['" . implode("', '", $ruleFields) . "'], 'string', 'max' => 500],\r\n";
            break;
        case 'text':
            echo "\t\t\t[['" . implode("', '", $ruleFields) . "'], 'string'],\r\n";
            break;
        case 'email':
            echo "\t\t\t[['" . implode("', '", $ruleFields) . "'], 'email'],\r\n";
            break;
        case 'integer':
            echo "\t\t\t[['" . implode("', '", $ruleFields) . "'], 'integer'],\r\n";
            break;
        case 'float':
            echo "\t\t\t[['" . implode("', '", $ruleFields) . "'], 'number'],\r\n";
            break;
        case 'safe':
            echo "\t\t\t[['" . implode("', '", $ruleFields) . "'], 'safe'],\r\n";
            break;
    }
} ?>
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'id' => 'ID',
<?php 
foreach($generator->fields as $field) {
    echo "\t\t\t'".$field['name']."' => '".$field['label']."',\r\n";
}
if ($generator->fieldWeight) {
    echo "\t\t\t'weight' => 'Порядок',\r\n";
}
if ($generator->fieldPublic) {
    echo "\t\t\t'public' => 'Публикация',\r\n";
}
foreach($generator->selects as $select) {
    echo "\t\t\t'".$select['name']."' => '".$select['label']."',\r\n";
}
foreach($generator->links as $link) {
    echo "\t\t\t'".$link['link']."' => '".$link['label']."',\r\n";
}
foreach($generator->manyLinks as $mLink) {
    echo "\t\t\t'".$mLink['name']."' => '".$mLink['label']."',\r\n";
    echo "\t\t\t'".$mLink['name']."Links' => '".$mLink['label']."',\r\n";
}
if ($generator->fieldSeo) {
    echo "\t\t\t'seo_title' => 'Seo title',\r\n";
    echo "\t\t\t'seo_description' => 'Seo description',\r\n";
    echo "\t\t\t'seo_keywords' => 'Seo keywords',\r\n";
}
if($generator->fieldHandle && $generator->isTree) {
    echo "\t\t\t'handle' => 'Генерируемый алиас',\r\n";
    echo "\t\t\t'real_handle' => 'Алиас',\r\n";
    echo "\t\t\t'tree_handle' => 'Самостоятельный алиас?',\r\n";
}

if($generator->fieldHandle && !$generator->isTree) {
    echo "\t\t\t'handle' => 'Алиас',\r\n";
}

if($generator->isTree) {
    echo "\t\t\t'lft' => 'Lft',\r\n";
    echo "\t\t\t'rgt' => 'Rgt',\r\n";
    echo "\t\t\t'tree' => 'Tree',\r\n";
    echo "\t\t\t'depth' => 'Depth',\r\n";
    echo "\t\t\t'tree_parent_id' => 'Родительский раздел',\r\n";
}

if($generator->fieldTimes) {
    echo "\t\t\t'created_at' => 'Создано',\r\n";
    echo "\t\t\t'updated_at' => 'Обновлено',\r\n";
}

if($generator->hasAdditional) {
    echo "\t\t\t'additionalLabel' => 'Модель',\r\n";
}
?>
        ]);
    }

    public function behaviors() {
        return [
<?php if($generator->isTree) { ?>
            'tree' => [
                'class' => NestedSetsBehavior::className(),
            ],
<?php } ?>
<?php if($generator->fieldTimes) { ?>
            'timeStamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
<?php } ?>
<?php if($generator->fieldHandle) { ?>
            'handleBehavior' => [
                'class' => HandleBehavior::className(),
<?php if($generator->isTree) { ?>
                'attribute' => 'real_handle',
<?php } else { ?>
                'attribute' => 'handle',
<?php } ?>
            ],
<?php } ?>
<?php if(!empty($rulesSepareted['table'])) { ?>
            'tableBehavior' => [
                'class' => TableBehavior::className(),
                'fields' => [
<?php foreach($rulesSepareted['table'] as $field) { ?>
                    '<?=$field?>' => [
                        'items' => [
                            'class 1' => ['f1', 'f2'],
                            'class 2' => ['f3', 'f4'],
                            'other' => ['other', ''],
                        ],
                        'hidden' => [0],
                        'cols' => 3,
                    ],
<?php } ?>
                ],
            ],
<?php } ?>
<?php if(count($generator->selects) > 0) { ?>
            'tableBehavior' => [
                'class' => SelectBehavior::className(),
                'attributes' => [
<?php foreach($generator->selects as $select) { ?>
                    '<?=$select['name']?>' => static::$<?=$select['name']?>List,
<?php } ?>
                ],
            ],
<?php } ?>
<?php $dates = array_merge((!empty($rulesSepareted['date']))?$rulesSepareted['date']:[], (!empty($rulesSepareted['date_only']))?$rulesSepareted['date_only']:[]);
if(count($dates) > 0) { ?>
            'dateBehavior' => [
                'class' => DateBehavior::className(),
                'attributes' => ['<?=implode("', '", $dates)?>'],
            ],
<?php } ?>
<?php if (count($rulesSepareted['image']) > 0) { ?>
            'imageBehavior' => [
                'class' => ImageBehavior::className(),
                'attributes' => ['<?=implode("', '", $rulesSepareted['image'])?>'],
            ],
<?php } ?>
        ];
    }

<?php if ($generator->fieldWeight) { ?>
    public static $sortField = 'weight';
<?php } ?>

<?php foreach($generator->links as $link) { ?>
    public function get<?=$link['name']?>() {
        return $this->hasOne(<?=$link['classname']?>::className(), ['id' => '<?=$link['link']?>']);
    }
<?php } ?>
<?php foreach($generator->manyLinks as $mLink) { ?>
    public function get<?=ucfirst($mLink['name'])?>() {
        if (<?=$mLink['classname']?>::$sortField != '') {
            return $this->hasMany(<?=$mLink['classname']?>::className(), ['id' => '<?=$mLink['id_2']?>'])->viaTable('<?=$mLink['linktable']?>', ['<?=$mLink['id_1']?>' => 'id'])->orderBy(<?=$mLink['classname']?>::$sortField . ' ASC');
        } else {
            return $this->hasMany(<?=$mLink['classname']?>::className(), ['id' => '<?=$mLink['id_2']?>'])->viaTable('<?=$mLink['linktable']?>', ['<?=$mLink['id_1']?>' => 'id']);
        }
    }
<?php } ?>
<?php foreach($generator->children as $child) { if (!$child['add']) { ?>
    public function get<?=ucfirst($child['var'])?>() {
        if (<?=$generator->getModelFromString($child['name'])?>::$sortField != '') {
            return $this->hasMany(<?=$generator->getModelFromString($child['name'])?>::className(), ['parent_id' => 'id'])->orderBy(<?=$generator->getModelFromString($child['name'])?>::$sortField . ' ASC');
        } else {
            return $this->hasMany(<?=$generator->getModelFromString($child['name'])?>::className(), ['parent_id' => 'id']);
        }
    }
<?php } } ?>
<?php if ($generator->hasAdditional) { ?>
    private $_additional;
    public static $additionals = [
<?php foreach($generator->children as $child) { if ($child['add']) { ?>
        '<?=$generator->getHandleFromStringFB($child['name'])?>' => ['app\\models\\<?=$generator->getModelFromString($child['name'])?>', '<?=$child['label1']?>'],
<?php } } ?>
    ];

    public function getAdditional() {
        if (empty($this->_additional)) {
            $cn = static::$additionals[$this->system_additional][0];
            if ($this->id) $this->_additional = $cn::find()->where(['parent_id' => $this->id])->one();
            if (!$this->_additional) $this->_additional = new $cn();
        }
        return $this->_additional;
    }

    public function getAdditionalLabel() {
        return static::$additionals[$this->system_additional][1];
    }
<?php } ?>

<?php foreach($generator->parents as $parent) { ?>
    public function get<?=ucfirst($parent['var'])?>() {
        return $this->hasOne(<?=$generator->getModelFromString($parent['name'])?>::className(), ['id' => 'parent_id']);
    }
<?php } ?>

<?php foreach($generator->manyLinks as $mLink) { ?>
    public function get<?=ucfirst($mLink['name'])?>Links()
    {
        if (!$this->_<?=$mLink['name']?>Links) {
            $this->_<?=$mLink['name']?>Links = ArrayHelper::getColumn($this-><?=$mLink['name']?>, 'id');
        }
        return $this->_<?=$mLink['name']?>Links;
    }
    public function set<?=ucfirst($mLink['name'])?>Links($value)
    {
        $this->_<?=$mLink['name']?>Links = $value;
    }
<?php } ?>

<?php if($needAfterSave) { ?>
    public function afterSave($insert, $changedAttributes)
    {
<?php foreach($generator->manyLinks as $mLink) { ?>
        $this->saveLinkMany2Many('<?=$mLink['linktable']?>', '<?=$mLink['name']?>Links', '<?=$mLink['id_1']?>', '<?=$mLink['id_2']?>');
<?php } ?>

        parent::afterSave($insert, $changedAttributes);
    }
<?php } ?>

<?php if ($generator->isTree && $generator->fieldHandle) { ?>
    public function beforeSave($insert)
    {
        $res = parent::beforeSave($insert);
        if (($this->tree_handle != $this->getOldAttribute('tree_handle') || $this->real_handle != $this->getOldAttribute('real_handle') || $this->isNewRecord) && $this->handle != 'root') {
            if ($this->tree_handle == 1) {
                $this->handle = $this->real_handle;
            } else {
                $parent = $this->parents(1)->one();
                if ($parent && $parent->handle != '' && $parent->handle != 'root') {
                    $this->handle = $parent->handle . '/' . $this->real_handle;
                } else {
                    $this->handle = $this->real_handle;
                }
            }

            $this->changeChildHandle();
        }

        return $res;
    }

    private function changeChildHandle() {
        $children = $this->children(1)->all();
        foreach($children as $child) {
            if ($child->tree_handle != 1) {
                $child->handle = $this->handle . '/' . $child->real_handle;
                $child->save();

                $child->changeChildHandle();
            }
        }
    }
<?php } ?>
<?php if($generator->isTree) { ?>
    public static function getDropDownList() {
        $result = array();
        $items = <?=$generator->getModelName()?>Parent::find()->orderBy('lft')->all();
        foreach($items as $item) {
            $result[$item->id] = str_repeat('–', $item->depth) . ' ' . $item->title . ' [' . $item->handle . ']';
        }
        return $result;
    }
<?php } ?>

}