<?php echo "<?php"; ?>

namespace app\controllers\admin\parents;

<?php if ($generator->isTree) { ?>
class <?=$generator->getControllerName()?>ParentController extends \rusbitles\adminbase\controllers\base\TreeAdminController
<?php } else { ?>
class <?=$generator->getControllerName()?>ParentController extends \rusbitles\adminbase\controllers\base\AdminController
<?php } ?>
{
    public static $mainmenuHandle = '<?=$generator->getHandleName()?>';
    public static $modelName='<?=$generator->getModelName()?>';
    public static $fullModelName='app\\models\\<?=$generator->getModelName()?>';
    public static $modelTitles = [<?=$generator->getModelLabels()?>];
    public static $images = [
<?php foreach($generator->fields as $field) { ?><?php if($field['type'] == 'image' || $field['type'] == 'file') { ?>
        '<?=$field['name']?>' => '@webroot/up/<?=$generator->getHandleName()?>',
<?php } ?><?php } ?>
    ];
    public static $children = [
<?php foreach($generator->children as $child) { ?><?php if(!$child['add']) {?>
        '<?=$generator->getHandleFromString($child['name'])?>' => ['<?=$child['label1']?>', '<?=$child['label2']?>'],
<?php } ?><?php } ?>
    ];
    public $additional = [
<?php foreach($generator->children as $child) { ?><?php if($child['add']) {?>
        '<?=$generator->getHandleFromStringFB($child['name'])?>' => ['app\\controllers\\admin\\<?=$generator->getHandleFromStringFB($child['name'])?>Controller', 'app\\models\\<?=$generator->getModelFromString($child['name'])?>', '<?=$child['label1']?>'],
<?php } ?><?php } ?>
    ];
    public $parents = [
<?php foreach($generator->parents as $parent) { ?>
        '<?=$generator->getHandleFromString($parent['name'])?>' => '<?=$parent['label']?>',
<?php } ?>
    ];
    public static $links = [
<?php foreach($generator->manyLinks as $mLink) { ?>
        '<?=$generator->getHandleFromString($mLink['name'])?>' => ['<?=$mLink['linktable']?>', '<?=$mLink['id_1']?>', '<?=$mLink['id_2']?>'],
<?php } ?>
    ];
<?php if (count($generator->parents) > 0) { ?>
    public static function getMenu() {
        return [];
    }
<?php } ?>
    public function searchFields() {
        return [
<?php foreach($generator->fields as $field) { ?>
<?php if (in_array($field['type'], ['string_255', 'string_500', 'text', 'email', 'integer', 'float'])) { ?>
            '<?=$field['name']?>' => ['type' => 'string'],
<?php } elseif (in_array($field['type'], ['date', 'date_only'])) { ?>
            '<?=$field['name']?>' => ['type' => 'date'],
<?php } elseif (in_array($field['type'], ['checkbox'])) { ?>
            '<?=$field['name']?>' => ['type' => 'checkbox'],
<?php } ?>
<?php } ?>
<?php foreach($generator->links as $link) { ?>
            '<?=$link['link']?>' => ['type' => 'stringModel', 'model' => 'app\\models\\<?=$link['classname']?>', 'field' => 'title'],
<?php } ?>
        ] + parent::searchFields();
    }
}