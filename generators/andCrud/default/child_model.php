<?php
    echo '<?php';

    $rulesSepareted = [];
    foreach($generator->fields as $field) {
        $rulesSepareted[$field['type']][] = $field['name'];
    }
?>

namespace app\models;

use Yii;
use app\models\parents\<?=$generator->getModelName()?>Parent;
<?php if(!empty($rulesSepareted['table'])) { ?>
use rusbitles\adminbase\behaviors\TableBehavior;
<?php } ?>

class <?=$generator->getModelName()?> extends <?=$generator->getModelName()?>Parent
{
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
        
        ]);
    }
    
    public function rules()
    {
        return array_merge(parent::rules(), [
        
        ]);
    }

    public function behaviors() {
        $parent = parent::behaviors();
<?php if(!empty($rulesSepareted['table'])) { ?>
        $parent['tableBehavior'] = [
            'class' => TableBehavior::className(),
            'fields' => [
<?php foreach($rulesSepareted['table'] as $field) { ?>
                '<?=$field?>' => [
                    'items' => [
                        'colNames' => [],
                        'class 1' => ['f1', 'f2'],
                        'class 2' => ['f3', 'f4'],
                        'other' => ['other', ''],
                    ],
                    'hidden' => [0],
                    'cols' => 3,
                ],
<?php } ?>
            ],
        ];
<?php } ?>
        
        return $parent;
    }
}