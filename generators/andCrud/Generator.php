<?php
namespace rusbitles\adminbase\generators\andCrud;

use rusbitles\adminbase\models\SystemGii;
use Yii;
use yii\gii\CodeFile;

class Generator extends \yii\gii\Generator
{
    public $tableName;
    public $fields;
    public $selects;
    public $links;
    public $manyLinks;
    public $children;
    public $parents;
    public $modelLabels;
    // 0 - doc, 1 - label
    public $types = [
        'string_255' => ['string', 'String 255'],
        'string_500' => ['string', 'String 500'],
        'text' => ['string', 'Text'],
        'email' => ['string', 'Email'],
        'image' => ['string', 'Image'],
        'file' => ['string', 'File'],
        'date' => ['string', 'Date'],
        'date_only' => ['string', 'Date (Only)'],
        'integer' => ['integer', 'Integer'],
        'float' => ['float', 'Float'],
        'checkbox' => ['integer', 'Checkbox'],
        'table' => ['string', 'Table'],
    ];
    public $fieldWeight;
    public $fieldPublic;
    public $fieldTimes;
    public $fieldSeo;
    public $doMail;
    public $doMigration;
    public $migrationNum = 0;
    public $isTree;
    public $fieldHandle;
    public $publicController;
    public $baseModels = [];

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['tableName', 'modelLabels'], 'required'],
            [['fields', 'selects', 'links', 'manyLinks', 'parents', 'children', 'fieldTimes', 'fieldWeight', 'fieldSeo', 'doMail', 'fieldPublic', 'doMigration', 'isTree', 'fieldHandle', 'publicController'], 'safe'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'fieldTimes' => 'Добавить время обновления и добавления',
            'fieldWeight' => 'Добавить порядок',
            'fieldSeo' => 'Добавить Сео поля',
            'doMail' => 'Добавить почтовый шаблон',
            'fieldPublic' => 'Добавить публикацию',
            'publicController' => 'Генерация публичного контроллера',
            'doMigration' => 'Генерация миграции',
            'isTree' => 'Дерево',
            'fieldHandle' => 'Алиас',
            'tableName' => 'Наименование таблицы',
            'modelLabels' => 'Наименование элемента через запятую (Новость, Новости, Новостей)',
        ]);
    }

    public function hints()
    {
        return array_merge(parent::hints(), [
            'tableName' => 'Маленькими буквами. Если несколько слов, разделитель «_».',
            'modelLabels' => '',
        ]);
    }

    public function requiredTemplates()
    {
        return ['adminController.php', 'publicController.php', 'model.php'];
    }

    public function generate()
    {
        $post = Yii::$app->request->post();

        $saveModelSettings = SystemGii::find()->where(['=', 'table', $this->tableName])->one();
        if (!$saveModelSettings) {
            $saveModelSettings = new SystemGii();
        }
        
        $filter = ['fields', 'selects', 'links', 'manyLinks', 'children', 'parents'];
        foreach ($filter as $filterItem) {
            if (!is_array($this->{$filterItem})) $this->{$filterItem} = [];
            foreach ($this->{$filterItem} as $k => $field) {
                if ($field['name'] == '') {
                    unset($this->{$filterItem}[$k]);
                }
            }
            $this->{$filterItem} = array_values($this->{$filterItem});
        }
        
        $files = [];
        $files[] = new CodeFile(
            Yii::getAlias('@app/controllers/admin/parents') . '/' . $this->getControllerName() . 'ParentController.php',
            $this->render('adminController.php')
        );
        if (!file_exists(Yii::getAlias('@app/controllers/admin') . '/' . $this->getControllerName() . 'Controller.php')) {
            $files[] = new CodeFile(
                Yii::getAlias('@app/controllers/admin') . '/' . $this->getControllerName() . 'Controller.php',
                $this->render('adminControllerChild.php')
            );
        }
        $files[] = new CodeFile(
            Yii::getAlias('@app/models') . '/parents/' . $this->getModelName() . 'Parent.php',
            $this->render('model.php')
        );
        if (!file_exists(Yii::getAlias('@app/models') . '/' . $this->getModelName() . '.php')) {
            $files[] = new CodeFile(
                Yii::getAlias('@app/models') . '/' . $this->getModelName() . '.php',
                $this->render('child_model.php')
            );
        }

        if ($this->publicController) {
            if (!file_exists(Yii::getAlias('@app/controllers') . '/' . $this->getControllerName() . 'Controller.php')) {
                $files[] = new CodeFile(
                    Yii::getAlias('@app/controllers') . '/' . $this->getControllerName() . 'Controller.php',
                    $this->render('publicController.php')
                );
            }
            if (!file_exists(Yii::getAlias('@app/views') . '/' . $this->getHandleName() . '/index.php')) {
                $files[] = new CodeFile(
                    Yii::getAlias('@app/views') . '/' . $this->getHandleName() . '/index.php',
                    '<?php'
                );
            }
            if (!file_exists(Yii::getAlias('@app/views') . '/' . $this->getHandleName() . '/detail.php')) {
                $files[] = new CodeFile(
                    Yii::getAlias('@app/views') . '/' . $this->getHandleName() . '/detail.php',
                    '<?php'
                );
            }
        }

        /*$files[] = new CodeFile(
            Yii::getAlias('@app/views/admin') . '/' . $this->getHandleName() . '/update.php',
            $this->render('update.php')
        );*/
        /*$files[] = new CodeFile(
            Yii::getAlias('@app/views/admin') . '/' . $this->getHandleName() . '/create.php',
            '<?=$this->render("@rusbitles/adminbase/views/base/create", ["content" => $content])?>'
        );*/
        $files[] = new CodeFile(
            Yii::getAlias('@app/views/admin') . '/' .  $this->getHandleName() . '/_form.php',
            $this->render('form.php')
        );
        $files[] = new CodeFile(
            Yii::getAlias('@app/views/admin') . '/' .  $this->getHandleName() . '/index.php',
            $this->render('index.php')
        );
        if ($this->doMigration && $saveModelSettings->db != json_encode($this->migrationFields)) {
            for ($i = 0; $i < 20; $i++) {
                if (!file_exists(Yii::getAlias('@app/migrations') . '/m'.date('ymd').'_'.substr('000000'.$i, -6).'_gii_create_' . $this->tableName . '_table.php')) {
                    $this->migrationNum = $i;
                    break;
                }
            }
            $files[] = new CodeFile(
                Yii::getAlias('@app/migrations') . '/m'.date('ymd').'_'.substr('000000'.$this->migrationNum, -6).'_gii_create_' . $this->tableName . '_table.php',
                $this->render('migration.php', ['history' => json_decode($saveModelSettings->db, true)])
            );
        }
        if ($this->doMigration) {
            foreach($this->manyLinks as $link) {
                if(!Yii::$app->db->schema->getTableSchema($link['linktable'])) {
                    for ($i = 0; $i < 20; $i++) {
                        if (!file_exists(Yii::getAlias('@app/migrations') . '/m'.date('ymd').'_'.substr('000000'.$i, -6).'_gii_create_' . $link['linktable'] . '_table.php')) {
                            $this->migrationNum = $i;
                            break;
                        }
                    }

                    $files[] = new CodeFile(
                        Yii::getAlias('@app/migrations') . '/m'.date('ymd').'_'.substr('000000'.$this->migrationNum, -6).'_gii_create_' . $link['linktable'] . '_table.php',
                        $this->render('migrationmany2many.php', ['tn' => $link['linktable'], 'id1' => $link['id_1'], 'id2' => $link['id_2']])
                    );
                }
            }
        }
        if ($this->isTree) {
            /*$files[] = new CodeFile(
                Yii::getAlias('@app/views/admin') . '/' .  $this->getHandleName() . '/_tree_form.php',
                $this->render('_tree_form.php')
            );*/
            $files[] = new CodeFile(
                Yii::getAlias('@app/views/admin') . '/' .  $this->getHandleName() . '/tree.php',
                $this->render('tree.php')
            );
        }
        if ($this->doMail) {
            $files[] = new CodeFile(
                Yii::getAlias('@app/mail') . '/' .  $this->getHandleName() . '.php',
                $this->render('mail.php')
            );
        }

        if (isset($post['generate']) && $this->tableName != '') {
            $saveModelSettings->table = $this->tableName;
            $saveModelSettings->post = json_encode($post['Generator']);
            $saveModelSettings->db = json_encode($this->migrationFields);
            $saveModelSettings->time = time();
            $saveModelSettings->save();
        }

        return $files;
    }

    public function getControllerName() { return $this->getControllerString($this->tableName); }
    public function getModelName() { return $this->getModelFromString($this->tableName); }
    public function getHandleName() { return $this->getHandleFromString($this->tableName); }

    public function getModelLabels() {
        $nameParts = preg_split('/\s*,\s*/', $this->modelLabels);

        return "'" . implode("', '", $nameParts) . "'";
    }

    public function getModelLabelsArray() {
        $nameParts = preg_split('/\s*,\s*/', $this->modelLabels);

        return $nameParts;
    }

    public function getControllerString($str) {
        $nameParts = explode('_', $str);
        foreach ($nameParts as $k => $np) {
            $nameParts[$k] = strtolower($np);
            if ($k == 0) {
                $nameParts[$k] = ucfirst($np);
            }
        }

        return implode('', $nameParts);
    }

    public function getModelFromString($str) {
        $nameParts = explode('_', $str);
        foreach ($nameParts as $k => $np) {
            $nameParts[$k] = strtolower($np);
            $nameParts[$k] = ucfirst($np);
        }

        return implode('', $nameParts);
    }
    
    public function getHandleFromString($str) {
        $nameParts = explode('_', $str);
        foreach ($nameParts as $k => $np) {
            $nameParts[$k] = strtolower($np);
        }

        return implode('', $nameParts);
    }

    public function getHandleFromStringFB($str) {
        return ucfirst($this->getHandleFromString($str));
    }

    public function getName()
    {
        return 'Универсальный КРЮД под админку';
    }

    public function getDescription()
    {
        return '';
    }
    
    public function getHasAdditional() {
        $hasAdditional = false;
        foreach($this->children as $child) {
            if ($child['add']) {
                $hasAdditional = true;
            }
        }
        
        return $hasAdditional;
    }
    
    public function getMigrationFields() {
        $res = [];
        $tn = $this->tableName;

        $res[$tn]['id'] = '$this->primaryKey()';
        if(count($this->parents) > 0) {
            $res[$tn]['parent_id'] = '$this->integer()';
        }
        foreach($this->fields as $field) { 
            switch ($field['type']) {
                case 'string_255':
                case 'handle':
                case 'image':
                case 'email':
                case 'file':
                    $res[$tn][$field['name']] = '$this->string(255)';
                    break;
                case 'string_500':
                    $res[$tn][$field['name']] = '$this->string(500)';
                    break;
                case 'text':
                case 'table':
                    $res[$tn][$field['name']] = '$this->text()';
                    break;
                case 'date':
                case 'date_only':
                case 'integer':
                    $res[$tn][$field['name']] = '$this->integer()';
                    break;
                case 'float':
                    $res[$tn][$field['name']] = "'double'";
                    break;
                case 'checkbox':
                    $res[$tn][$field['name']] = "'tinyint'";
            } 
        }
        
        foreach($this->selects as $select) {
            $res[$tn][$select['name']] = '$this->string(255)';
        }

        foreach($this->links as $link) {
            $res[$tn][$link['link']] = '$this->integer()';
        }

        $hasAdditional = false;
        foreach($this->children as $child) {
            if ($child['add']) {
                $hasAdditional = true;
            }
        }
        if ($hasAdditional) {
            $res[$tn]['system_additional'] = '$this->string(255)';
        }

        if ($this->fieldWeight) {
            $res[$tn]['weight'] = "'smallint'";
        }

        if ($this->fieldPublic) {
            $res[$tn]['public'] = "'tinyint'";
        }
        
        if ($this->fieldTimes) {
            $res[$tn]['created_at'] = '$this->integer()';
            $res[$tn]['updated_at'] = '$this->integer()';
        }
        
        if ($this->fieldSeo) {
            $res[$tn]['seo_title'] = '$this->string(255)';
            $res[$tn]['seo_description'] = '$this->string(500)';
            $res[$tn]['seo_keywords'] = '$this->string(255)';
        }
        
        if ($this->isTree) {
            $res[$tn]['lft'] = '$this->integer()';
            $res[$tn]['rgt'] = '$this->integer()';
            $res[$tn]['depth'] = '$this->integer()';
        }
        
        if ($this->fieldHandle && !$this->isTree) {
            $res[$tn]['handle'] = '$this->string(255)';
        }
        
        if ($this->fieldHandle && $this->isTree) {
            $res[$tn]['handle'] = '$this->string(500)';
            $res[$tn]['real_handle'] = '$this->string(255)';
            $res[$tn]['tree_handle'] = "'tinyint'";
        }
        
        return $res;
    }
}