<?php
namespace rusbitles\adminbase\helpers;

use rusbitles\adminbase\models\CActiveRecord;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class FormTools {
    private $models = false;

    public static function begin($models, $action, $blockClass = false, $method = 'post', $interactive = false, $tagAttributes = [])
    {
        $res = new static();
        if (!is_array($models)) $models = [$models];
        if ($blockClass === false) {
            $blockClass = end(explode('\\', $models[0]->classname()));
        }
        $res->models = $models;

        echo "
            <div class='" . $blockClass . "FormBlock'>
                <div class='blockWrapper'>
                    <form".($interactive?(' data-source-block="#'.$interactive.'" data-target-block="#'.$interactive.'" id="'.$interactive.'"' ):'')." action='" . $action . "' method='" . $method . "' class='" . $blockClass . "Form' enctype='multipart/form-data' ". FormFields::tagAttributesToString($tagAttributes).">
        ";
        
        return $res;
    }
    
    public function end() {
        echo "
            <input type='hidden' name='" . Yii::$app->request->csrfParam . "' value='" . Yii::$app->request->csrfToken . "'/>
            <input type='hidden' name='email' value='1' />
        ";

        echo "
                    </form>
                </div>
            </div>
        ";
    }

    public function errors() {
        $errors = '';

        foreach($this->models as $model) {
            if ($model->hasErrors()) {
                $errors .= '<p>' . implode('</p><p>', $model->firstErrors) . '</p>';
            }
        }

        if ($errors != '') {
            echo '<div class="message message-errors">' . $errors . '</div>';
        }
    }

    public function success($message) {
        if ($this->models[0]->success) {
            echo '<div class="message message-success">' . $message . '</div>';
        }
    }
    
    public function fieldsetBegin($title = false, $tagAttributes = [], $textAfterStart = '') {
        if ($title !== false) {
            echo "<h2>".$title."</h2>";
        }
        
        echo "
            <fieldset ". FormFields::tagAttributesToString($tagAttributes).">".$textAfterStart."
                <ul class='fieldList'>
        ";
    }

    public function fieldsetEnd() {
        echo "
                </ul>
            </fieldset>
        ";
    }

    public function fieldHidden($model, $attribute) {
        echo "<input type='hidden' name='".$model->formName()."[".$attribute."]' value='".$model->{$attribute}."' />";
    }

    /**
     * @param $model ActiveRecord
     */
    public function field($model, $attribute, $label = false, $fieldclasses = []) {
        $template = "
            <li class='field".($model->isAttributeRequired($attribute)?' field-important':'').(count($fieldclasses)>0?(" field-".implode(" field-", $fieldclasses)):"")."[[liclass]]'>
                <label>
                    <strong class='title'>".($label?$label:$model->getAttributeLabel($attribute))."</strong>
                    <span class='wrapper'>[[content]]</span>
                </label>
            </li>
        ";
        
        return new FormFields($template, $model, $attribute);
    }

    public function fieldBegin($model, $attribute) {
        $template = "
            <li class='field[[liclass]]'>
                <label>
                    <strong class='title'>".$model->getAttributeLabel($attribute)."</strong>
                    <span class='wrapper'>[[content]]</span>
                </label>
        ";

        return new FormFields($template, $model, $attribute);
    }
    
    public function fieldEnd() {
        echo "</li>";
    }

    /**
     * @param $model CActiveRecord
     * @param $attribute
     */
    public function fieldCheckbox($model, $attribute) {
        echo "
            <label>
                <input value='1' type='checkbox' name='".$model->getAttributeFormName($attribute)."'".(($model->{$attribute})?' checked':'')."/> <span class='caption'>".$model->getAttributeLabel($attribute).($model->isAttributeRequired($attribute)?' *':'')."</span>
            </label>
        ";
    }

    public function fieldgroupBegin($title = false, $class = false) {
        $class = ((is_array($class)||($class===false))?$class:[$class]);
        
        echo "
            <li class='fieldGroup".(($class===false)?'':' fieldGroup-'.implode(' fieldGroup-', $class))."'>
                ".(($title===false)?"":"<strong class='title'>".$title."</strong>")."
                <ul class='fieldList'>
        ";
    }

    public function fieldgroupEnd() {
        echo "
                </ul>
            </li>
        ";
    }

    public function linkBlockBegin($classes = '') {
        echo "
            <div class='linkBlock ".$classes."'>
        ";
    }

    public function linkBlockEnd() {
        echo "</div>";
    }
    
    public function link($url, $label, $classes = '') {
        echo "<a href='".$url."' class='link ".$classes."'>".$label."</a>";
    }
    
    public function submit($title, $links = [], $comment = false) {
        if (is_array($title)) {
            $buttons = $title;
        } else {
            $buttons = [$title];
        }

        echo "
            <div class='submitBlock'>
        ";

        foreach($buttons as $k => $v) {
            echo "
                <button class='submit'".(is_numeric($k)?'':" name='".$k."'").">".$v."</button>
            ";
        }
        
        foreach($links as $k => $v) {
            echo "
                <a href='".$k."'>".$v."</a>
            ";
        }

        if ($comment !== false) {
            echo "<div class='commentBlock'>".$comment."</div>";
        }
        echo "</div>";
    }

    public function submitBegin($title) {

        if (is_array($title)) {
            $buttons = $title;
        } else {
            $buttons = [$title];
        }

        echo "
            <div class='submitBlock'>
        ";

        foreach($buttons as $k => $v) {
            echo "
                <button class='submit'".(is_numeric($k)?'':" name='".$k."'").">".$v."</button>
            ";
        }
    }

    public function submitEnd() {
        if ($this->models[0]->success) return;

        echo "</div>";
    }
}

class FormFields {
    private $template;

    /**
     * @var ActiveRecord
     */
    private $model;

    private $attribute;

    public function __construct($template, $model, $attribute)
    {
        $this->template = $template;
        $this->model = $model;
        $this->attribute = $attribute;
    }

    public function textInput($type = 'text', $tagAttributes = [], $value = false) {
        $content = "
            <input type='".$type."' name='".$this->model->getAttributeFormName($this->attribute)."' value='".($value!==false?$value:$this->model->getValue($this->attribute))."' " . static::tagAttributesToString($tagAttributes) . ($this->model->isAttributeRequired($this->attribute)?' required':'') ."/>
        ";
        $liclass = '';
        if ($type == 'tel') {
            $liclass .= ' field-phone';
        }
        echo str_replace(['[[content]]', '[[liclass]]'], [$content, $liclass], $this->template);
    }

    public function textArea($tagAttributes = []) {
        $content = "
            <textarea name='".$this->model->getAttributeFormName($this->attribute)."' " . static::tagAttributesToString($tagAttributes) .($this->model->isAttributeRequired($this->attribute)?' required':''). ">".$this->model->getValue($this->attribute)."</textarea>
        ";
        echo str_replace(['[[content]]', '[[liclass]]'], [$content, ' field-textarea'], $this->template);
    }

    public function fileInput($tagAttributes = []) {
        $content = "
            <input type='file' name='".$this->model->getAttributeFormName($this->attribute)."' " . static::tagAttributesToString($tagAttributes) . ($this->model->isAttributeRequired($this->attribute)?' required':'') ."/>
        ";
        echo str_replace(['[[content]]', '[[liclass]]'], [$content, ' field-file'], $this->template);
    }

    public function dateInput($tagAttributes = []) {
        $content = "
            <input type='text' name='".$this->model->getAttributeFormName($this->attribute)."' " . static::tagAttributesToString($tagAttributes) . ($this->model->isAttributeRequired($this->attribute)?' required':'') ."/>
        ";
        echo str_replace(['[[content]]', '[[liclass]]'], [$content, ' field-date'], $this->template);
    }

    public function select($options, $multiple = false, $tagAttributes = [], $value = false) {
        if ($multiple) $tagAttributes[] = 'multiple';

        $content = "<select name='".$this->model->getAttributeFormName($this->attribute).($multiple?'[]':'')."' " . static::tagAttributesToString($tagAttributes) . ($this->model->isAttributeRequired($this->attribute)?' required':'').">";
        $content .= "<option></option>";
        foreach($options as $k => $option) {
            $selected = false;
            if($value !== false) {
                $selected = $k == $value;
            } else {
                if (is_array($this->model->{$this->attribute}) && in_array($k, $this->model->{$this->attribute})) {
                    $selected = true;
                }
                if (!is_array($this->model->{$this->attribute}) && $k == $this->model->{$this->attribute}) {
                    $selected = true;
                }
            }

            $content .= "<option label='".current(explode(' | ', $option))."' value='".$k."'".($selected?' selected':'').">".$option."</option>";
        }
        $content .= "</select>";
        echo str_replace(['[[content]]', '[[liclass]]'], [$content, ''], $this->template);
    }

    public function select2($options, $multiple = false, $tagAttributes = [], $value = false) {
        if ($multiple) $tagAttributes[] = 'multiple';

        $content = "<select name='".$this->model->getAttributeFormName($this->attribute).($multiple?'[]':'')."' " . static::tagAttributesToString($tagAttributes) . ($this->model->isAttributeRequired($this->attribute)?' required':'').">";
        $content .= "<option></option>";
        foreach($options as $k => $option) {
            $selected = false;
            if($value !== false) {
                $selected = $k == $value;
            } else {
                if (is_array($this->model->{$this->attribute}) && in_array($k, $this->model->{$this->attribute})) {
                    $selected = true;
                }
                if (!is_array($this->model->{$this->attribute}) && $k == $this->model->{$this->attribute}) {
                    $selected = true;
                }
            }

            $content .= "<option data-parent-id='".$option['parent']."' label='".current(explode(' | ', $option['value']))."' value='".$k."'".($selected?' selected':'').">".$option['value']."</option>";
        }
        $content .= "</select>";
        echo str_replace(['[[content]]', '[[liclass]]'], [$content, ''], $this->template);
    }

    public static function tagAttributesToString($tagAttributes = []) {
        $tagAttributesRes = [];
        foreach($tagAttributes as $k => $tagAttribute) {
            if (is_numeric($k)) {
                $tagAttributesRes[] = $tagAttribute;
            } else {
                $tagAttributesRes[] = $k . '="' . $tagAttribute . '"';
            }
        }

        return implode(' ', $tagAttributesRes);
    }
}