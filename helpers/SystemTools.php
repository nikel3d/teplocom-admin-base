<?php
namespace rusbitles\adminbase\helpers;

use rusbitles\adminbase\components\Pagination;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class SystemTools {
    public static function isCurPage($page){
        $curPage = current(explode('?', $_SERVER['REQUEST_URI']));
        return $page==$curPage;
    }
    
    public static function getUrl($add = [], $del = []) {
        list($url, $params) = explode('?', $_SERVER['REQUEST_URI']);
        if ($params != '') {
            $params = explode('&', $params);
        } else {
            $params = [];
        }
        
        $resparams = [];
        foreach($params as $param) {
            $param = explode('=', $param);
            if (!in_array($param[0], $del) && empty($add[$param[0]])) {
                $resparams[] = implode('=', $param);
            }
        }

        foreach($add as $addKey => $addItem) {
            $resparams[] = $addKey . '=' . $addItem;
        }

        if (count($resparams) > 0) {
            return $url . '?' . implode('&', $resparams);
        }
        return $url;
    }

    public static function full_pathinfo($path_file){
        $path_file = strtr($path_file,array('\\'=>'/'));

        $exp = explode('/', $path_file);
        $file = array_pop($exp);
        $dirname = implode('/', $exp);
        $fileexp = explode('.', $file);
        $extension = array_pop($fileexp);

        return array(
            'dirname' => $dirname,
            'basename' => $file,
            'extension' => $extension,
            'filename' => implode('.', $fileexp)
        );
    }

    public static function fileSize($path_file) {
        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $path_file)) {
            $size = filesize($_SERVER['DOCUMENT_ROOT'] . $path_file);

            return self::formatSizeUnits($size);
        }

        return '';
    }

    public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' Gb';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' Mb';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' Kb';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public static function loadImage ($model, $img_var, $uploadDir = '@webroot/up/common') {
        $dir_name = \Yii::getAlias($uploadDir);
        $doc_root = \Yii::getAlias('@webroot');

        if (!file_exists($dir_name)) {
            mkdir($dir_name);
        }

        $varName = preg_replace('/\[[0-9]*\]/', '', $img_var);
        
        $buffImg = UploadedFile::getInstance($model, $img_var);
        if ($buffImg) {
            $extentionName = current(array_reverse(explode('.', $buffImg->name)));
            $rawFilename = TextHelper::translit($extentionName ? str_replace('.' . $extentionName, '', $buffImg->name) : $buffImg->name);
            $newRawFilename = AdminTools::generateSafeFilename($dir_name, $rawFilename, $extentionName);
            $newFilename = $newRawFilename . '.'.$extentionName;
            $buffImg->saveAs($dir_name . '/' . $newFilename);
            $model->$varName = str_replace(array($doc_root, '\\'), array('', '/'), $dir_name . '/' . $newFilename);

            return true;
        } else {
            if ($model->getOldAttribute($varName) != '') {
                $model->$varName = $model->getOldAttribute($varName);
            }

            return false;
        }
    }

    public static function loadImageBase64($model, $img_var, $title, $file, $uploadDir = '@webroot/up/common') {
        $dir_name = \Yii::getAlias($uploadDir);
        $doc_root = \Yii::getAlias('@webroot');

        if (!file_exists($dir_name)) {
            mkdir($dir_name);
        }

        $varName = preg_replace('/\[[0-9]*\]/', '', $img_var);
        
        $extentionName = current(array_reverse(explode('.', $title)));
        $rawFilename = TextHelper::translit($extentionName ? str_replace('.' . $extentionName, '', $title) : $title);
        $newRawFilename = AdminTools::generateSafeFilename($dir_name, $rawFilename, $extentionName);
        $newFilename = $newRawFilename . '.'.$extentionName;
        file_put_contents($dir_name . '/' . $newFilename, base64_decode($file));
        $model->$varName = str_replace(array($doc_root, '\\'), array('', '/'), $dir_name . '/' . $newFilename);

        return true;
    }

    public static function loadImageCopyFile($model, $img_var, $title, $filepath, $uploadDir = '@webroot/up/common') {
        $dir_name = \Yii::getAlias($uploadDir);
        $doc_root = \Yii::getAlias('@app/web');

        if (!file_exists($dir_name)) {
            mkdir($dir_name);
        }

        $varName = preg_replace('/\[[0-9]*\]/', '', $img_var);

        $extentionName = current(array_reverse(explode('.', $title)));
        $rawFilename = TextHelper::translit($extentionName ? str_replace('.' . $extentionName, '', $title) : $title);
        $newRawFilename = AdminTools::generateSafeFilename($dir_name, $rawFilename, $extentionName);
        $newFilename = $newRawFilename . '.'.$extentionName;
        rename($filepath, $dir_name . '/' . $newFilename);
        $model->$varName = str_replace(array($doc_root, '\\'), array('', '/'), $dir_name . '/' . $newFilename);

        return true;
    }

    public static function getExternalFile($url, $uploadDir, $newName = '') {
        $dir_name = 'up/' . \Yii::getAlias($uploadDir);
        $doc_root = \Yii::getAlias('@webroot');

        if (!file_exists($dir_name)) {
            mkdir($dir_name);
        }

        $file = file_get_contents($url);
        $fp = self::full_pathinfo($url);
        if ($newName == '') {
            $newName = TextHelper::translit($fp['filename']);
        }
        $fileWay = AdminTools::generateSafeFilename($dir_name, $newName, $fp['extension']).'.'.$fp['extension'];
        $fileWay = str_replace(array($doc_root, '\\'), array('', '/'), $dir_name . '/' . $fileWay);
        $fileWay = current(explode('?', $fileWay));

        file_put_contents($fileWay, $file);
        return '/' . $fileWay;
    }

    public static function getMenu($class, $route) {
        $menu = [];

        $nClass = 'app\\models\\'.$class;

        $items = $nClass::find()->published()->andWhere(['>', 'depth', 1])->orderBy('lft')->all();
        foreach ($items as $item) {
            $menu[$class . $item->id] = [
                'label' => $item->title,
                'url' => Yii::$app->urlManager->createUrl([$route, 'handle' => $item->handle]),
                'active' => false,
                'handle' => $item->handle,
                'children' => array(),
                'is_child' => ($item->depth==2)?false:true,
            ];
            if ($item->depth > 2) {
                $parent = $item->parents(1)->one();
                $menu[$class . $parent->id]['children'][] = $class . $item->id;
            }
        }

        return $menu;
    }

    public static function setActiveMenu(&$menu, $selId, $id = false) {
        foreach ($menu as $mCode => $mItem) {
            if (!$mItem['is_child'] && $id === false || $id !== false && in_array($mCode, $menu[$id]['children'])) {
                if ($mCode == $selId) {
                    $menu[$mCode]['active'] = true;
                } else {
                    $menu[$mCode]['active'] = self::setActiveMenu($menu, $selId, $mCode);
                }
                if ($menu[$mCode]['active']) return true;
            }
        }

        return false;
    }
    
    public static function addBreadcrumbs($menu, $controller, $id = false) {
        foreach ($menu as $mCode => $mItem) {
            if (!$mItem['is_child'] && $id === false || $id !== false && in_array($mCode, $menu[$id]['children'])) {
                if ($mItem['active']) {
                    $controller->addBreadCrumb($mItem['url'], $mItem['label']);
                    self::addBreadcrumbs($menu, $controller, $mCode);
                }
            }
        }
    }

    public static function addBreadcrumbsTree($item, $controller, $route) {
        $bc = [];
        while ($item->depth != 1) {
            $bc[] = [
                'url' => Yii::$app->urlManager->createUrl([$route, 'handle' => $item->handle]),
                'title' => $item->title,
            ];
            $item = $item->parents(1)->one();
        }
        $bc = array_reverse($bc);
        
        foreach ($bc as $bcItem) {
            $controller->addBreadCrumb($bcItem['url'], $bcItem['title']);
        }
    }

    public static function getReferrer() {
        $ref = Yii::$app->request->referrer;
        $ref = preg_replace('/[\?\&]v\=[0-9]+/', '', $ref);

        if (strpos($ref, '?') === false) {
            return $ref.'?v='.time();
        } else {
            return $ref.'&v='.time();
        }
    }
    
    public static function explodeURL($url) {
        $url = explode('?', $url);
        $url[0] = explode('/', $url[0]);
        if (count($url) > 1) {
            $url[1] = explode('&', $url[1]);
        }
        
        return $url;
    }
    public static function implodeURL($url) {
        if (count($url) > 1) {
            $url[1] = implode('&', $url[1]);
        }
        $url[0] = implode('/', $url[0]);
        $url = implode('?', $url);
        
        return $url;
    }
    
    public static function getPages($cnt, $size, $route, $params, $page) {
        $pages = new Pagination(['totalCount' => $cnt]);
        $pages->pageSize = $size;
        $pages->route = $route;
        $pages->params = $params;
        $pages->page = $page;

        $pages->calculate();
        if ($pages->maxPage <= $page && $pages->maxPage != 0) throw new NotFoundHttpException();
        
        return $pages;
    }
    
    public static function getRandomArray($seed, $count) {
        srand($seed);
        $nums = [];
        while ($count > count($nums)) {
            $newNum = rand(0, $count - 1);
            if (!in_array($newNum, $nums)) $nums[] = $newNum;
        }
        srand((double) microtime() * 1000000);
        
        return $nums;
    }

    private static $menuTemplates = [];
    public static function menuDraw($controller, $menu, $options = []) {
        $defaultOptions = [
            'template' => '@rusbitles/adminbase/views/basemenu',
            'virtualLvl' => [],
        ];

        $resOptions = ArrayHelper::merge($defaultOptions, $options);

        $templates = [];
        if (isset(static::$menuTemplates[$resOptions['template']])) {
            $templates = static::$menuTemplates[$resOptions['template']];
        } else {
            $lastFound = [
                'container' => $defaultOptions['template'] . '/container1',
                'row' => $defaultOptions['template'] . '/row1',
            ];
            for ($i = 1; $i <= 8; $i++) {
                if (file_exists(Yii::getAlias($resOptions['template'] . '/container'.$i.'.php')))
                    $lastFound['container'] = $resOptions['template'] . '/container'.$i;

                if (file_exists(Yii::getAlias($resOptions['template'] . '/row'.$i.'.php')))
                    $lastFound['row'] = $resOptions['template'] . '/row'.$i;

                $templates[$i]['container'] = $lastFound['container'];
                $templates[$i]['row'] = $lastFound['row'];
            }

            static::$menuTemplates[$resOptions['template']] = $templates;
        }

        $ids = [];
        foreach($menu as $k => $menuItem) {
            if (count($menuItem['parents']) == 0) $ids[] = $k;
        }

        return static::menuDrawRecursive($controller, $menu, $ids, $templates, $resOptions);
    }

    public static function menuDrawRecursive($controller, $menu, $ids, $templates, $options, $lvl = 1) {
        $containerContent = '';
		$not = array('c_238','c_239','c_240','c_241','c_242','c_243','c_345'); //[di]

        foreach($ids as $id) {
			if(in_array($id, $not)) continue;
            $rowContent = '';
            if (count($menu[$id]['children']) > 0) {
                if (empty($options['virtualLvl'][$lvl])) {
                    $rowContent = static::menuDrawRecursive($controller, $menu, $menu[$id]['children'], $templates, $options, $lvl + 1);
                } elseif ($options['virtualLvl'][$lvl]['type'] == 'columns') {
                    $columns = $options['virtualLvl'][$lvl]['value'];
                    $count = count($menu[$id]['children']);
                    $k = 0;
                    $colContent = '';
                    for($i = 0; $i < $columns; $i++) {
                        $chIds = [];
                        for($j = 0; $j < ceil(($count - $i)/$columns); $j++) {
                            $chIds[] = $menu[$id]['children'][$k++];
                        }
                        $colContent .= static::menuDrawRecursive($controller, $menu, $chIds, $templates, $options, $lvl + 2);
                    }
                    $rowContent = $controller->render($templates[$lvl + 1]['container'], ['content' => $colContent, 'lvl' => $lvl + 1, 'options' => $options]);
                }
            }

            $containerContent .= $controller->render($templates[$lvl]['row'], ['menu' => $menu[$id], 'content' => $rowContent, 'lvl' => $lvl, 'options' => $options]);
        }

        return $controller->render($templates[$lvl]['container'], ['content' => $containerContent, 'lvl' => $lvl, 'options' => $options]);
    }
}