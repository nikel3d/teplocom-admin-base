<?php
namespace rusbitles\adminbase\helpers;

use app\models\Property;
use Yii;

class CatalogHelper {
    public static function getFilter($category, $filter, $priceID) {
        $filter = !empty($filter['p'])?$filter['p']:[];
        ksort($filter);

        $filterRes = Yii::$app->cache->get(['filter', $category->id, $filter, $priceID]);

        if (!$filterRes || YII_DEBUG) {
            $connection = Yii::$app->getDb();
            $command = $connection->createCommand("
            SELECT COUNT(*) cnt, cp.`variant_id`, `property_id`
            FROM `catalog_property` cp
            LEFT JOIN `catalog` c ON (c.`id` = cp.`parent_id`)
            LEFT JOIN `catalog2category` c2c ON (c.`id` = c2c.`catalog_id`)
            LEFT JOIN `category` ca ON (c2c.`category_id` = ca.`id`)
            WHERE c.`public`>0 AND ca.`lft` >= :lft AND ca.`rgt` <= :rgt 
            GROUP BY cp.`variant_id`",
                [
                    ':lft' => $category->lft,
                    ':rgt' => $category->rgt,
                ]
            );
            $result = $command->queryAll();

            $props = [];

            foreach ($result as $res) {
                $props[$res['property_id']] = 'Y';
            }

            $fresult = [];
            if (count($filter) > 0) {
                // Фиксируем какие то свойства
                $changeFilterProps = Property::find()->published()->andWhere(['=', 'is_filter', 1])->all();

                foreach ($changeFilterProps as $curProp) {
                    $addFilter = [];
                    $newAddFilter = [];
                    $pcount = 0;
                    if (!empty($props[$curProp->id])) {
                        foreach ($changeFilterProps as $prop) {
                            if (!empty($filter[$prop->id]) && $curProp->id != $prop->id) {
                                $addFilter[] = 'cp.property_id=' . $prop->id . ' AND cp.variant_id IN (' . implode(',', $filter[$prop->id]) . ')';
                                $newAddFilter = array_merge($newAddFilter, $filter[$prop->id]);
                                $pcount++;
                            }
                        }
                        
                        if (count($addFilter) > 0) {
                            $command = $connection->createCommand("
                                SELECT COUNT(*) cnt, cp.`variant_id`, `property_id`
                                FROM `catalog_property` cp
                                LEFT JOIN `catalog` c ON (c.`id` = cp.`parent_id`)
                                LEFT JOIN `catalog2category` c2c ON (c.`id` = c2c.`catalog_id`)
                                LEFT JOIN `category` ca ON (c2c.`category_id` = ca.`id`)
                                WHERE c.`public`>0 AND ca.`lft` >= :lft AND ca.`rgt` <= :rgt AND c.`ID` IN (
                                    SELECT pid FROM (
                                    SELECT `parent_id` as pid, COUNT(*) cnt
                                    FROM (
                                    SELECT *
                                    FROM `catalog_property`
                                    WHERE `variant_id` IN (" . implode(', ', $newAddFilter) . ")
                                    GROUP BY `property_id`, `parent_id`) AS cat_prop
                                    GROUP BY `parent_id`) as pids
                                    WHERE cnt >= ".$pcount."
                                )
                                GROUP BY cp.`variant_id`",
                                [
                                    ':lft' => $category->lft,
                                    ':rgt' => $category->rgt,
                                ]
                            );
                            $fresult[$curProp->id] = $command->queryAll();
                        }
                    }
                }
            }

            $cnts = [];

            foreach ($result as $res) {
                if (count($fresult[$res['property_id']]) == 0) {
                    $cnts[$res['property_id']][$res['variant_id']] = $res['cnt'];
                } else {
                    $cnts[$res['property_id']][$res['variant_id']] = 0;
                }
            }

            foreach ($fresult as $curPropId => $curProp) {
                foreach ($curProp as $res) {
                    if ($res['property_id'] == $curPropId) {
                        $cnts[$res['property_id']][$res['variant_id']] = $res['cnt'];
                    }
                }
            }

            $props = array_keys($props);

            $price = [];
            $orderDirs = ['max' => 'DESC', 'min' => 'ASC'];
            foreach ($orderDirs as $odKey => $od) {
                $command = $connection->createCommand("
                    SELECT price
                    FROM `catalog_price` cp
                    LEFT JOIN `catalog` c ON (c.`id` = cp.`parent_id`)
                    LEFT JOIN `catalog2category` c2c ON (c.`id` = c2c.`catalog_id`)
                    LEFT JOIN `category` ca ON (c2c.`category_id` = ca.`id`)
                    WHERE c.`public`>0 AND ca.`lft` >= :lft AND ca.`rgt` <= :rgt AND cp.`price_id` = :pid
                    ORDER BY cp.price+0 " . $od . "
                    LIMIT 1",
                    [
                        ':lft' => $category->lft,
                        ':rgt' => $category->rgt,
                        ':pid' => $priceID,
                    ]
                );
                $result = $command->queryAll();

                $price[$odKey] = (count($result) > 0) ? $result[0]['price'] : 0;
            }

            $filterRes = [
                'p' => $props,
                'c' => $cnts,
                'price' => $price,
            ];
            Yii::$app->cache->set(['filter', $category->id, $filter, $priceID], $filterRes, 86400);
        }
        $filterRes['p'] = Property::find()->where(['IN', 'id', $filterRes['p']])->andWhere(['>', 'public', '0'])->andWhere(['=', 'is_filter', '1'])->orderBy('weight')->all();

        return $filterRes;
    }
}