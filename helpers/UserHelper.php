<?php

namespace rusbitles\adminbase\helpers;

use Yii;
use yii\web\Cookie;

class UserHelper {
    public static $cookieTMP = [];

    public static function getParam($arKey, $default = '') {
        $params = self::getSumArray('params');
        if (!empty($params[$arKey])) {
            return json_decode($params[$arKey], true);
        }

        return $default;
    }

    public static function setParam($arKey, $value) {
        $params = self::getSumArray('params');
        $params[$arKey] = json_encode($value);
        self::setSumArray('params', $params);
    }

    public static function initSumArray($arVar) {
        if (Yii::$app->user->isGuest) {
            if (!isset(Yii::$app->request->cookies[$arVar]) && !isset(self::$cookieTMP[$arVar])) {
                self::setSumArray($arVar, []);
            }
        } else {
            $array = json_decode(Yii::$app->user->identity->cookieArray, true);
            if (!isset($array[$arVar])) {
                self::setSumArray($arVar, []);
            }
        }
    }

    public static function getSumArray($arVar, $refresh = false) {
        self::initSumArray($arVar);
        $result = [];

        if (!isset(self::$cookieTMP[$arVar])) {
            if (Yii::$app->user->isGuest) {
                $res = Yii::$app->request->cookies[$arVar];
                if ($res != '') {
                    $result = json_decode($res, true);
                }
            } else {
                if ($refresh) {
                    Yii::$app->user->identity->refresh();
                }
                $array = json_decode(Yii::$app->user->identity->cookieArray, true);
                $result = $array[$arVar];
            }
            self::$cookieTMP[$arVar] = $result;
        }
        return self::$cookieTMP[$arVar];
    }

    public static function setSumArray($arVar, $arr) {
        if (Yii::$app->user->isGuest) {
            Yii::$app->response->cookies->add(new Cookie(array_merge([
                'name' => $arVar,
                'value' => json_encode($arr),
                'expire' => (count($arr) > 0)?time() + 3600 * 24 * 7:time()-1000,
            ], !empty(Yii::$app->params['cookie_domain'])?['domain' => '.'.Yii::$app->params['cookie_domain']]:[])));
        } else {
            $array = json_decode(Yii::$app->user->identity->cookieArray, true);
            $array[$arVar] = $arr;
            Yii::$app->user->identity->cookieArray = json_encode($array);
            Yii::$app->user->identity->save(false, ['cookieArray']);
        }

        self::$cookieTMP[$arVar] = $arr;
    }

    public static function isInSumArray($arVar, $vID) {
        $res = self::getSumArray($arVar);
        if (!empty($res[$vID])) return true;

        return false;
    }

    public static function addToSumArray($arVar, $vID, $vVal, $add = true) {
        $res = self::getSumArray($arVar);
        if (!empty($res[$vID]) && $add) {
            $res[$vID] = floatval($res[$vID]) + floatval($vVal);
        } else {
            $res[$vID] = floatval($vVal);
        }
        if ($res[$vID] <= 0) {
            unset($res[$vID]);
        }

        self::setSumArray($arVar, $res);
    }

    public static function clearSumArray($arVar) {
        self::setSumArray($arVar, []);
    }

    public static function convertSumArray($arVar) {
        if (!Yii::$app->user->isGuest) {
            $res = Yii::$app->request->cookies[$arVar];
            if ($res != '') $arr = json_decode($res, true);

            if (is_array($arr)) {
                foreach ($arr as $k => $v) {
                    self::addToSumArray($arVar, $k, $v);
                }

                Yii::$app->response->cookies->add(new Cookie([
                    'name' => $arVar,
                    'value' => json_encode([]),
                    'expire' => time(),
                ]));
            }
        }
    }
}

?>