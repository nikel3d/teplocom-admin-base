<?php
    namespace rusbitles\adminbase\helpers;

    use dosamigos\datetimepicker\DateTimePicker;
    use kartik\color\ColorInput;
    use kartik\select2\Select2;
    use mihaildev\ckeditor\CKEditor;
    use mihaildev\elfinder\ElFinder;
    use rusbitles\adminbase\Admin;
    use Yii;
    use yii\helpers\Html;
    use yii\jui\DatePicker;
    use yii\web\JsExpression;

    class AdminTools {
        public static function generateColorInput($model, $form, $var_name) {
            echo $form->field($model, $var_name)->widget(ColorInput::classname(), [
                'options' => ['placeholder' => 'Выберите цвет ...'],
            ]);
        }

        public static function generateImageInputs($model, $form, $image_var) {
            if ($model->$image_var) { ?>
                <div class="form-group" >
                    <label class="control-label col-sm-2" >Просмотр</label >

                    <div class="col-sm-8" >
                        <img style = "max-width: 800px;" src="<?=$model->$image_var?>" alt="" /><br />
                        <?=Html::checkbox($image_var.'_deleting', false, []);?> - удалить?
                    </div >
                </div>
            <?php
                //echo $form->field($model, $image_var . 'deleting')->checkbox([], false);
            }
            echo $form->field($model, $image_var)->fileInput();
        }

        public static function generateFileInputs($model, $form, $file_var) {
            if ($model->$file_var) { $fileInfo = SystemTools::full_pathinfo($model->$file_var); ?>
                <div class="form-group">
                    <label class="control-label col-sm-2">Просмотр</label>

                    <div class="col-sm-8">
                        <a href="<?=$model->$file_var?>" target="_blank"><?=$fileInfo['basename']?></a><br />
                        <?=Html::checkbox($file_var.'_deleting', false, []);?> - удалить?
                    </div>
                </div>
                <?php
            }
            echo $form->field($model, $file_var)->fileInput();
        }

        public static function generateSafeFilename($dir, $filename, $extension){
            $newFilename = $filename.'.'.$extension;
            $i = 0;
            if ($dir[strlen($dir)-1] !== DIRECTORY_SEPARATOR)
                $dir .= DIRECTORY_SEPARATOR;
            while(file_exists($dir.$newFilename)){
                $i++;
                $newFilename = $filename.'_'.$i.'.'.$extension;
            }
            return $filename.($i>0?'_'.$i:'');
        }

        public static function generateDatepicker($model, $form, $image_var, $onlyDate = false) {
            return $form->field($model, $image_var)->widget(DateTimePicker::className(),[
                'language' => 'ru',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => $onlyDate?'dd.mm.yyyy':'dd.mm.yyyy HH:ii',
                    'todayBtn' => true
                ],
            ]);
        }

        public static function generateOnlyDatepicker($model, $form, $image_var) {
            return $form->field($model, $image_var)->widget(DatePicker::className(), [
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
                'options' => [
                    'class' => 'form-control',
                ]
            ]);
        }

        public static function generateOnlyDatepickerWithBooking($model, $form, $image_var, $booking) {
            return $form->field($model, $image_var)->widget(DatePicker::className(), [
                'language' => 'ru',
                'dateFormat' => 'dd.MM.yyyy',
                'options' => [
                    'class' => 'form-control',
                ],
                'clientOptions' => [
                    'beforeShowDay' => new \yii\web\JsExpression('function(date) {
                            var bDates = '.json_encode($booking).';
                            var dTime = date.getTime() / 1000;
                            var nTime = new Date();
                            nTime = nTime / 1000;
                            
                            if (nTime > dTime) {
                                return [false, ""];
                            }
                            
                            if (dTime in bDates) {
                                if (bDates[dTime]) {
                                    return [true, "free"];
                                } else {
                                    return [false, "busy"];
                                }
                            }
                            
                            return [false, "unknown"];
                        }'),
                ],
            ]);
        }

        public static function generateRedactor($model, $form, $image_var) {
            return $form->field($model, $image_var)->widget(CKEditor::className(),[
                'editorOptions' => ElFinder::ckeditorOptions(['elfinder']),
            ]);

            /*return $form->field($model, $image_var)->widget(Redactor::className(),[
                'clientOptions' => [
                    'imageManagerJson' => ['/redactor/upload/image-json'],
                    'imageUpload' => ['/redactor/upload/image'],
                    'fileUpload' => ['/redactor/upload/file'],
                    'lang' => 'ru',
                    'plugins' => ['fontcolor','imagemanager', 'table', 'video'],
                    'formatting' => ['p', 'u', 'blockquote', 'div', 'h2', 'h3', 'h4', 'h5', 'h6'],
                    'focus' => true,
                    'formattingAdd' => [*/
                        /*'div-contacts' => [
                            'tag' => 'div',
                            'title' => 'Блок контакты',
                            'class' => 'contacts',
                        ],*/
                    /*],
                ]
            ]);*/

            /*return $form->field($model, $image_var)->widget(TinyMce::className(), [
                'options' => ['rows' => 6],
                'language' => 'ru',
                'clientOptions' => [
                    'plugins' => [
                        "advlist autolink lists link charmap print anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table contextmenu paste image responsivefilemanager filemanager"
                    ],
                    'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media",
                    'external_filemanager_path' => '/plugins/responsive_filemanager/filemanager/',
                    'filemanager_title' => 'Файловый менеджер',
                    'external_plugins' => [
                        'filemanager' => '/plugins/responsive_filemanager/filemanager/plugin.min.js',
                        'responsivefilemanager' => '/plugins/responsive_filemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                    ],
                ]
            ]);*/
        }

        public static function generateSelect2($model, $form, $variable, $data, $multiple = false, $placeholder = '', $ajax = false) {
            $admin = Admin::getInstance();
            $options = [
                'language' => 'ru',
                'options' => [
                    'placeholder' => $placeholder,
                    'multiple' => $multiple,
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ];
            if ($ajax !== false) {
                $options['pluginOptions']['ajax'] = [
                    'url' => Yii::$app->urlManager->createUrl([$admin->id . '/ajax/dropdownlist', 'options' => [
                        'mn' => $ajax['model'],
                        'tmpl' => $ajax['template'],
                        'attr' => $ajax['attribute'],
                        'for' => $ajax['for'],
                    ]]),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) { console.log(params);return {q:params.term}; }')
                ];
                //$options['pluginOptions']['escapeMarkup'] = new JsExpression('function (markup) { return markup+"2"; }');
                $options['pluginOptions']['templateResult'] = new JsExpression('function (item) { return item.text; }');
                $options['pluginOptions']['templateSelection'] = new JsExpression('function (item) { return item.text; }');
                $options['pluginOptions']['minimumInputLength'] = 3;
            } else {
                $options['data'] = $data;
            }
            return $form->field($model, $variable)->widget(Select2::classname(), $options);
        }

        public static function generateTableDatas($model, $form, $variable, $cols = 2, $templates = []) {
            $settings = $model->getTableSettings($variable);
            if (empty($settings['items'])) $settings['items'] = [];
            if (empty($settings['hidden'])) $settings['hidden'] = [];
            if (empty($settings['cols'])) $settings['cols'] = 1;
            if (empty($settings['colNames'])) $settings['colNames'] = [];
            
            return $form->field($model, $variable)->textInput(['class' => 'tableData', 'data-cols' => $settings['cols'], 'data-templates' => json_encode($settings)]);
        }
        
        public static $publicstatuses = [
            -1 => 'Скрыто (видно только админам)',
            0 => 'Деактивировано',
            1 => 'Активно',
        ];
    }