<?php

namespace rusbitles\adminbase\helpers;

use app\models\Order;

class Yandex {
    private static $demo = true;
    private static $shopID = 128519;
    private static $scID = 555658;
    private static $pass = 'D1uQhjiAdnCTSZyb';

    public static function payButton($booking, $target)
    {
        //return '<iframe frameborder="0" allowtransparency="true" scrolling="no" src="https://money.yandex.ru/embed/small.xml?account=' . \Yii::$app->params['yandexID'] . '&quickpay=small&any-card-payment-type=on&button-text=01&button-size=l&button-color=orange&targets='.urldecode($target).'&label=booking'.$booking->id.'&default-sum='.$booking->booking.'&successURL='.urlencode('http://'.\Yii::$app->params['domain'].'/yandexconfirm/success/'.$booking->id).'" width="242" height="54"></iframe>';
        return '<iframe frameborder="0" allowtransparency="true" scrolling="no" src="https://money.yandex.ru/embed/shop.xml?account=' . \Yii::$app->params['yandexID'] . '&quickpay=shop&payment-type-choice=on&mobile-payment-type-choice=on&writer=seller&targets='.urldecode($target).'&label=booking'.$booking->id.'&targets-hint=&default-sum='.$booking->booking.'&button-text=01&successURL='.urlencode('http://'.\Yii::$app->params['domain'].'/yandexconfirm/success/'.$booking->id).'" width="450" height="198"></iframe>';
    }

    public static function payForm($orderID) {
        $order = Order::findOne($orderID);
        if ($order) {
            if ($order->payed) {
                return 'Заказ оплачен';
            }

            $orderHiddens = [
                'shopId' => self::$shopID,
                'scid' => self::$scID,
                'sum' => $order->price + $order->price_delivery,
                'orderNumber' => $order->id,
                'customerNumber' => ($order->user_id?$order->user_id:'Guest'),
                'paymentType' => 'AC',
            ];
            $orderData = [
                'customerContact' => $order->phone?$order->phone:$order->ur_phone,
                'items' => [],
            ];
            foreach($order->catalogs as $item) {
                $orderData['items'][] = [
                    'quantity' => $item->count,
                    'price' => ['amount' => $item->price],
                    'text' => $item->catalog->title,
                ];
            }

            if (self::$demo) {
                $form = '<form action="https://demomoney.yandex.ru/eshop.xml" method="post">';
            } else {
                $form = '<form action="https://money.yandex.ru/eshop.xml" method="post">';
            }
            foreach($orderHiddens as $hiddenKey => $hiddenVal) {
                $form .= '<input name="'.$hiddenKey.'" value="'.$hiddenVal.'" type="hidden"/>';
            }
            $form .= '<input name="ym_merchant_receipt" value=\''.json_encode($orderData).'\' type="hidden"/>';
            $form .= '<input type="submit" value="Оплатить"/>';
            $form .= '</form>';

            return $form;
        }

        return '';
    }

    public static function checkMD5($request) {
        $str = $request['action'] . ";" .
            $request['orderSumAmount'] . ";" . $request['orderSumCurrencyPaycash'] . ";" .
            $request['orderSumBankPaycash'] . ";" . $request['shopId'] . ";" .
            $request['invoiceId'] . ";" . $request['customerNumber'] . ";" . self::$pass;
        $md5 = strtoupper(md5($str));
        if ($md5 != strtoupper($request['md5'])) {
            return false;
        }
        return true;
    }
    
    public static function getOrderCheckResult($request, $payed) {
        $resArray = [
            'performedDatetime' => date('c'),
            'code' => 200,
            'invoiceId' => $request['invoiceId'],
            'shopId' => self::$shopID,
        ];
        if (self::checkMD5($request)) {
            $order = Order::findOne($request['orderNumber']);
            if ($order) {
                if($order->payed == 0 && $order->payment == 2) {
                    if (floatval($order->price + $order->price_delivery) == floatval($request['orderSumAmount'])) {
                        if($payed) {
                            $order->payed = 1;
                            $order->pay_date = time();
                            $order->pay_price = floatval($request['orderSumAmount']);
                            $order->tech_info = json_encode($request);

                            if($order->save(false)) {
                                $resArray['code'] = 0;
                            } else {
                                $resArray['message'] = implode(', ', $order->firstErrors);
                            }
                        } else {
                            $resArray['code'] = 0;
                        }
                    } else {
                        $resArray['message'] = 'Сумма оплаты не совпадает';
                    }
                } else {
                    $resArray['message'] = 'Заказ уже оплачен или тип оплаты не верен';
                }
            } else {
                $resArray['message'] = 'Номер заказа не найден';
            }
        } else {
            $resArray['code'] = 1;
        }

        $retMessage = '<checkOrderResponse ';
        foreach($resArray as $k => $v) {
            $retMessage .= $k.'="'.$v.'" ';
        }
        $retMessage .= '/>';
        return $retMessage;
    }
}