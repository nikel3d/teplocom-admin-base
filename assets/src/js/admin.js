$(document).ready(function() {
    $('input.tableData').each(function(idx, el) {
        $(el).after('<table class="tableDataContainer"></table>');
        var $container = $(el).next();

        var $headerRow = $('<tr class="info"></tr>');
        var $row = $('<tr>' + ('<td><input class="form-control" type="text" /></td>'.repeat($(el).attr('data-cols'))) + '<td class="delButton"><a class="btn btn-primary tableDataDel">Удалить</a></td></tr>');

        var templates = $(el).data('templates');
        var tempSelect = $('<select class="form-control"></select>');
        for (var i in templates['items']) {
            var option = $('<option>'+i+'</option>');
            option.data('template', templates['items'][i]);
            tempSelect.append(option);
        }
        for (var i = 0; i < templates['hidden'].length; i++) {
            $row.find('td').eq(templates['hidden'][i]).addClass('hidden');
        }
        for (var i in templates['colNames']) {
            $headerRow.append('<td>'+templates['colNames'][i]+'</td>');
        }
        tempSelect.change(selectTableDataTemplateEvent);

        if (tempSelect.children().length > 0) {
            $row.prepend('<td></td>');
            $row.find('td').eq(0).append(tempSelect);
        }

        $container.append($headerRow);
        $container.append($row.clone(true).addClass('last'));

        $container.find('input').blur(blurTableDataInput);
        $container.find('a.tableDataDel').click(delTableDataInput);
        $container.after('<div><a class="btn btn-primary tableDataAdd">Добавить</a></div>');

        $container.next().find('a').click(addTableDataInputEvent);

        var val = $(el).val();
        if (val != '') {
            var rows = val.split('[;;]');
            for (var i = 0; i < rows.length; i++) {
                addTableDataInput($container, rows[i].split('[::]'));
            }
        }
    });

    $('#multipleForm .copy input, #multipleForm .copy select, #multipleForm .copy textarea').each(function(idx, el) {
        $(el).addClass('setDisabled').attr('disabled', true);
    });

    $('#multipleForm [name=add]').click(function() {
        $clone = $('#multipleForm .multipletable .copy').clone();
        $clone.removeClass('copy').insertBefore($('#multipleForm .multipletable .copy')).find('.setDisabled').removeClass('setDisabled').attr('disabled', false);
        var num = $('#multipleForm tr').length;

        $clone.find('label').each(function(idx, el) {
            $attr = $(el).attr('for');
            
            $(el).attr('for', $attr + num);
            $clone.find('#'+$attr).attr('id', $attr + num);
        });

        setMultipleformIndexes();
    });
    
    $('[type=checkbox].all_check').change(function () {
        $($(this).attr('data-aim')).prop('checked', this.checked);
    });

    setMultipleformIndexes();
});

function setMultipleformIndexes() {
    $('#multipleForm .multipletable tr').not('.copy').each(function (idx, el) {
        $(el).find('input').each(function(idx1, inp) {
            var newName = inp.name.replace(/\[[0-9]*\]/g, '['+idx+']');
            inp.name = newName;
        });
    });
}

function recalcTableData($container) {
    var dataLines = new Array();

    $container.find('tr').not('.last').not('.info').each(function(idx, el) {
        var dataLine = new Array();

        $(el).find('input').each(function(idx, el) {
            dataLine.push(el.value);
        });

        dataLines.push(dataLine.join('[::]'));
    });

    $container.prev().val(dataLines.join('[;;]'));
}

function blurTableDataInput() {
    var $container = $(this).parents('table');
    recalcTableData($container);
}

function addTableDataInputEvent() {
    var $container = $(this).parent().prev();
    addTableDataInput($container);
}

function addTableDataInput($container, arr) {
    var $lastRow = $container.find('tr.last');

    var $newRow = $lastRow.clone(true).removeClass('last');
    if (typeof arr != 'undefined') {
        for (var i = 0; i < arr.length; i++) {
            $newRow.find('input').eq(i).val(arr[i]);
        }
        if ($newRow.find('select').length > 0) {
            for(var i = $newRow.find('select option').length - 1; i >= 0; i--) {
                var template = $newRow.find('select option').eq(i).data('template');
                var found = true;
                for (var j = 0; j < template.length; j++) {
                    if (template[j] != $newRow.find('input').eq(j).val()) {
                        found = false;
                    }
                }
                if (found) {
                    $newRow.find('select option').eq(i).prop('selected', true);
                    i = -1;
                }
            }
            selectTableDataTemplate($newRow.find('select'));
        }
    } else {
        if ($newRow.find('select').length > 0) {
            $newRow.find('select option').eq(0).prop('selected', true);
            selectTableDataTemplate($newRow.find('select'));
        }
    }

    $lastRow.before($newRow);

    if (typeof arr == 'undefined') {
        recalcTableData($container);
    }
}

function delTableDataInput() {
    var $container = $(this).parents('table');
    var $row = $(this).parent().parent();
    $row.remove();

    recalcTableData($container);
}

function selectTableDataTemplateEvent() {
    selectTableDataTemplate(this);
}

function selectTableDataTemplate(obj) {
    var $container = $(obj).parents('table');
    var template = $(obj).children(":selected").data('template');
    var $row = $(obj).parents('tr');

    $row.find('input').removeProp('readonly');
    for (var i = 0; i < template.length; i++) {
        if (template[i] != '' || $row.find('input').eq(i).parent().hasClass('hidden')) {
            $row.find('input').eq(i).val(template[i]).prop('readonly', true);
        }
    }

    recalcTableData($container);
}