<?php

namespace rusbitles\adminbase\assets;

use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $sourcePath = '@rusbitles/adminbase/assets/src';
    public $css = [
        'css/admin.css?v=2',
    ];
    public $js = [
        'js/admin.js?v=2',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\jui\JuiAsset',
        'yii\jui\DatePickerLanguageAsset',
        'yiister\gentelella\assets\Asset',
    ];
}