<?php

namespace rusbitles\adminbase\commands;

use yii\console\Controller;

class InitController extends Controller
{
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }
}
