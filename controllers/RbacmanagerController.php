<?php
namespace rusbitles\adminbase\controllers;

use rusbitles\adminbase\controllers\base\AdminController;
use rusbitles\adminbase\controllers\base\CommonAdminController;
use rusbitles\adminbase\models\forms\RbacGroup;
use rusbitles\adminbase\models\forms\RbacRights;
use rusbitles\adminbase\models\User;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class RbacmanagerController extends CommonAdminController
{
    public static $modelTitles = ['Группа', 'Группы', 'Групп'];

    public function getModelTitles() {
        return static::$modelTitles;
    }

    public function actionIndex() {
        $auth = Yii::$app->authManager;

        $resPermissions = [];
        $permissions = $auth->getRoles();
        foreach($permissions as $permission) {
            if((isset($permission->data['type']) && $permission->data['type'] != 'system' || !isset($permission->data['type']))) {
                $resPermissions[] = $permission;
            }
        }

        return $this->render('index', ['permissions' => $resPermissions]);
    }

    public function actionCreate($name = false) {
        if ($name === false) {
            $model = new RbacGroup();
        } else {
            $model = RbacGroup::getModel($name);
        }

        if (!$model) throw new NotFoundHttpException();

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (isset($_POST['cancel'])) return [];

            $model->load(Yii::$app->request->post());
            return ActiveForm::validate($model);
        }

        if (isset($_POST['cancel'])) {
            $this->redirect(Yii::$app->urlManager->createUrl([$this->admin->id . '/rbacmanager/index']));

            \Yii::$app->end();
        }

        if (isset($_POST['save']) || isset($_POST['apply'])) {
            $model->roles = [];
            $model->permissions = [];
            $model->load(\Yii::$app->request->post());

            if ($model->save()) {
                if (isset($_POST['save'])) {
                    return $this->redirect(Yii::$app->urlManager->createUrl([$this->admin->id . '/rbacmanager/index']));
                } else {
                    return $this->redirect(Yii::$app->urlManager->createUrl([$this->admin->id . '/rbacmanager/create', 'name' => $model->name]));
                }
            }
        }

        return $this->render('@rusbitles/adminbase/views/base/create', ['models' => [$model], 'views' => ["@rusbitles/adminbase/views/rbacmanager/_form"]]);
    }
    
    public function actionRights($uid) {
        $user = User::findOne($uid);
        if (!$user) throw new NotFoundHttpException();
        
        $model = RbacRights::getModel($uid);

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (isset($_POST['cancel'])) return [];

            $model->load(Yii::$app->request->post());
            return ActiveForm::validate($model);
        }

        if (isset($_POST['cancel'])) {
            $this->redirect(Yii::$app->urlManager->createUrl([$this->admin->id . '/usermanager/update', 'id' => $uid]));

            \Yii::$app->end();
        }

        if (isset($_POST['save']) || isset($_POST['apply'])) {
            $model->roles = [];
            $model->permissions = [];
            $model->load(\Yii::$app->request->post());

            if ($model->save($uid)) {
                if (isset($_POST['save'])) {
                    return $this->redirect(Yii::$app->urlManager->createUrl([$this->admin->id . '/usermanager/update', 'id' => $uid]));
                } else {
                    return $this->redirect(Yii::$app->urlManager->createUrl([$this->admin->id . '/rbacmanager/rights', 'uid' => $uid]));
                }
            }
        }

        return $this->render('@rusbitles/adminbase/views/base/create', ['models' => [$model], 'views' => ["@rusbitles/adminbase/views/rbacmanager/_formrights"]]);
    }

    public function actionDelete($name) {
        RbacGroup::deleteModel($name);
        
        return $this->redirect(Yii::$app->urlManager->createUrl([$this->admin->id . '/rbacmanager/index']));
    }
}