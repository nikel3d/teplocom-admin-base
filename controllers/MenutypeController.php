<?php
namespace rusbitles\adminbase\controllers;

class MenutypeController extends \rusbitles\adminbase\controllers\base\AdminController
{
    public static $mainmenuHandle = 'menutype';
    public static $modelName='MenuType';
    public static $fullModelName='rusbitles\\adminbase\\models\\MenuType';
    public $baseModel=true;
    public static $modelTitles = ['Тип меню', 'Тип меню', 'Тип меню'];
    public static $images = [
    ];
    public static $children = [
    ];
    public $parents = [
    ];
    public static $links = [
    ];
}