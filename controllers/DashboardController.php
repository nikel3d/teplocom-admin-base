<?php

namespace rusbitles\adminbase\controllers;

use rusbitles\adminbase\controllers\base\CommonAdminController;
use rusbitles\adminbase\models\SystemGii;

class DashboardController extends CommonAdminController {
    public function actionGii() {
        $createdTables = SystemGii::find()->orderBy('time DESC')->all();
        return $this->render('gii', ['createdTables' => $createdTables]);
    }
}