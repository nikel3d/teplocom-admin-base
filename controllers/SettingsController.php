<?php

namespace rusbitles\adminbase\controllers;

use rusbitles\adminbase\controllers\base\AdminController;

class SettingsController extends AdminController
{
    public static $mainmenuHandle = 'settings';
    public static $modelName='Settings';
    public static $fullModelName='rusbitles\\adminbase\\models\\Settings';
    public $baseModel=true;
    public static $modelTitles = ['Настройки', 'Параметр'];
    public static $images = [
        'value_file' => '@webroot/up/settings',
    ];

    public function getButtons()
    {
        return [];
    }

    public function beforeAction($action)
    {
        $res = parent::beforeAction($action);

        $current = \Yii::$app->request->getQueryParam('current');
        if (isset($current['module'])) {
            $this->admin->setMenuHandleActive($current['module'].'_settings');
        }

        return $res;
    }
}