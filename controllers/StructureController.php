<?php
namespace rusbitles\adminbase\controllers;

use rusbitles\adminbase\Admin;
use rusbitles\adminbase\models\Structure;

class StructureController extends \rusbitles\adminbase\controllers\base\AdminController
{
    public static $mainmenuHandle = 'structure';
    public static $modelName='Structure';
    public static $fullModelName='rusbitles\\adminbase\\models\\Structure';
    public $baseModel=true;
    public static $modelTitles = ['Структура', 'Структура', 'Структура'];
    public static $images = [
        'og_image' => '@webroot/up/structure',
    ];
    public static $children = [
    ];
    public $parents = [
    ];

    public static $links = [
    ];

    public function getButtons()
    {
        $buttons = parent::getButtons();

        $allControllers = Admin::$allControllers;
        $structures = Structure::find()->all();
        foreach($structures as $structure) {
            unset($allControllers[$structure->controller]);
        }

        if (count($allControllers) == 0) unset($buttons['add']);

        return $buttons;
    }
}