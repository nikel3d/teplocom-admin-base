<?php
namespace rusbitles\adminbase\controllers;

use rusbitles\adminbase\controllers\base\CommonAdminController;
use Yii;

class SystemController extends CommonAdminController
{
    public $doHistory = false;
    public $layout = 'gentelella_form';
    public $requireAuth = false;

    public function actionError()
    {
        $exception = Yii::$app->errorHandler->exception;

        $this->title = 'Ошибка ' . $exception->statusCode;
        $this->h1title = 'Ошибка ' . $exception->statusCode;

        return $this->render('error', ['exception' => $exception]);
    }
}
