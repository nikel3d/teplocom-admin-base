<?php

namespace rusbitles\adminbase\controllers\base;

use rusbitles\adminbase\Admin;
use rusbitles\adminbase\controllers\SitemapController;
use rusbitles\adminbase\helpers\SystemTools;
use rusbitles\adminbase\models\Menu;
use rusbitles\adminbase\models\MenuType;
use rusbitles\adminbase\models\Settings;
use rusbitles\adminbase\models\Structure;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;

class PublicController extends CommonController {
    public $isMobile = false;
    public $_breadcrumbs = [];
    public $seo = [
        'title' => '',
        'description' => '',
        'keywords' => '',
    ];
    public $og = [
        'title' => '',
        'description' => '',
        'image' => '',
    ];
    public $h1title = '';
    public $structure = false;
    public $layout = 'main';
    public $requireAuth = false;
    public $user = false;

    public function init()
    {
        parent::init();

        $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], 'iPhone');
        $Android = stripos($_SERVER['HTTP_USER_AGENT'], 'Android');
        $Palmpre = stripos($_SERVER['HTTP_USER_AGENT'], 'webOS');
        $BlackBerry = stripos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry');
        $iPod = stripos($_SERVER['HTTP_USER_AGENT'], 'iPod');
        
        if ($iPhone || $Palmpre || $Android || $BlackBerry || $iPod)
            $this->isMobile = true;
        
        if (!Yii::$app->user->isGuest) $this->user = Yii::$app->user->identity;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            if ($this->requireAuth && !Yii::$app->user->isGuest || !$this->requireAuth) {
                                return true;
                            }

                            return false;
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $controller SitemapController
     */
    public static function sitemap($controller) {
    }

    public function addBreadCrumb($url, $label) {
        $bread = [];
        if ($url != '') {
            $bread['url'] = $url;
        }
        if ($label != '') $bread['label'] = $label;
        $bread['last'] = false;

        $this->_breadcrumbs[] = $bread;
    }

    public function addBreadCrumbTree($treeItem) {
        $item = $treeItem;
        $bc = [];
        
        while($item->depth > 0) {
            $bc[] = [
                'url' => $item->url,
                'label' => $item->label,
            ];
            
            $item = $item->parents(1)->one();
        }

        foreach(array_reverse($bc) as $item) {
            $this->addBreadCrumb($item['url'], $item['label']);
        }
    }

    public function getBreadcrumbs() {
        $breadcrumbs = $this->_breadcrumbs;
        $breadcrumbs[count($breadcrumbs) - 1]['last'] = true;
        
        return $breadcrumbs;
    }
    
    public static function getRules($type = false) {
        return [];
    }
    
    public static function getTitle() {
        return static::$controllerTitle;
    }
    
    public static $controllerTitle = false;

    public function beforeAction($action)
    {


        if(empty(Yii::$app->params['menuG'])) {

            if ( (Yii::$app->params['menuG'] = Yii::$app->cache->get('menuG')) === false AND  (Yii::$app->params['allMenu'] = Yii::$app->cache->get('allMenu')) === false) {


                //$category = Category::find()->select(['id', 'title', 'lft', 'rgt', 'depth', 'handle', 'real_handle', 'h1title'])->published()->orderBy(['lft' => SORT_ASC])->asArray()->all();

                Yii::$app->params['allCat'] = (new \yii\db\Query())
                    ->select(['id', 'title', 'lft', 'rgt', 'depth', 'handle', 'real_handle', 'tree_handle'])
                    ->from('category')
                    ->where(['>=', 'public', 1 ])
                    ->orderBy(['lft' => SORT_ASC])
                    ->indexBy('id')
                    ->all();

                Yii::$app->params['allMenu'] = (new \yii\db\Query())
                    ->select('*')
                    ->from('system_menu')
                    ->leftJoin('system_menu2type', 'system_menu2type.menu_id = system_menu.id')
                    ->orderBy(['system_menu2type.type_id' => SORT_ASC, 'system_menu.lft' => SORT_ASC])
                    ->all();

                $uri_parts = explode('?', $_SERVER['REQUEST_URI']);
                $urls = explode('/', $uri_parts[0]);
                foreach($urls AS $k => $v){
                    $bread['/'.$v] = ($k+1);
                }
                //$bc = $this->getBreadcrumbs();
                //echo '<pre>';print_r($bc);echo '</pre>';
                //$this->addBreadCrumb('/', 'ГАГАга');
                //echo '<pre>';print_r($urls);echo '</pre>';
               // echo '<pre>';print_r($bread);echo '</pre>';
               // echo '<pre>';print_r(Yii::$app->params['allMenu']);echo '</pre>';

                $a = [];
                $depth = [];
                foreach (Yii::$app->params['allMenu'] AS $k => $v) {

                    //if($k == 6) $k = 'q6';



                    if (empty($v['type_id'])) $v['type_id'] = 0;

                    $depth[$v['type_id']][($v['depth'] + 1)] = $k;

                    // addBreadCrumb
                    if(!empty($bread[$v["url"]]) AND $v['public'] == 1 AND $v['in_breadcrumbs'] == 1 AND $v['type_id'] == 2 ){
                        $this->addBreadCrumb($v["url"], $v['title']);
                        //echo '<pre>';print_r($v);echo '</pre>';
                    }

                    if (empty($v['in_menu'])) continue;


                    $a[$v['type_id']][$k]['label'] = $v['title'];
                    $a[$v['type_id']][$k]['url'] = $v['url'];
                    $a[$v['type_id']][$k]['active'] = '';
                    $a[$v['type_id']][$k]['target'] = '0';
                    $a[$v['type_id']][$k]['class'] = $v['class'];
                    $a[$v['type_id']][$k]['virtual'] = $v['virtual'];
                    $a[$v['type_id']][$k]['children'] = [];
                    $a[$v['type_id']][$k]['parents'] = [];

                    $kk = $depth[$v['type_id']][$v['depth']];

                    if (isset($kk) AND !empty($a[$v['type_id']][$kk])) {
                        $a[$v['type_id']][$depth[$v['type_id']][$v['depth']]]['children'][] = $k;
                        $a[$v['type_id']][$k]['parents'][] = $kk;
                    }

                    $v['id'] = $k;
                }



                //cat
                //Yii::$app->params['allCat'][1]
                //$a = [];
                $depth = [];
                $depth[2][3] = '6';
                unset(Yii::$app->params['allCat'][1]);

                //echo '<pre>';print_r(Yii::$app->params['allCat']);echo '</pre>';
                foreach(Yii::$app->params['allCat'] AS $k => $v) {

                    $k = 'c_'.$k;
                    $v['type_id'] = 2;
                    $v['depth'] = $v['depth'] +2;

                    $depth[$v['type_id']][($v['depth'] + 1)] = $k;


                    if($v['depth'] != 3 AND $count = Yii::$app->cache->get('count_'.$v['handle'])){
                        $count = ' <span>'.$count.'</span>';
                    } else {
                        $count = '';
                    }

                    $a[$v['type_id']][$k]['label'] = $v['title'].$count;
                    $a[$v['type_id']][$k]['url'] = '/catalog/'.$v['handle'];
                    $a[$v['type_id']][$k]['active'] = '';
                    $a[$v['type_id']][$k]['target'] = '';
                    $a[$v['type_id']][$k]['class'] = '';
                    $a[$v['type_id']][$k]['virtual'] = '';
                    $a[$v['type_id']][$k]['children'] = [];
                    $a[$v['type_id']][$k]['parents'] = [];

                    $kk = $depth[$v['type_id']][$v['depth']];

                    if (isset($kk) AND !empty($a[$v['type_id']][$kk])) {
                        $a[$v['type_id']][$kk]['children'][] = $k;
                        $a[$v['type_id']][$k]['parents'][] = $kk;
                    }

                    $v['id'] = $k;
                }

                // cat





                Yii::$app->params['menuG'] = $a;

                Yii::$app->cache->set('menuG', Yii::$app->params['menuG'], 1);
                Yii::$app->cache->set('allMenu', Yii::$app->params['allMenu'], 1);


            }

        }

        /*$menuType = MenuType::find()->ordered()->one();
        $url = SystemTools::explodeURL(Yii::$app->request->url);

        if ($menuType) {
            while (!($menu = Menu::find()->joinWith('types type')->where(['url' => SystemTools::implodeURL($url), 'type.code' => $menuType->code])->one()) && SystemTools::implodeURL($url) != '') {
                if (count($url) > 1) {
                    unset($url[1][count($url[1])-1]);
                    if (count($url[1]) == 0) {
                        unset($url[1]);
                    }
                } else {
                    unset($url[0][count($url[0])-1]);
                }
            }

            if ($menu) {
                $bc = $menu->getBreadcrumbs($menuType);
                foreach($bc as $item) {
                    $this->addBreadCrumb($item['url'], $item['label']);
                }
                $this->admin->setPublicMenuActive($menu);
            }
        }*/



        $url = SystemTools::explodeURL(Yii::$app->request->url);
/*
        while (!($menu = Menu::find()->where(['url' => SystemTools::implodeURL($url), 'in_breadcrumbs' => 1])->one()) && SystemTools::implodeURL($url) != '') {
            if (count($url) > 1) {
                unset($url[1][count($url[1])-1]);
                if (count($url[1]) == 0) {
                    unset($url[1]);
                }
            } else {
                unset($url[0][count($url[0])-1]);
            }
        }

        if ($menu) {
            $bc = $menu->getBreadcrumbs();
            foreach($bc as $item) {
                $this->addBreadCrumb($item['url'], $item['label']);
            }
            //$this->admin->setPublicMenuActive($menu);
        }*/







        $controllerName = explode('/', $this->id);
        $controllerName[count($controllerName) - 1] = ucfirst($controllerName[count($controllerName) - 1]);
        $controllerName = implode('\\', $controllerName);
        if (is_array(static::getTitle())) {
            $type = Yii::$app->request->getQueryParam('type');
            $controllerName = $controllerName.'_'.$type;
        }
        
        $structure = Structure::findOne(['controller' => $controllerName]);
        if ($structure) {
            $this->structure = $structure;
            $this->h1title = ($structure->h1_title=='')?Admin::$allControllers[$controllerName]:$structure->h1_title;

            $this->seo['title'] = ($structure->seo_title=='')?Admin::$allControllers[$controllerName]:$structure->seo_title;
            $this->seo['description'] = $structure->seo_description;
            $this->seo['keywords'] = $structure->seo_keywords;

            $this->og['title'] = ($structure->og_title=='')?$this->seo['title']:$structure->og_title;
            $this->og['description'] = ($structure->og_description=='')?$structure->seo_description:$structure->og_description;
            $this->og['image'] = $structure->og_image;
        }

        return parent::beforeAction($action);
    }
    
    public function addSeoItem($item, $title, $bcTitle = false) {
        $this->h1title = $title;

        $newTitle = ($item->seo_title=='')?$title:$item->seo_title;
        $this->seo['title'] = ($newTitle=='')?$this->seo['title']:$newTitle;
        $this->seo['description'] = ($item->seo_description=='')?$this->seo['description']:$item->seo_description;
        $this->seo['keywords'] = ($item->seo_keywords=='')?$this->seo['keywords']:$item->seo_keywords;

        if ($this->og['title'] == '') $this->og['title'] = $title;
        
        if ($bcTitle) $this->addBreadCrumb('', $bcTitle);
    }

    public function head($view, $layout = '', $module = 'site') {
		$uri_parts = explode('?', $_SERVER['REQUEST_URI']);
		$this->seo['description'] = str_replace(' в в ', ' в ', $this->seo['description']);
        echo '
            <meta name="description" content="'.Html::encode($this->seo['description']).'" />
            <meta name="keywords" content="'.Html::encode($this->seo['keywords']).'" />
            <meta property="og:title" content="'.Html::encode($this->seo['title']).'" />
            <meta property="og:description" content="'.Html::encode($this->seo['description']).'" />
            <meta property="og:image" content="'.Yii::$app->params['http'].Yii::$app->params['domain'].Html::encode($this->og['image']).'" />
            <meta property="og:url" content="'.Yii::$app->params['http'].Yii::$app->params['domain'].Yii::$app->request->url.'" />
            <meta property="og:type" content="website" />
            
            <base href="'.((empty($_SERVER['HTTPS'])) ? "http" : "https").'://'.str_replace( array(":80", ":443"), '',$_SERVER["HTTP_HOST"]).$uri_parts[0].'" />
            
            <link rel="canonical" href="'.((empty($_SERVER['HTTPS'])) ? "http" : "https").'://'.str_replace( array(":80", ":443"), '',$_SERVER["HTTP_HOST"]).$uri_parts[0].'" />
            
            <link rel="icon" href="'.Settings::getParam($module, 'favicon'.$layout).'" />
        ';
        if (Settings::getParam($module, 'apple-favicon'.$layout) != '') {
            echo '
                <link rel="apple-touch-icon" href="'.Settings::getParam($module, 'apple-favicon'.$layout).'">
            ';
        }
		
        echo Html::csrfMetaTags();
        echo Settings::getParam($module, 'headEnd'.$layout);

        $view->head();
    }
    
    public function beginBody($view, $layout = '', $module = 'site') {
        $view->beginBody();
        echo Settings::getParam($module, 'bodyBegin'.$layout);
    }

    public function endBody($view, $layout = '', $module = 'site') {
        echo Settings::getParam($module, 'bodyEnd'.$layout);
        $view->endBody();
    }
}