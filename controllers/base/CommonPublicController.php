<?php

namespace rusbitles\adminbase\controllers\base;

class CommonPublicController extends CommonController {
    public $isMobile = false;
    public $breadcrumbs = [];
    public $activeBreadcrumbs = [];
    public $seo = [
        'title' => '',
        'description' => '',
        'keywords' => '',
    ];
    public $og = [
        'title' => '',
        'description' => '',
        'image' => '',
    ];
    public $h1title = '';
    
    public function init()
    {
        parent::init();

        $iPhone = stripos($_SERVER['HTTP_USER_AGENT'], 'iPhone');
        $Android = stripos($_SERVER['HTTP_USER_AGENT'], 'Android');
        $Palmpre = stripos($_SERVER['HTTP_USER_AGENT'], 'webOS');
        $BlackBerry = stripos($_SERVER['HTTP_USER_AGENT'], 'BlackBerry');
        $iPod = stripos($_SERVER['HTTP_USER_AGENT'], 'iPod');
        
        if ($iPhone || $Palmpre || $Android || $BlackBerry || $iPod)
            $this->isMobile = true;

        $this->layout = 'main';
        $this->breadcrumbs[] = [
            'url' => '/',
            'label' => 'Главная',
        ];
        $this->activeBreadcrumbs['/'] = 1;
    }

    public function tempInit($pt, $handle, $last_section = false) {
        
    }

    public function addBreadCrumb($url, $label) {
        $bread = [];
        if ($url != '') {
            $bread['url'] = $url;
            $this->activeBreadcrumbs[$url] = count($this->activeBreadcrumbs)+1;
        }
        if ($label != '') $bread['label'] = $label;

        $this->breadcrumbs[] = $bread;
    }
    
    public static function getRules() {
        return [];
    }
}