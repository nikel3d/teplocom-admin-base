<?php

namespace rusbitles\adminbase\controllers\base;

use app\models\ProjectImages;
use app\models\Projects;
use rusbitles\adminbase\helpers\SystemTools;
use rusbitles\adminbase\models\Log;
use rusbitles\adminbase\models\Settings;
use rusbitles\adminbase\models\SystemSort;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class AdminController extends CommonAdminController
{
    public static $modelName='CActiveRecord';
    public static $fullModelName='CActiveRecord';
    public $pageSize = 20;
    public static $images = [];
    /*public $images = [
        'image' => '@webroot/up/sections'
    ];*/

    public static $children = [];
    /*public $children = [
        'blocks' => ['Блоки', 'Добавить блок'],
    ];*/

    public $additional = [];
    /*public $children = [
        'blocks' => ['Блоки', 'Добавить блок'],
    ];*/

    public $parents = [];
    /*public $parents = [
        'section' => 'Назад к разделу',
    ];*/

    public static $links = [];
    public $baseModel = false;

    public function getModelTitles() {
        return static::$modelTitles;
    }

    public function setModelTitles($value) {
        static::$modelTitles = $value;
    }

    public function searchFields() {
        return [
            'public' => ['type' => 'public'],
            'weight' => ['type' => 'integer'],
            'handle' => ['type' => 'string'],
            'real_handle' => ['type' => 'string'],
            'created_at' => ['type' => 'date'],
            'updated_at' => ['type' => 'date'],
        ];
    }

    public static function getMenu() {
        $handle = static::$mainmenuHandle;
        $modelTitles = static::$modelTitles;

        $result = [
            $handle => [
                'parent' => false,
                'title' => $modelTitles[1],
                'url' => '#',
            ],
            $handle . '_main' => [
                'parent' => $handle,
                'title' => 'Список ' . $modelTitles[2],
                'url' => '/'.$handle,
                'icon' => '',
                'weight' => 10,
            ],
        ];

        if (Settings::find()->where(['module' => $handle])->all()) {
            $result[$handle . '_settings'] = [
                'parent' => $handle,
                'title' => 'Настройки',
                'url' => '/settings?current[module]='.$handle,
                'icon' => 'gear',
                'weight' => 20,
            ];
        }

        return $result;
    }

    public function getButtons() {
        $add = static::getAddUrl();

        $moduleName = $this->baseModel?$this->admin->id:'admin';
        $controllerName = $this->baseModel?$this->admin->id.'/'.$this->id:$this->id;

        $buttons = [];

        if (!$this->model_id) {
            $buttons['manage'] = [
                'title' => 'Управление',
                'icon' => '',
                'url' => Yii::$app->urlManager->createUrl([$moduleName . '/' . $this->mainmenuHandle . '/index']) . $add,
            ];
        }

        $params = [];
        if ($this->model_id) $params['parent'] = $this->model_id;
        if ($this->additional_id) $params['parent'] = $this->additional_id;
        if (count($this->additional) == 0) {
            $buttons['add'] = [
                'title' => 'Добавить',
                'icon' => 'plus',
                'url' => Yii::$app->urlManager->createUrl(array_merge([$moduleName . '/' . $this->mainmenuHandle . '/create'], $params)) . $add,
            ];

            if (file_exists(Yii::getAlias('@app/views/'.$controllerName.'/_formmultiple.php'))) {
                $buttons['addmultiple'] = [
                    'title' => 'Массовое добавление',
                    'icon' => 'plus',
                    'url' => Yii::$app->urlManager->createUrl(array_merge([$moduleName . '/' . $this->mainmenuHandle . '/createmultiple'], $params)) . $add,
                ];
            }
        } else {
            foreach($this->additional as $k => $additional) {
                $buttons['add'.$k] = [
                    'title' => $additional[2],
                    'icon' => 'plus',
                    'url' => Yii::$app->urlManager->createUrl(array_merge([$moduleName . '/' . $this->mainmenuHandle . '/create', 'additional' => $k], $params)) . $add,
                ];
            }
        }

        return $buttons;
    }

    public static function getAddUrl() {
        $current = Yii::$app->request->get('current');
        $add = [];

        if (!empty($current)) {
            foreach ($current as $k => $v) {
                $add[] = 'current['.$k.']='.$v;
            }
        }

        if (!empty($add)) {
            $add = '?'.implode('&', $add);
        } else {
            $add = '';
        }

        return $add;
    }

    public function actionIndex($parent = false, $partial = false, $sort = false, $ps = false)
    {
        if (!empty($_GET['ps'])) $ps = $_GET['ps'];
        if ($sort !== false || $ps !== false) {
            $sSort = SystemSort::findOne(['user_id' => Yii::$app->user->id, 'model' => strtolower(static::$modelName), 'partial' => $partial?1:0]);
            if (!$sSort) $sSort = new SystemSort();
            $sSort->user_id = Yii::$app->user->id;
            $sSort->model = strtolower(static::$modelName);
            if ($sort !== false) {
                $sSort->column = preg_replace('/^\-/', '', $sort);
                $sSort->sort = preg_match('/^\-/', $sort) ? 2 : 1;
            } elseif($sSort->isNewRecord) {
                $sSort->column = 'id';
                $sSort->sort = 1;
            }
            if ($ps !== false) {
                if ($ps == 'all') {
                    $sSort->pagesize = 0;
                } else {
                    $sSort->pagesize = $ps;
                }
            } elseif($sSort->isNewRecord) {
                $sSort->pagesize = 20;
            }
            $sSort->partial = $partial?1:0;
            $sSort->save();
        }

        $model = static::initModel();

        $dataProvider = $this->dataProviderModel(Yii::$app->request->get(), $parent, $partial);

        if (!$partial) {
            return $this->render('index', ['model' => $model, 'dataProvider' => $dataProvider[0], 'item' => $dataProvider[1]]);
        } else {
            return preg_replace('/<h1>.*?<\/h1>/', '', $this->renderPartial('index', ['model' => $model, 'dataProvider' => $dataProvider[0], 'item' => $dataProvider[1]]));
        }
    }

    public function ajaxCheck($models) {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (isset($_POST['cancel'])) return [];

            $ret = [];
            foreach ($models as $v) {
                $v->load(Yii::$app->request->post());
                $ret = array_merge($ret, ActiveForm::validate($v));
            }

            return $ret;
        }
        return false;
    }

    public function loadPost($models, $controllers) {
        if (isset($_POST['save']) || isset($_POST['apply'])) {
            foreach($models as $modelKey => $modelVal) {
                $controller = $controllers[$modelKey];
                $modelVal->load(\Yii::$app->request->post());

                if($modelKey == 0) {
                    $current = Yii::$app->request->get('current');
                    if (!empty($current)) {
                        foreach ($current as $k => $v) {
                            $modelVal->{$k} = $v;
                        }
                    }
                }

                foreach ($controller::$images as $imageVar => $imageWay) {
                    if (!empty($_POST[$imageVar . '_deleting'])) {
                        $modelVal->$imageVar = '';
                    } else {
                        SystemTools::loadImage($modelVal, $imageVar, $imageWay);
                    }
                }

                if (!$modelVal->validate()) return false;
            }
            $isNewRecord = $models[0]->isNewRecord;
            foreach($models as $modelKey => $modelVal) {
                if ($modelVal->isNewRecord && $modelKey != 0) {
                    $modelVal->parent_id = $models[0]->id;
                }

                $controller = $controllers[$modelKey];
                $controller::saveModel($modelVal);

                foreach ($controller::$links as $link => $linkVars) {
                    if (isset($_POST[$controller::$modelName][$link])) {
                        $query = new Query;
                        $command = $query->createCommand();
                        $command->sql = 'DELETE FROM `' . $linkVars[0] . '` WHERE `' . $linkVars[1] . '`="' . $modelVal->id . '";';
                        $command->execute();

                        $values = [];
                        if (is_array($_POST[$controller::$modelName][$link])) {
                            foreach ($_POST[$controller::$modelName][$link] as $val) {
                                $values[] = '("' . $modelVal->id . '", "' . $val . '")';
                            }
                        }

                        if (count($values)) {
                            $command->sql = 'INSERT INTO `' . $linkVars[0] . '` (`' . $linkVars[1] . '`, `' . $linkVars[2] . '`) VALUES ' . implode(', ', $values);
                            $command->execute();
                        }
                    }
                }
            }
            if ($isNewRecord && count($models) > 1) {
                $models[0]->refresh();
                $models[0]->afterSave(true, []);
            }

            return true;
        }
        return false;
    }

    public function actionCreatemultiple($parent = false)
    {
        $controllerName = $this->baseModel?$this->admin->id.'/'.$this->id:$this->id;
        if (isset($_POST['cancel'])) {
            if ($parent !== false) {
                return $this->redirect(Yii::$app->urlManager->createUrl(['admin/'.current(array_keys($this->parents)).'/update', 'id' => $parent, 'tab' => $this->mainmenuHandle]));
            } else {
                $this->redirect(Yii::$app->urlManager->createUrl([$controllerName . '/index']));
            }
            \Yii::$app->end();
        }

        $model = static::initModel();

        if (isset($_POST['save'])) {
            $post = Yii::$app->request->post($model->formName());

            $className = static::$fullModelName;
            $savemodels = [];
            for ($i = 0; $i < count($post); $i++) {
                $savemodels[] = static::initModel();
            }

            $className::loadMultiple($savemodels, Yii::$app->request->post());
            $images = static::$images;
            foreach($savemodels as $k => $model) {
                foreach($post[$k] as $fieldName => $values) {
                    if (isset($images[$fieldName])) {
                        SystemTools::loadImage($savemodels[$k], '['.$k.']'.$fieldName, $images[$fieldName]);
                    }
                }
            }

            foreach ($savemodels as $savemodel) {
                if ($parent) {
                    $savemodel->parent_id = $parent;
                }
                $savemodel->save();
            }

            if ($parent !== false) {
                return $this->redirect(Yii::$app->urlManager->createUrl(['admin/' . current(array_keys($this->parents)) . '/update', 'id' => $parent, 'tab' => $this->mainmenuHandle]));
            } else {
                return $this->redirect(Yii::$app->urlManager->createUrl([$controllerName.'/index']));
            }
        }

        $view = '//' . $controllerName . '/_formmultiple';

        return $this->render('@rusbitles/adminbase/views/base/createmultiple', ['model' => $model, 'view' => $view]);
    }

    public function actionCreate($parent = false, $additional = false)
    {
        $controllerName = $this->baseModel?$this->admin->id.'/'.$this->id:$this->id;
        $controllers = [static::className()];
        $models = [static::initModel()];
        if ($this->baseModel) {
            $views = ['@rusbitles/adminbase/views/' . $this->id . '/_form'];
        } else {
            $views = ['//' . $controllerName . '/_form'];
        }

        if ($additional !== false) {
            $controller = $this->additional[$additional][0];
            $model = $this->additional[$additional][1];
            $models[] = $controller::initModel();
            $controllers[] = $controller;
            $views[] = '//admin/'.$controller::$mainmenuHandle.'/_form';
            $models[0]->system_additional = $additional;
        }

        if (($ajax = $this->ajaxCheck($models)) !== false) return $ajax;

        if (isset($_POST['cancel'])) {
            if ($parent !== false) {
                return $this->redirect(Yii::$app->urlManager->createUrl(['admin/'.current(array_keys($this->parents)).'/update', 'id' => $parent, 'tab' => $this->mainmenuHandle]));
            } else {
                $this->redirect(Yii::$app->urlManager->createUrl([$controllerName . '/index']));
            }
            \Yii::$app->end();
        }

        if ($parent !== false) {
            $models[0]->parent_id = $parent;
        }

        if ($this->loadPost($models, $controllers)) {
            if (isset($_POST['save'])) {
                if ($parent !== false) {
                    return $this->redirect(Yii::$app->urlManager->createUrl(['admin/' . current(array_keys($this->parents)) . '/update', 'id' => $parent, 'tab' => $this->mainmenuHandle]));
                } else {
                    return $this->redirect(Yii::$app->urlManager->createUrl([$controllerName.'/index']));
                }
            } else {
                return $this->redirect(Yii::$app->urlManager->createUrl([$controllerName . '/update', 'id' => $models[0]->id]));
            }
        }

        return $this->render('@rusbitles/adminbase/views/base/create', ['models' => $models, 'views' => $views]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id, $tab = '', $pback = false, $sort = false)
    {
        $controllerName = $this->baseModel?$this->admin->id.'/'.$this->id:$this->id;
        $controllers = [static::className()];
        $models = [static::loadModel($id)];
        if ($this->baseModel) {
            $views = ['@rusbitles/adminbase/views/' . $this->id . '/_form'];
        } else {
            $views = ['//'.$controllerName.'/_form'];
        }

        if ($models[0]->hasAttribute('system_additional') && !empty($this->additional[$models[0]->system_additional])) {
            $controller = $this->additional[$models[0]->system_additional][0];
            $model = $this->additional[$models[0]->system_additional][1];
            $models[] = $model::findOne(['parent_id' => $models[0]->id]);
            $controllers[] = $controller;
            $views[] = '//admin/'.$controller::$mainmenuHandle.'/_form';
        }

        if (($ajax = $this->ajaxCheck($models)) !== false) return $ajax;

        if (isset($_POST['cancel'])) {
            $params = [];
            if ($pback !== false)  $params['page'] = $pback;
            if (!empty($_GET['current'])) $params['current'] = $_GET['current'];
            if ($this->parents) {
                return $this->redirect(
                    Yii::$app->urlManager->createUrl(
                        ['admin/'.current(array_keys($this->parents)).'/update', 'id' => $models[0]->parent_id, 'tab' => strtolower(static::$modelName)] + $params
                    )
                );
            } else {
                return $this->redirect(Yii::$app->urlManager->createUrl([$controllerName . '/index'] + $params));
            }
            \Yii::$app->end();
        }

        if ($this->loadPost($models, $controllers)) {
            if (isset($_POST['save'])) {
                $params = [];
                if ($pback !== false)  $params['page'] = $pback;
                if (!empty($_GET['current'])) $params['current'] = $_GET['current'];
                if ($this->parents) {
                    return $this->redirect(['admin/'.current(array_keys($this->parents)).'/update', 'id' => $models[0]->parent_id, 'tab' => strtolower(static::$modelName)] + $params);
                } else {
                    return $this->redirect(['index'] + $params);
                }
            } else {
                $params = [];
                if ($pback !== false) $params['pback'] = $pback;
                if (!empty($_GET['current'])) $params['current'] = $_GET['current'];

                return $this->redirect(['update', 'id' => $models[0]->id] + $params);
            }
        }

        $params = $this->pback?['pback'=>$this->pback]:[];
        $children = [[
            'label' => $this->modelTitles[0],
            'active' => strtolower($tab)==static::$modelName||$tab=='',
            'link' => Yii::$app->urlManager->createUrl([Yii::$app->controller->id.'/update', 'id' => $id]+$params),
        ]];

        foreach(static::$children as $child => $childTitle) {
            $childElem = [
                'label' => $childTitle[0],
                'active' => strtolower($tab)==$child,
                'link' => Yii::$app->urlManager->createUrl([Yii::$app->controller->id.'/update', 'id' => $id, 'tab' => $child]+$params),
                'controller' => 'admin/'.$child.'',
                'controller_params' => ['parent' => $models[0]->id, 'partial' => true, 'sort' => $sort],
            ];
            $children[] = $childElem;
        }

        if ($models[0]->hasAttribute('system_additional') && !empty($this->additional[$models[0]->system_additional])) {
            $cn = $this->additional[$models[0]->system_additional][0];
            foreach($cn::$children as $child => $childTitle) {
                $childElem = [
                    'label' => $childTitle[0],
                    'active' => strtolower($tab)==$child,
                    'link' => Yii::$app->urlManager->createUrl([Yii::$app->controller->id.'/update', 'id' => $id, 'tab' => $child, 'additional' => $models[1]->id]+$params),
                    'controller' => 'admin/'.$child.'',
                    'controller_params' => ['parent' => $models[1]->id, 'partial' => true, 'sort' => $sort],
                ];
                $children[] = $childElem;
            }
        }

        return $this->render("@rusbitles/adminbase/views/base/update", ['models' => $models, 'views' => $views, 'tabs' => $children]);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id, $tab = '', $pback = false, $partial = false)
    {
        $parent = false;
        // we only allow deletion via POST request
        $model = static::loadModel($id);

        if (isset($model->parent_id)) {
            $parent = $model->parent_id;
        }

        foreach (static::$images as $img => $way) {
            if ($model->$img != '' && file_exists($_SERVER['DOCUMENT_ROOT'] . $model->$img) && !is_dir($_SERVER['DOCUMENT_ROOT'] . $model->$img)) {
                unlink($_SERVER['DOCUMENT_ROOT'] . $model->$img);
            }
        }

        if ($model->hasAttribute('additional') && !empty($this->additional[$model->additional])) {
            $amodel = $this->additional[$model->additional][1];
            $add = $amodel::findOne(['parent_id' => $model->id]);
            if ($add) $add->delete();
        }

        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $params = [];
            if ($pback !== false)  $params['page'] = $pback;
            if ($this->parents/* && $partial !== false*/) {
                return $this->redirect(['admin/'.current(array_keys($this->parents)).'/update', 'id' => $model->parent_id, 'tab' => strtolower(static::$modelName)] + $params);
            } else {
                return $this->redirect(['index'] + $params);
            }
        }
    }

    public function actionChangepublic($id, $tab = '', $pback = false, $partial = false)
    {
        $model = static::loadModel($id);

        if ($model->hasAttribute('public')) {
            if ($model->public != 1) {
                $model->public = 1;
            } else {
                $model->public = 0;
            }

            $model->save();
        }

        if (!isset($_GET['ajax'])) {
            $params = [];
            if ($pback !== false)  $params['page'] = $pback;
            if ($this->parents/* && $partial !== false*/) {
                return $this->redirect(['admin/'.current(array_keys($this->parents)).'/update', 'id' => $model->parent_id, 'tab' => strtolower(static::$modelName)] + $params);
            } else {
                return $this->redirect(['index'] + $params);
            }
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public static function loadModel($id)
    {
        if ($id == 0) return false;

        $className = static::$fullModelName;

        $model = $className::findOne($id);
        if ($model === null) {
            throw new NotFoundHttpException();
        }
        return $model;
    }

    public static function initModel()
    {
        $res = new static::$fullModelName();
        $res->loadDefaultValues();
        return $res;
    }

    public function dataProviderModel($params = false, $parent = false, $partial = false) {
        $className = static::$fullModelName;
        $model = static::initModel();
        $model->scenario = 'search';

        if($parent) {
            $query = $className::find()->where(['parent_id'=>$parent]);
        } else {
            $query = $className::find();
        }

        if (!empty($params['current'])) $query->andWhere($params['current']);

        $dpParams = [];
        $sSort = SystemSort::findOne(['user_id' => Yii::$app->user->id, 'model' => strtolower(static::$modelName), 'partial' => $partial?1:0]);
        if ($sSort) {
            $dpParams['sort'] = ['defaultOrder'=> [$sSort->column => ($sSort->sort==1?SORT_ASC:SORT_DESC)]];
            if ($sSort->pagesize == 0) {
                $this->pageSize = 100000000;
            } else {
                $this->pageSize = $sSort->pagesize;
            }
        }

        if ($model->load($params)) {
            //if ($model->validate()) {
                $this->searchModelChangeQuery($query, $model);
            //}
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
        ]+$dpParams);

        return [$dataProvider, $model];
    }

    /**
     * @param $query ActiveQuery
     * @param $model static
     */
    public function searchModelChangeQuery($query, $model) {
        $searchFields = $this->searchFields();

        foreach($searchFields as $field => $sf) {
            if (isset($model->$field) && trim($model->$field) != '') {
                if (isset($searchFields[$field])) {
                    $model->$field = trim($model->$field);
                    switch($searchFields[$field]['type']) {
                        case 'public':
                        case 'number':
                        case 'checkbox':
                            $query->andFilterWhere(['=', $field, $model->$field]);
                            break;
                        case 'numberModel':
                            $className = $searchFields[$field]['model'];
                            $fieldName = $searchFields[$field]['field'];
                            $item = $className::findOne([$fieldName => $model->$field]);
                            if($item) $query->andFilterWhere(['=', $field, $item->id]);
                            break;
                        case 'stringModel':
                            $className = $searchFields[$field]['model'];
                            $fieldName = $searchFields[$field]['field'];
                            $items = $className::find()->andFilterWhere(['like', $fieldName, preg_split('/\s+/', $model->$field)])->all();
                            $ids = [];
                            foreach ($items as $item) $ids[] = $item->id;
                            if (count($ids) > 0) {
                                $query->andFilterWhere(['in', $field, $ids]);
                            } else {
                                $query->andFilterWhere(['=', $field, '-100']);
                            }
                            break;
                        case 'date':
                            $items = explode('-', $model->$field);
                            foreach ($items as $k => $item) {
                                $items[$k] = strtotime(trim($item));
                            }
                            if (count($items) == 1) {
                                $items[1] = $items[0];
                            }
                            if(count($items) > 1) {
                                $query->andFilterWhere(['>=', $field, $items[0]]);
                                $query->andFilterWhere(['<', $field, $items[1] + 86400]);
                            }
                            break;
                        case 'manual':
                            $function = $searchFields[$field]['function'];
                            $query = $function($query, $model);
                            break;
                        case 'string':
                        default:
                            $query->andFilterWhere(['like', $field, preg_split('/\s+/', $model->$field)]);
                    }

                } else {
                    $query->andFilterWhere(['like', $field, $model->$field]);
                }
            }
        }
    }

    public function beforeAction($action) {
        $res = parent::beforeAction($action);

        if (!empty($this->parents)) {
            $this->admin->setMenuHandleActive(current(array_keys($this->parents)).'_main');
        }

        return $res;
    }

    public function beforeUpdate(&$model, &$tabs) {

    }

    public function beforeModelSave($model) {

    }

    public function afterModelSave($model) {

    }

    public function afterAdmin($model) {

    }

    public static function saveModel($model) {
        $model->save();
    }
}
