<?php

namespace rusbitles\adminbase\controllers\base;

use rusbitles\adminbase\models\Settings;
use Yii;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\db\Query;
use yii\web\NotAcceptableHttpException;

class CommonAdminController extends CommonController
{
    public $layout='@rusbitles/adminbase/views/layouts/gentelella';
    public $parent_id;
    public $model_id;
    public $additional_id;
    public $pback = false;
    public $partial = false;
    public static $mainmenuHandle = '';
    public static $modelTitles = ['Сущность', 'Сущностей']; // Добавить сущность, Список сущностей
    public $h1title = "Страница";
    public $title = "Страница";
    public $requireAuth = true;

    public function init()
    {
        parent::init();
        $this->h1title = static::$modelTitles[0];
        $this->title = static::$modelTitles[1];
    }

    public function getMainmenuHandle() {
        return static::$mainmenuHandle;
    }

    public function setMainmenuHandle($value) {
        static::$mainmenuHandle = $value;
    }

    public static function getMenu() {
        $result = [];
        $handle = static::$mainmenuHandle;
        $modelTitles = static::$modelTitles;

        if (Settings::find()->where(['module' => $handle])->all()) {
            $result[$handle] = [
                'parent' => false,
                'title' => $modelTitles[0],
                'url' => '/settings?current[module]='.$handle,
                'icon' => '',
                'weight' => 0,
            ];
        }

        return $result;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            if ($this->requireAuth) {
                                $auth = Yii::$app->authManager;
                                $controllerName = str_replace('Controller', '', end(explode('\\', get_class($action->controller))));
                                $actionName = str_replace('action', '', $action->actionMethod);

                                $actionList = [
                                    'Delete' => 'Update',
                                    'Changepublic' => 'Update',
                                ];
                                if (!empty($actionList[$actionName])) $actionName = $actionList[$actionName];
                                $permission = $auth->getPermission($controllerName . 'Admin' . $actionName);

                                if (Yii::$app->user->can('adminpage')) {
                                    if ($permission && !Yii::$app->user->can($permission->name)) return false;

                                    return true;
                                }
                            } else {
                                return true;
                            }
                        }
                    ],
                ],
            ],
        ];
    }

    public function actionIndex($parent = false)
    {
        $parserLogs = (new Query)->select('*')->from('{{%parser_log}}')->orderBy('id DESC')->limit(5)->all();

        Yii::$app->timeZone = 'Asia/Krasnoyarsk';

        return $this->render('index', [
          'parserLogs' => $parserLogs
        ]);
    }

    public function beforeAction($action)
    {
        Yii::$app->user->loginUrl = [$this->admin->id.'/user/login'];
        Yii::$app->errorHandler->errorAction = $this->admin->id.'/system/error';
        $this->enableCsrfValidation = false;
        $this->parent_id = \Yii::$app->request->getQueryParam('parent');
        $this->model_id = \Yii::$app->request->getQueryParam('id');
        $this->additional_id = \Yii::$app->request->getQueryParam('additional');
        $this->pback = \Yii::$app->request->getQueryParam('pback');
        $this->partial = \Yii::$app->request->getQueryParam('partial');

        $this->admin->setMenuHandleActive(static::$mainmenuHandle.'_main');

        return parent::beforeAction($action);
    }
}
