<?php

namespace rusbitles\adminbase\controllers\base;

use rusbitles\adminbase\Admin;
use yii\web\Controller;

/**
 * Class CommonController
 * @package rusbitles\adminbase\controllers\base
 * @property Admin $admin
 */
class CommonController extends Controller {
    public $lastPage = false;
    public $doHistory = true;
    public $admin = false;

    public function init()
    {
        parent::init();
        $this->admin = Admin::getInstance();

        $this->lastPage = \Yii::$app->session->get('history');
        if (!$this->lastPage) $this->lastPage = '/';
    }

    public function beforeAction($action)
    {
        if ($action->id != 'error' && $this->doHistory) {
            \Yii::$app->session->set('history', \Yii::$app->request->url);
        }

        return parent::beforeAction($action);
    }
}
