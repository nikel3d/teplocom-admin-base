<?php

namespace rusbitles\adminbase\controllers\base;

use rusbitles\adminbase\models\TreeForm;
use Yii;
use yii\base\Exception;

class TreeAdminController extends AdminController
{
    public function getButtons()
    {
        $add = static::getAddUrl();
        $parent = parent::getButtons();
        $moduleName = $this->baseModel?$this->admin->id:'admin';

        $parent['tree'] = [
            'title' => 'Дерево',
            'icon' => 'code-fork',
            'url' => Yii::$app->urlManager->createUrl([$moduleName . '/' . $this->mainmenuHandle . '/tree']) . $add,
        ];

        return $parent;
    }
    
    public function actionTree()
    {
        $model = $this->initModel();

        return $this->render('tree', ['model' => $model, 'tree' => $model->find()->orderBy('lft')->all()]);
    }

    public function getRoot()
    {
        $className = static::$fullModelName;
        return $className::find()->where(['depth'=>0])->one();
    }

    public function actionTreeform($id)
    {
        $model = static::loadModel($id);

        $treeForm = new TreeForm();
        $treeForm->load(\Yii::$app->request->post());
        if ($treeForm->validate()) {
            $parent = static::loadModel($treeForm->target_id);

            try {
                switch ($treeForm->operation) {
                    case 1:
                        $model->insertBefore($parent);
                        break;
                    case 2:
                        $model->insertAfter($parent);
                        break;
                    case 3:
                        $model->prependTo($parent);
                        break;
                    case 4:
                        $model->appendTo($parent);
                        break;
                }
            } catch (Exception $e) {
                
            }

            if (isset($_POST['save'])) {
                return $this->redirect(['index']);
            } else {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            return $this->redirect(['update', 'id' => $model->id]);
        }
    }

    public static function saveModel($model) {
        if ($model->isNewRecord) {
            if (!($root = static::loadModel($model->tree_parent_id))) $root = static::getRoot();
            if (!$root) {
                $model->makeRoot();
            } else {
                $model->appendTo($root);
            }
        } else {
            $model->save();
        }
    }
}