<?php
namespace rusbitles\adminbase\controllers;

use rusbitles\adminbase\controllers\base\CommonAdminController;
use Yii;
use yii\web\Response;

class AjaxController extends CommonAdminController
{
    public $doHistory = false;
    public $requireAuth = false;

    public function actionAutodeploy()
    {
        $dir = Yii::$aliases['@app'];
        exec("cd $dir && git pull 2>&1", $output);
        Yii::info(print_r($output, true));

        return json_encode(print_r($output, true));
    }

    public function actionDropdownlist($q = false) {
        $options = Yii::$app->request->get('options');
        $mn = $options['mn'];

        Yii::$app->response->format = Response::FORMAT_JSON;

        if ($q !== false) {
            return $mn::dropDownAjax($q, $options);
        } else {
            return [];
        }
    }
}