<?php
namespace rusbitles\adminbase\controllers;

use rmrevin\yii\ulogin\AuthAction;
use rusbitles\adminbase\controllers\base\CommonAdminController;
use rusbitles\adminbase\helpers\SystemTools;
use rusbitles\adminbase\models\User;
use rusbitles\adminbase\models\UserSoc;
use Yii;
use yii\web\NotFoundHttpException;

class UserController extends CommonAdminController
{
    public $doHistory = false;
    public $layout = 'gentelella_form';
    public $requireAuth = false;

    public function actionLogin()
    {
        $this->title = "Вход на сайт";
        $this->h1title = "Вход на сайт";


        if (!\Yii::$app->user->isGuest) {
            return $this->redirect($this->lastPage);
        }
        
        $model = new User();
        $model->scenario = 'login';
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect($this->lastPage);
        }
        return $this->render('@rusbitles/adminbase/views/user/login', ['model' => $model]);
    }

    public function actionLogout($back = '/')
    {
        Yii::$app->user->logout();

        return $this->redirect($back);
    }

    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/');
        }

        $success = false;
        $model = new RegisterForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $success = true;

            $newUser = new User();
            $newUser->username = $model->username;
            $newUser->email = $model->email;
            $newUser->password = md5($model->password);
            if ($model->name == '') {
                $newUser->name = $model->username;
            } else {
                $newUser->name = $model->name;
            }
            $newUser->public = 0;
            $newUser->auth_key = substr(md5(time() . $newUser->username), 5, 6);
            $newUser->save();

            Yii::$app->mailer->compose('register', ['model' => $newUser])
                ->setFrom(Settings::getParam('adminEmail'))
                ->setTo($newUser->email)
                ->setSubject('Успешная регистрация на сайте «'.Yii::$app->params['name'].'»')
                ->send();
        }

        return $this->render('register', ['model' => $model, 'success' => $success]);
    }

    public function actionConfirm($id = false, $code = '')
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/');
        }

        $success = false;
        if ($id !== false) {
            $model = User::findOne(intval($id));
            if ($model->auth_key != '' && $model->auth_key == $code) {
                $model->auth_key = '';
                $model->public = 1;
                $model->save();

                Yii::$app->user->login($model, 3600*24*30);

                $success = true;
            }
        } else {
            throw new NotFoundHttpException();
        }

        return $this->render('confirm', ['model' => $model, 'success' => $success]);
    }

    public function actionRecovery()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/');
        }

        $success = false;
        $model = new RecoveryForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $userLogin = User::find()->andWhere(['=', 'username', $model->username])->andWhere(['=', 'public', 1])->one();
            $userEmail = User::find()->andWhere(['=', 'email', $model->email])->andWhere(['=', 'public', 1])->one();
            $user = $userLogin;
            if (!$user) $user = $userEmail;

            if ($user) {
                $time = intval(end(explode(':', $user->recovery)));

                if ($time + 1800 < time()) {
                    $user->recovery = substr(md5(time() . $user->password), 4, 6) . ':' . time();
                    $user->save();

                    Yii::$app->mailer->compose('recovery', ['model' => $user])
                        ->setFrom(Settings::getParam('adminEmail'))
                        ->setTo($user->email)
                        ->setSubject('Восстановление пароля на сайте «' . Yii::$app->params['name'] . '»')
                        ->send();

                    $success = true;
                } else {
                    $model->addError('username', 'Письмо для восстановления пароля уже было отправлено! Повторное отправление возможно через 30 минут.');
                }
            } else {
                $model->addError('username', 'Пользователь не найден!');
            }
        }

        return $this->render('recovery', ['model' => $model, 'success' => $success]);
    }

    public function actionChangepass($id = false, $code = '')
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect('/');
        }

        $success = false;
        if ($id !== false) {
            $user = User::findOne(intval($id));
            if (!$user || $user->recovery == '') throw new NotFoundHttpException();
            $recovery = explode(':', $user->recovery);
            if ($recovery[0] != $code || intval($recovery[1]) + 1800 < time()) throw new NotFoundHttpException();

            $model = new ChangepassForm();
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $user->password = md5($model->password);
                $user->recovery = '';
                $user->save();

                $success = true;
            }
        } else {
            throw new NotFoundHttpException();
        }

        return $this->render('changepass', ['model' => $model, 'success' => $success]);
    }

    public function actionUlogin($backurl = false) {
        $uAuth = new AuthAction('ulogin', $this);
        $uAuth->successCallback = 'rusbitles\adminbase\controllers\UserController::uloginSuccessCallback';
        $uAuth->run();

        if ($backurl !== false) {
            return $this->redirect($backurl);
        } else {
            return $this->redirect($this->lastPage);
        }
    }

    public static function uloginSuccessCallback($attributes)
    {
        if (!empty($attributes)) {
            $soc = UserSoc::find()->where(['network' => $attributes['network'], 'uid' => $attributes['uid']])->one();
            if (!$soc) {
                $soc = new UserSoc();
                $soc->network = $attributes['network'];
                $soc->uid = $attributes['uid'];
                $soc->soc_array = json_encode($attributes);
            }

            if (Yii::$app->user->isGuest) {
                if ($soc->isNewRecord) {
                    $user = User::find()->where(['username' => $attributes['email']])->one();
                } else {
                    $user = $soc->user;
                }
                if (!$user) {
                    $user = new User();
                    $user->username = $attributes['email'];
                    $user->password = substr(md5(time()), 5, 10);
                    $user->email = $attributes['email'];
                    $user->public = 1;
                    $user->name = $attributes['first_name'];
                    $user->last_name = $attributes['last_name'];
                }

                if ($user->image == '') {
                    $user->image = SystemTools::getExternalFile($attributes['photo_big'], 'user');
                }

                if ($user->save()) {
                    $soc->user_id = $user->id;
                    $soc->save();

                    Yii::$app->user->login($user);
                }
            } else {
                $soc->user_id = Yii::$app->user->id;
                $soc->save();
            }
        }
    }
}
