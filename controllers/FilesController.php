<?php
namespace rusbitles\adminbase\controllers;

use rusbitles\adminbase\controllers\base\CommonAdminController;
use Yii;

class FilesController extends CommonAdminController
{
    public function actionIndex($parent = false)
    {
        return $this->render('index');
    }
}