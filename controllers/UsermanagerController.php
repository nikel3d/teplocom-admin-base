<?php
namespace rusbitles\adminbase\controllers;

use rusbitles\adminbase\controllers\base\AdminController;
use Yii;

class UsermanagerController extends AdminController
{
    public static $mainmenuHandle = 'usermanager';
    public static $modelName='User';
    public static $fullModelName='rusbitles\\adminbase\\models\\User';
    public $baseModel=true;
    public static $modelTitles = ['Пользователь', 'Пользователи', 'Пользователей'];
    public static $images = [
        'image' => '@webroot/up/user',
    ];
    public static $children = [
    ];
    public $parents = [
    ];
    public static $links = [
    ];

    public function init()
    {
        static::$fullModelName = Yii::$app->user->identityClass;
        parent::init();
    }

    public function searchFields()
    {
        return array_merge(parent::searchFields(), [
            'username' => ['type' => 'string'],
            'email' => ['type' => 'string'],
            'fullname_search' => ['type' => 'manual', 'function' => function($query, $model) {
                /** @var ActiveQuery $query */
                $parts = preg_split('/\s+/', $model->fullname_search);

                $rules = ['and'];
                foreach ($parts as $part) {
                    $rules[] = ['or', ['like', 'name', $part], ['like', 'second_name', $part], ['like', 'last_name', $part], ['like', 'email', $part]];
                }
                $query->andFilterWhere($rules);

                return $query;
            }],
        ]);
    }

    public function actionSpyauth($uid) {
        if (!Yii::$app->user->isGuest && Yii::$app->user->can('spyauthorize')) {
            $cn = Yii::$app->user->identityClass;
            $user = $cn::find()->where(['id' => $uid])->one();
            if ($user) {
                Yii::$app->user->login($user->getUser(), 0);
            }
        }
        return $this->redirect('/');
    }
}