<?php
namespace rusbitles\adminbase\controllers;

class SystemmenuController extends \rusbitles\adminbase\controllers\base\TreeAdminController
{
    public static $mainmenuHandle = 'systemmenu';
    public static $modelName='Menu';
    public static $fullModelName='rusbitles\\adminbase\\models\\Menu';
    public $baseModel=true;
    public static $modelTitles = ['Меню', 'Меню', 'Меню'];
    public static $images = [
    ];
    public static $children = [
    ];
    public $parents = [
    ];
    public static $links = [
        'types' => ['system_menu2type', 'menu_id', 'type_id'],
    ];
}